<?php

class mbuilder_config_ui{

    /**
     * @var string $config_id configuration id
     */
    var $config_id;

    /**
     * @var Callable $validator Callback function that validate post save
     */
    var $validator;

    /**
     * @var string $configuration_id config identifier
     */
    var $configuration_id;

    /**
     * @var string $name the object name
     */
    var $name;

    /**
     * @var array config list fields
     */
    var $fields;

    /**
     * @var int $microsite_category current microsite main category
     */
    private $microsite_category;

    /**
     * @var int $microsite_id current microsite id
     */
    private $microsite_id;

    /**
     * @var array $microsite current microsite data
     */
    private $microsite;

    /**
     * @var array $args post type args
     */
    private $args;

    /**
     * @var string $logo_placeholder the logo default image
     */
    private $logo_placeholder;

    /**
     * @var array $object_taxonomies current object registered taxonomy
     */
    private $object_taxonomies = array();

    /**
     * @var string $class_stamp class identifier
     */
    private $class_stamp = '';

    function __construct($microsite_id, $configuration_id, $name, $fields, $args = array()){

        $this->config_id    = $configuration_id;

        $this->microsite_id = $microsite_id;

        $this->class_stamp  = md5($configuration_id);

        $this->args     = wp_parse_args($args, array(
            'menu_icon'  => 'dashicons-admin-generic'
        ));

        $this->name     = $name;

        $this->fields   = $fields;

        foreach($this->fields as &$field)
        {
            $field = wp_parse_args($field, array(
                'required'  => false
                ,'label'    => ''
                ,'type'     => 'text'
                ,'default'  => ''
            ));
        }

        $this->args     = wp_parse_args($args, array(
            'caps'  => 'manage-microsite'
        ));

        /**
         * Post validator
         */
        $this->validator = isset($args['validator']) ? $args['validator'] : false;

        $this->initialize();
    }

    /**
     * Get current microsite category
     */
    function _load_env(){
        $microsite                  = wp_cache_get( 'microsite', 'dmd-event-microsite' );
        $meta                       = wp_cache_get( 'microsite-meta', 'dmd-event-microsite' );
        $this->microsite            = msp()->get_microsite($this->microsite_id);
        $this->microsite_category   = msp()->get_microsite_cat($this->microsite_id);
        $this->logo_placeholder     = '';
    }

    function initialize(){
        $this->_load_env();
        add_action('admin_init', array($this,'_register_modules'));
        add_action('admin_init', array($this,'_process'));
    }

    function _process(){

        if(!mikro()->is_admin())
        {
            return false;
        }

        if(!mikro()->current_user_can($this->args['caps']))
        {
            return false;
        }

        $class_stamp_against = isset($_POST['_class_stamp']) ? $_POST['_class_stamp'] : '';

        if($class_stamp_against == $this->class_stamp)
        {
            msp_process_api()->process(
                array($this,'config_update')
                ,'mbuilder-config-ui'
                ,'Success save changes'
                ,'Failed to save your changes'
                ,'manage-microsite'
            );
        }
    }

    /**
     * Register the management module
     */
    function _register_modules(){
        if(!mikro()->current_user_can($this->args['caps']))
        {
            return false;
        }
        $stamp      = $this->class_stamp;
        $fields     = $this->fields;
        $config_id  = $this->config_id;
        mikro()->add_menu_page( 
            $this->name
            ,$this->name
            ,'manage-microsite'
            ,$this->config_id
            ,function($microsite_id) use ($stamp, $fields, $config_id){
                $data = get_post_meta($microsite_id, 'mb-cui-' . $config_id, true);
                if(empty($data))
                {
                    $data = array();
                }
                msp_render('/view/modules/configuration.php', array(
                    'microsite_id'  => $microsite_id
                    ,'class_stamp'  => $stamp
                    ,'configs'      => $fields
                    ,'data'         => $data
                ));
            }
            ,sprintf('<span class="dashicons the-icon %s"></span>', $this->args['menu_icon'])
            ,'Configuration'
        );
    }

    /**
     * Wrapper function to retrieve objects
     */
    function get($key, $default_value = ''){
        if(!isset($this->fields))
        {
            return $default_value;
        }
        if(!isset($this->fields[$key]))
        {
            return $default_value;
        }

        $configs = wp_cache_get('config-ui-meta-' . $this->config_id, 'mbuilder');

        if(empty($configs))
        {
            $configs = get_post_meta($this->microsite_id, 'mb-cui-' . $this->config_id, true);

            wp_cache_add('config-ui-meta-' . $this->config_id, $configs, 'mbuilder');
        }

        $field_args = $this->fields[$key];

        if($field_args['type'] == 'image-picker')
        {
            if(!empty($configs[$key]))
            {
                return array(
                    'id'    => $configs[$key]
                    ,'url'  => wp_get_attachment_url($configs[$key])
                );
            }
            else 
            {
                return array(
                    'id'    => ''
                    ,'url'  => $default_value
                );
            }
        }

        if(isset($configs[$key]))
        {
            return $configs[$key];
        }
        else
        {
            return $default_value;
        }
    }

    /**
     * Process saving object
     */
    function config_update(){
        $object = apply_filters('mbuilder_config_ui_form_data', $_POST, $this->config_id);

        $stamp = $this->class_stamp;

        require_once msp()->plugin_dir . '/vendor/autoload.php';

        $data = array(
            'validity' => isset($_POST['validity']) ? $_POST['validity'] : ''
        );

        $validator = array(
            'rules'     => array(
                'validity' => array(
                    'required'
                    ,'validity-check' => function($value) use ($object, $stamp){
                        if(empty($object['id']))
                        {
                            return true;
                        }
                        return wp_verify_nonce($value, 'config-ui-' . $stamp);
                    }
                )
            )
            ,'naming'   => array(
                
            )
            ,'errors'   => array(
                'validity-check'    => 'Validity check failed'
            )
        );

        $previous_data = get_post_meta($this->microsite_id, 'mb-cui-' . $this->config_id, true);

        if(empty($previous_data))
        {
            $previous_data = array();
        }

        foreach ($this->fields as $key => $field) 
        {
            $args = wp_parse_args($field, array(
                'required'  => false
                ,'label'    => ''
                ,'type'     => 'text'
                ,'caps'     => 'manage-microsite'
            ));

            if(!mikro()->current_user_can($args['caps']))
            {
                if(isset($previous_data[$key]))
                {
                    $data[$key] = $previous_data[$key];
                }
                continue;
            }

            if($args['required'])
            {
                $validator['rules'][$key] = array(
                    'required'
                );
                $validator['naming'][$key] = $args['label'];
            }

            $data[$key] = isset($_POST[$key]) ? $_POST[$key] : '';
        }

        if(is_callable($this->validator))
        {
            $valid = call_user_func_array($this->validator, array($id, $to_save, $data));

            if(isset($valid['status']) && $valid['status'] == false)
            {
                wp_die( $valid['message'], $valid['message'] );
            }
        }

        $validator = apply_filters('mbuilder_config_ui_form_data', $validator, $data, $this->config_id);

        $validation_result = SimpleValidator\Validator::validate($data, $validator['rules'], $validator['naming']);

        $validation_result->customErrors($validator['errors']);

        if(!$validation_result->isSuccess())
        {
            $errors = $validation_result->getErrors();

            return array(
                'status'    => false
                ,'message'  => $errors[0]
                ,'data'     => $object
            );
        }
        else
        {
            foreach ($this->post_meta_args as $key => $meta_args) 
            {
                if(isset($meta_args['required']) && $meta_args['required'] && empty($object[$key]))
                {
                    wp_die( $meta_args['label'] . ' is Required', $meta_args['label'] . ' is Required' );
                }
            }

            if(is_callable($this->validator))
            {
                $valid = call_user_func_array($this->validator, array($id, $to_save, $data));
    
                if(isset($valid['status']) && $valid['status'] == false)
                {
                    wp_die( $valid['message'], $valid['message'] );
                }
            }

            unset($data['validity']);

            update_post_meta($this->microsite_id, 'mb-cui-' . $this->config_id, $data);
    
            return array(
                'status'    => true
                ,'message'  => 'Sucess saving your changes..'
            );            
        }
    }
}