<?php
/**
 * Engine file of microsite builder emulated function. 
 *
 * @author Deni Tri Hartanto <2denitri@gmail.com>
 * @version 2.0
 */
class msp_emulate{

    /**
     * @var msp_emulate $instance class instance
     */
    static $instance;

    /**
     * @var array $wp_post_types
     */
    public $wp_post_types = array();

    /**
     * Get class instance
     *
     * @since 1.0 introduced
     * @return msp_emulate the class instance
     */
    public static function get_instance(){
        if(self::$instance === null)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/wp_doing_ajax/
     */
    function wp_doing_ajax(){
        return wp_doing_ajax() && !empty($_REQUEST['mbuilder_ajax']);
    }

    /**
     * Get current microsite Home URL
     */
    function home_url($path = ''){
        $microsite_path     = msp()->get_microsite_path(msp()->get_microsite_id());
        if(empty($microsite_path))
        {
            return '';
        }
        return untrailingslashit(home_url($microsite_path . $path)) ;
    }

    /**
     * Get microsite dashboard URL
     */
    function admin_url($path = ''){
        $microsite_path     = msp()->get_microsite_path(msp()->get_microsite_id());
        return untrailingslashit(home_url($microsite_path. '/' . $path)) ;
    }

    /**
     * Emulate https://codex.wordpress.org/Function_Reference/wp_add_dashboard_widget
     */
    function wp_add_dashboard_widget($widget_id, $widget_name, $callback, $widget_icon = '', $background = '', $tile_width = 1){
        add_filter('mbuilder_dashboard_widgets', function($widgets) use($widget_id, $widget_name, $callback, $widget_icon, $background, $tile_width){
            $widgets[$widget_id] = array(
                'widget_id'     => $widget_id
                ,'widget_name'  => $widget_name
                ,'callback'     => $callback
                ,'icon'         => $widget_icon
                ,'image'        => $background
                ,'width'        => $tile_width
            );
            return $widgets;
        });
    }

    /**
     * Emulate https://codex.wordpress.org/Rewrite_API/add_rewrite_rule
     */
    function add_rewrite_rule($regex, $redirect, $rewrite_tags = array()){
        msp_rewrite::get_instance()->add_rewrite_rule(
            $regex
            ,$redirect
            ,$rewrite_tags
        );
    }

    function add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $group = 'Object', $group_sort = 10){
        add_filter( 'mbuilder_microsite_controls', function($controls) use ($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $group, $group_sort){
            
            if(current_user_can( $capability ))
            {
                $controls[$menu_slug] = array(
                    'menu'      => $menu_title
                    ,'page'     => $page_title
                    ,'callback' => $function
                    ,'icon'     => $icon_url
                    ,'group'        => $group
                    ,'group_sort'   => $group_sort
                );
            }

            return $controls;
        });
    }

    /**
     * Emulate WP register post type
     */
    function register_post_type($post_type, $args){
        if(empty($args['label']))
        {
            return null;
        }

        if($this->is_admin() OR $this->is_microsite())
        {
            return $this->wp_post_types[ $post_type ] = new msp_post(msp()->get_microsite_id(),$post_type, $args['label'], $args);
        }
        else
        {
            return null;
        }
    }

    /**
     * Add custom meta to a registered post type
     */
    function set_post_type_meta($post_type, $post_metas = array()){
        add_filter( 'msp_post_meta_args', function($metas, $type) use ($post_type, $post_metas) {
            if($post_type == $type)
            {
                return array_merge( $metas, $post_metas );
            }
            return $metas;
        }, 10, 2 );
    }

    /**
     * Emulate WP register taxonomy
     */
    function register_taxonomy($taxonomy, $object_type, $args = array()){
        $args['rewrite'] = false;
        register_taxonomy($taxonomy, $object_type, $args);
    }

    /**
     * Emulate wordpress get_stylesheet_directory()
     */
    function get_stylesheet_directory(){

        $microsite_id = msp()->get_microsite_id();
        $theme = msp()->get_microsite_theme($microsite_id);


        return $theme['ThemePath'];
    }
    
    /**
     * Emulate wordpress get_stylesheet_directory_uri()
     */
    function get_stylesheet_directory_uri(){
        
        $microsite_id = msp()->get_microsite_id();
        $theme = msp()->get_microsite_theme($microsite_id);
        
        return $theme['ThemeURL'];
    }
    
    /**
     * Emulate wordpress get_template_directory()
     */
    function get_template_directory(){
        
        $microsite_id = msp()->get_microsite_id();
        $theme = msp()->get_microsite_theme($microsite_id);

        if(!empty($theme['Template']))
        {
            return msp()->get_themes_dir() . "/" . $theme['Template'];
        }
        else
        {
            return $theme['ThemePath'];
        }

        
    }

    /**
     * Emulate wordpress get_template_directory_uri()
     */
    function get_template_directory_uri(){
        
        $microsite_id = msp()->get_microsite_id();

        $theme = msp()->get_microsite_theme($microsite_id);

        if(!empty($theme['Template']))
        {
            return msp()->get_themes_url() . "/" . $theme['Template'];
        }
        else
        {
            return $theme['ThemeURL'];
        }

    }

    /**
     * Emulate wordpress register_nav_menus
     */
    function register_nav_menus($locations){
        foreach ($locations as $location => $name) 
        {
            register_nav_menu( $location, $name );
        }
        add_filter('mbuilder_microsite_menu_locations', function($current_locations) use ($locations){
            return array_merge($current_locations, $locations);
        });
    }

    /**
     * Emulate wordpress get_registered_nav_menus
     */
    function get_registered_nav_menus(){
        return apply_filters('mbuilder_microsite_menu_locations', array());
    }

    function get_nav_menu_locations(){
        return get_post_meta(msp()->get_microsite_id(), 'menus', true);
    }

    /**
     * Emulate wordpress get_category_link
     */
    function get_category_link($category){
        if ( ! is_object( $category ) ) 
        {
            $category = (int) $category;
        }
     
        $category = get_category($category);
     
        if ( is_wp_error( $category ) ) {
            return '';
        }
     
        return $this->home_url('/category/' . $category->slug . '/' );
    }

    /**
     * Emulate WordPress default get_permalink
     * 
     * @param int|WP_Post $post
     */
    function get_permalink($post){
        if(is_integer($post) or is_string($post))
        {
            $post = get_post($post);
        }
        else if(get_class($post) == 'msp_post')
        {
            return '';
        }
        else if(get_class($post) == 'WP_Post')
        {
            
        }
        else
        {
            return '';
        }
        
        $is_microsite_meta = get_post_meta($post->ID,'_mbuilder_updated', true);
        

        if(!empty($is_microsite_meta) && $is_microsite_meta == msp()->get_microsite_id())
        {
            if($post->post_type == 'post')
            {
                return $this->home_url('/post/' . $post->post_name);
            }
            else
            {
                $post_type = $this->get_post_type_object($post->post_type);
                if($post_type->rewrite_args['slug'] === false)
                {
                    return false;
                }

                return mikro()->home_url('/' . $post_type->rewrite_args['slug'] . '/' . $post->post_name);
            }
        }
        else
        {
            return get_permalink($post);
        }
    }

    /**
     * Emulate locate_template 
     * 
     * @link https://developer.wordpress.org/reference/functions/locate_template/
     */
    function locate_template( $template_names, $load = false, $require_once = true ){
        $located = '';
        foreach ( (array) $template_names as $template_name ) {
            if ( ! $template_name ) {
                continue;
            }
            if ( file_exists( $this->get_stylesheet_directory() . '/' . $template_name ) ) {
                $located = $this->get_stylesheet_directory() . '/' . $template_name;
                break;
            } elseif ( file_exists( $this->get_template_directory() . '/' . $template_name ) ) {
                $located = $this->get_template_directory() . '/' . $template_name;
                break;
            }
        }
        if ( $load && '' != $located ) {
            load_template( $located, $require_once );
        }
    
        return $located;
    }

    /**
     * Emulate get_template_part 
     * 
     * @link https://developer.wordpress.org/reference/functions/get_template_part/
     */
    function get_template_part( $slug, $name = null ) {
        do_action( "msp_get_template_part_{$slug}", $slug, $name );
     
        $templates = array();
        $name      = (string) $name;
        if ( '' !== $name ) {
            $templates[] = "{$slug}-{$name}.php";
        }
     
        $templates[] = "{$slug}.php";
     
        /**
         * Fires before a template part is loaded.
         *
         * @since 5.2.0
         *
         * @param string   $slug      The slug name for the generic template.
         * @param string   $name      The name of the specialized template.
         * @param string[] $templates Array of template files to search for, in order.
         */
        do_action( 'msp_get_template_part', $slug, $name, $templates );
     
        $this->locate_template( $templates, true, false );
    }
    

    /**
     * Emulate WordPress get_post_type_object
     * 
     * @link https://developer.wordpress.org/reference/functions/get_post_type_object/
     */
    function get_post_type_object( $post_type ) {
        
        if ( ! is_scalar( $post_type ) || empty( $this->wp_post_types[ $post_type ] ) ) {
            return null;
        }
     
        return $this->wp_post_types[ $post_type ];
    }

    

    function get_bloginfo($show = '', $filter = 'raw'){
        $microsite = msp()->get_microsite(msp()->get_microsite_id());
        $output = '';
        
        switch ( $show ) {
            case 'url':
            case 'wpurl':
                $output = mikro()->home_url();
                break;
            case 'description':
                $output = $microsite->post_content;
                break;
            case 'rdf_url':
            case 'rss_url':
            case 'rss2_url':
            case 'atom_url':
            case 'comments_atom_url':
            case 'comments_rss2_url':
            case 'pingback_url':
            case 'stylesheet_url':
                $output = mikro()->get_stylesheet_directory_uri();
                break;
            case 'stylesheet_directory':
                $output = mikro()->get_stylesheet_directory();
                break;
            case 'template_directory':
                $output = mikro()->get_template_directory();
                break;
            case 'template_url':
                $output = mikro()->get_template_directory_uri();
                break;
            case 'admin_email':
            case 'charset':
            case 'html_type':
            case 'version':
            case 'language':
            case 'text_direction':
                $output = 'ltr';
                break;
            case 'name':
            default:
                $output = $microsite->post_title;
                break;
        }
     
        $url = true;
        if ( strpos( $show, 'url' ) === false &&
            strpos( $show, 'directory' ) === false &&
            strpos( $show, 'home' ) === false ) {
            $url = false;
        }
     
        if ( 'display' == $filter ) {
            if ( $url ) {
                /**
                 * Filters the URL returned by get_bloginfo().
                 *
                 * @since 2.0.5
                 *
                 * @param mixed $output The URL returned by bloginfo().
                 * @param mixed $show   Type of information requested.
                 */
                $output = apply_filters( 'bloginfo_url', $output, $show );
            } else {
                /**
                 * Filters the site information returned by get_bloginfo().
                 *
                 * @since 0.71
                 *
                 * @param mixed $output The requested non-URL site information.
                 * @param mixed $show   Type of information requested.
                 */
                $output = apply_filters( 'bloginfo', $output, $show );
            }
        }
     
        return $output;
    }

    // 
    // CONDITIONAL TAGS
    // 

    /**
     * Emulate https://developer.wordpress.org/reference/functions/is_admin/
     */
    function is_admin(){
        if(wp_doing_ajax())
        {
            return !empty($_REQUEST['mbuilder_ajax']);
        }
        else if(is_admin())
        {
            return !empty($_GET['mbuilder']);
        }

        return false;
    }

    /**
     * Is current frontend is part of microsite
     */
    function is_microsite(){
        if(is_admin())
        {
            return false;
        }
        
        $microsite_id   = wp_cache_get('microsite_id', 'mbuilder');
        $microsite      = wp_cache_get('microsite', 'mbuilder');
                
        return !empty($microsite_id);
    }

    /**
     * Is current page a microsite environment
     * 
     * @return boolean true if this is a microsite environment
     */
    function is_microsite_environment(){
        return $this->is_microsite() OR $this->is_admin() OR $this->wp_doing_ajax();
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/is_home/
     */
    function is_home(){
        return get_query_var('mbuilder-page') == 'home';
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/is_single/
     * 
     * @param int|string|array $post post identity to check
     */
    function is_single($_post = ''){
        if(empty($_post))
        {
            return get_query_var('mbuilder-page') == 'post';
        }

        $is_archive = get_query_var('mbuilder-page') == 'archive';

        if($is_archive)
        {
            return false;
        }

        if(!is_array($_post))
        {
            $posts = array($_post);
        }
        else
        {
            $posts = $_post;
        }

        $true = false;
        
        global $post;

        foreach($posts as $p)
        {
            if(is_integer($p))
            {
                $true = $p == $post->ID;
            }
            else
            {
                $true = ($p == $post->post_title OR $p == $post->post_name);
            }

            if($true)
            {
                break;
            }
        }

        return $true;
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/is_singular/
     * 
     * @param string|array $post_types post type to check
     */
    function is_singular($post_types){
        
        $is_archive = get_query_var('mbuilder-page') == 'archive';

        if($is_archive)
        {
            return false;
        }

        if(empty($post_types))
        {
            return $this->is_single();
        }
        else
        {
            $true = false;
        
            global $post;

            if(empty($post))
            {
                return false;
            }

            if(!is_array($post_types))
            {
                $types = array($post_types);
            }
            else
            {
                $types = $post_types;
            }

            foreach($types as $p)
            {
                $true = $p == $post->post_type;

                if($true)
                {
                    break;
                }
            }

            return $true;
        }
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/is_post_type_archive/
     * 
     * @param string|array $post_types post type to check
     */
    function is_post_type_archive($_post_type){
        $true       = false;
        $post_type  = get_query_var('post_type');
        $is_archive = get_query_var('mbuilder-page') == 'archive';

        if(!$is_archive)
        {
            return false;
        }
        
        global $post;

        if(!is_array($post_type))
        {
            $types = array($post_type);
        }
        else
        {
            $types = $post_type;
        }

        foreach($types as $p)
        {
            $true = $p == $_post_type;

            if($true)
            {
                break;
            }
        }

        return $true;
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/is_category/
     * 
     * @param mixed $category Category ID, name, slug, or array of Category IDs, names, and slugs.
     */
    function is_category($category = ''){
        $is_cat = get_query_var('mbuilder-page') == 'category';

        if(!$is_cat)
        {
            return false;
        }

        global $wp_query;

        
        $cat = $wp_query->queried_object;

        $true = false;

        if(!is_array($category))
        {
            $categories = array($category);
        }
        else
        {
            $categories = $category;
        }

        foreach($categories as $p)
        {
            $true = $category->term_id == $category || $category->name == $category || $category->slug == $category;

            if($true)
            {
                break;
            }
        }

        return $true;
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/is_tax/
     * 
     * @param string|array $taxonomy Taxonomy slug or slugs.
     * @param int|string|array $term Term ID, name, slug or array of Term IDs, names, and slugs.
     * @todo Taxonomy archive system
     */
    function is_tax($taxonomy = '', $term = ''){
        return false;
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/is_archive/
     */
    function is_archive(){
        return get_query_var('mbuilder-page') == 'archive';
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/is_search/
     * 
     * @todo create function to emulate is_search
     */
    function is_search(){
        return false;
    }


    /**
     * Emualate https://developer.wordpress.org/reference/functions/is_404/
     */
    function is_404(){
        return get_query_var('mbuilder-page') == 'other';
    }

    /**
     * Emulate https://developer.wordpress.org/reference/functions/current_user_can/
     */
    function current_user_can($cap){
        $current_user = wp_get_current_user();
 
        if ( empty( $current_user ) ) 
        {
            return false;
        }

        return isset($current_user->allcaps[$cap]) && $current_user->allcaps[$cap];
    }
}