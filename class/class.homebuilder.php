<?php
/**
 * Engine file of microsite home builder
 *
 * @version 1.0
 * @author deni tri hartanto
 */

/**
 * Engine class of homepage builder
 *
 * @version 1.0
 * @author deni tri hartanto
 */
class msp_home_builder{
    /**
     * @var msp_home_builder $instance class instance
     */
    static $instance;

    /**
     * @var array $elements available element on homepage builder
     */
    var $elements;

    /**
     * Get class instance
     *
     * @since 1.0 introduced
     * @return msp_home_builder the class instance
     */
    public static function get_instance(){
        if(self::$instance === null)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Integrate into microsite system
     */
    function integrate(){
        add_action('msp_microsite_loaded', array($this,'microsite_integrate'), 10);
    }

    /**
     * Start function that only need to run in microsite environment
     */
    function microsite_integrate(){
        add_action('init', array($this,'register_homepage_elements'), 10);
        add_action('admin_init', array($this,'process'));
        add_action('wp_loaded', array($this,'load_elements'), 10);
        add_action('admin_menu', array($this,'register_menu'), 11);
    }

    /**
     * Register default homepage element
     */
    function register_homepage_elements(){
        msp_register_home_component(
            'mbuilder-fake-column-element'
            ,'Fake Column Element'
            ,array(msp_home_builder::get_instance(),'fake_column')
            ,array(msp_home_builder::get_instance(),'fake_column')
            ,array(
                'icon'             => 'dashicons-tagcloud'
                ,'_system'           => true
            )
        );
        msp_register_home_component(
            'mbuilder-column-two'
            ,'Row Container - 2 Column'
            ,array(msp_home_builder::get_instance(),'GUI_column_two')
            ,array(msp_home_builder::get_instance(),'FE_column_two')
            ,array(
                'icon'             => 'dashicons-tagcloud'
            )
        );
        msp_register_home_component(
            'mbuilder-column-three'
            ,'Row Container - 3 Column'
            ,array(msp_home_builder::get_instance(),'GUI_column_three')
            ,array(msp_home_builder::get_instance(),'FE_column_three')
            ,array(
                'icon'             => 'dashicons-tagcloud'
            )
        );
    }
    
    function load_elements(){
        $this->elements = apply_filters( 'mhp_homepage_element', array());
    }

    /**
     * Register into microsite menu
     */
    function register_menu(){
        mikro()->add_menu_page( 
            'Home Layout'
            ,'Home Layout'
            ,'manage-microsite'
            ,'home-page'
            ,array($this,'GUI')
            ,'<span class="dashicons the-icon dashicons-admin-home"></span>'
            ,'Appearance'
            ,30
        );
    }

    /**
     * Register process hook
     */
    function process(){
        msp_process_api()->process(
            array($this,'save')
            ,'mhp'
            ,'Success save homepage'
            ,'Failed to save your changes'
            ,'manage-microsite'
        );
        msp_process_api()->process_by_link(
            array($this,'reset')
            ,'mhp-reset'
            ,'Homepage Reset'
            ,'Failed to save your changes'
            ,'manage-microsite'
        );
    }

    /**
     * Get homepage element
     * 
     * @return array
     */
    function get_elements(){
        if(empty($this->elements))
        {
            return array();
        }
        return $this->elements;
    }

    /**
     * Process save the homepage
     *
     * @since 1.0
     */
    function save(){
        if(!isset($_POST['mhp']))
        {   
            update_post_meta(msp()->get_microsite_id(), 'mhp', array());

            return array(
                'status'    => true
                ,'message'  => 'Homepage was saved'
            );
        }


        $raw_elements   = $_POST['mhp'];

        /**
         * Remap array so it easier to read
         */
        $elements       = array();
        foreach ($raw_elements as &$element) 
        {
            $element_identifier = key($element);
            array_push($elements,array(
                'element'   => $element_identifier
                ,'options'  => $element[$element_identifier]
            ));
        }

        /**
         * Process element in column
         */
        $in_column_element  = array();                  // mapped $elements into this variable
        $element_to_store   = 0;                        // a flag to tell how much again to store element
        foreach ($elements as $index => $element) 
        {
            // 02. if there is a flag, then save the element inside column
            if($element_to_store > 0)
            {
                // 04. if next element is a column element, then void the flag
                if(in_array($element['element'],array('mbuilder-column-two','mbuilder-column-three')))
                {
                    $element_to_store = 0;
                    array_push($in_column_element, $element);
                }
                // 05. put element into column child
                else
                {
                    array_push($in_column_element[count($in_column_element) - 1]['options']['child'], $element);
                    $element_to_store--;
                }
            }
            // 03. no flag, then save into normal dimension
            else
            {
                array_push($in_column_element, $element);
            }

            // 01. flag setup
            if($element['element'] == 'mbuilder-column-two')
            {
                $in_column_element[count($in_column_element) - 1]['options']['child'] = array();
                $element_to_store = 2;
            }
            if($element['element'] == 'mbuilder-column-three')
            {
                $in_column_element[count($in_column_element) - 1]['options']['child'] = array();
                $element_to_store = 3;
            }
        }

        update_post_meta(msp()->get_microsite_id(), 'mhp', $in_column_element);

        return array(
            'status'    => true
            ,'message'  => 'Homepage was saved'
        );

    }

    /**
     * Reset / Erase Homepage configuration
     */
    function reset(){
        update_post_meta(msp()->get_microsite_id(), 'mhp', array());

        return array(
            'status'    => true
            ,'message'  => 'Homepage was saved'
            ,'query_to_remove' => array(
                'mhp-reset'
            )
        );
    }

    /**
     * Build Element Item
     *
     * @since 1.0
     * @param string $type the item type, available | appended
     * @param array $element_conf element configuration
     * @param array $saved_options currently saved options
     * @param string $index element index on builder
     * @return void
     */
    function build_element_item($type, $element_conf, $saved_options = array(), $index = '<% index %>'){
        ?>
        <li class="mhp-element <?php echo $type ?> <?php echo $element_conf['id'] ?>" data-multiple="<?php echo $element_conf['parameter']['multiple'] ?>" data-element-id="<?php echo $element_conf['id'] ?>" <?php echo $type == 'appended' ? sprintf('data-index-id="%s"', uniqid('element_')) : ''  ?>  >
            <div class="title-block">
                <div class="table-layout">
                    <div class="table-cell auto-width icon">
                        <span class="dashicons <?php echo $element_conf['parameter']['icon'] ?>"></span>
                    </div>
                    <div class="table-cell auto-width">
                        <span class="control-trigger">
                            <span class="dashicons dashicons-admin-generic element-config" title="Setup this element" ></span>
                            <span class="dashicons dashicons-trash element-delete" title="Delete this element"></span>
                        </span>
                    </div>
                    <div class="table-cell">
                        <span class="title">
                            <?php echo $element_conf['name'] ?>
                        </span>
                    </div>
                    <div class="table-cell auto-width">
                        <span class="control-trigger right">
                            <span class="dashicons dashicons-menu element-reorder" title="Re-Order element"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="content-block">
                <?php 
                if(!empty($element_conf['renderer']) AND is_callable($element_conf['renderer']))
                {
                    call_user_func_array($element_conf['renderer'], array($index, $saved_options, $element_conf['id'], $element_conf));
                }
                else
                {
                    $this->static_layout_config($index, $saved_options, $element_conf['id'], $element_conf);
                }
                if(isset($saved_options['id']))
                {
                    printf(
                        '<input type="hidden" value="%s" name="%s">'
                        ,$saved_options['id']
                        , "mhp[$index][{$element_conf['id']}][id]"
                    );
                }
                ?>
                
            </div>
        </li>
        <?php
    }

    /**
     * Static configuration just to save the layout
     * 
     * @param string $index component index
     * @param mixed $saved_options saved configuration
     * @param string $id component identifier
     */
    function static_layout_config($index, $saved_options, $id){
        printf('<input type="hidden" value="1" name="mhp[%s][%s][embed]"', $index, $id);
    }

    /**
     * Is current microsite using static structurized homepage
     * 
     * @return boolean trus if using static homepage
     */
    function is_static_homepage(){
        return !empty(apply_filters('msp_home_layout_element', array()) );
    }

    /**
     * Get homepage builded element data
     * 
     * @return array the homepage element data
     */
    function get_homepage(){
        $elements = $this->get_elements();

        $builded = array();

        $saved_elements = get_post_meta(msp()->get_microsite_id(), 'mhp', true);

        if(empty($saved_elements))
        {
            $saved_elements = array();
        }
        
        $builded_elements = $saved_elements;
    
        $use_preconfigured_structure    = msp_home_builder::get_instance()->is_static_homepage();
        $static_element                 = apply_filters('msp_home_layout_element', $saved_elements);
        foreach ($static_element as $index => $element) 
        {
            if(empty($elements[$element['element']]))
            {
                continue;
            }
            
            $element_conf       = $elements[$element['element']];

            $default_options    = $use_preconfigured_structure ? $element['options'] : array();

            // Get builded element based on "id" key in configuration options
            $using_id_element_conf = false;
            if(isset($default_options['id']))
            {
                foreach ($builded_elements as $b) 
                {
                    if(isset($b['options']) && isset($b['options']['id']) && $b['options']['id'] == $default_options['id'] && $element['element'] == $b['element'] )
                    {
                        $element_conf['options'] = wp_parse_args( $b['options'], $default_options ) ;
                        $using_id_element_conf = true;
                        break;
                    }
                }
            }

            if(!$using_id_element_conf)
            {
                // Get builded element in this position
                $builded_element = isset($builded_elements[$index]) ? $builded_elements[$index] : array();
    
                // Get this position builded element options
                if(!empty($builded_element) && $element['element'] == $builded_element['element'])
                {
                    $element_conf['options'] = wp_parse_args( $builded_element['options'], $default_options ) ;
                }
                else
                {
                    $element_conf['options'] = $default_options;
                }
            }



            array_push($builded, $element_conf);
        }

        return apply_filters('msp_mhp_homepage', $builded) ;
    }

    /**
     * Display builded homepage
     */
    function render(){
        
        foreach ($this->get_homepage() as $index => $element) 
        {
            $this->_element_render($index, $element);
        }
    }
    
    /**
     * Render backend kitchen view
     */
    function GUI($microsite_id){
        /**
         * Print CSS & JS
         */
        wp_enqueue_editor();
        wp_enqueue_style('hbp', msp()->plugin_url . '/view/assets/css/view/home-layout.css');
        wp_enqueue_script('hbp', msp()->plugin_url . '/view/assets/js/home-layout.js', array('jquery-ui-draggable','jquery-ui-droppable','underscore'));

        // get elements
        $elements = msp_home_builder::get_instance()->get_elements();

        ksort($elements);

        $use_preconfigured_structure = msp_home_builder::get_instance()->is_static_homepage();

        ?>
        <div id="homepage-builder" style="opacity:0">
            <?php msp_process_api()->respond(); ?>
            <p class="description">
                Configure your microsite homepage <br>&nbsp;
            </p>
            <div id="homepage-builder-wrapper" class="<?php echo !$use_preconfigured_structure ?  : 'fixed-home' ?>">
                <?php
                if(!$use_preconfigured_structure)
                {
                    ?>
                    <div id="stock-element">
                        <div class="col-wrap">
                            <div class="form-wrap">
                                <h1 class="grand-title">
                                    Elements
                                </h1>
                                <ul id="element-available">
                                    <?php
                                    foreach ($elements as $element) 
                                    {
                                        if($element['parameter']['_system'])
                                        {
                                            continue;
                                        }
                                        msp_home_builder::get_instance()->build_element_item('available',$element);
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div id="'home-layout'">
                    <div class="col-wrap">
                        <form action="" method="post">
                            <?php msp_process_api()->mark('mhp'); ?>
                            <div id="mhp-microsite-mock">
                                <div id="current-homepage">
                                    <div class="mhp-element unmovable">
                                        <div class="title-block">
                                            <span class="title">Header</span>
                                        </div>
                                    </div>
                                    
                                    <ul id="mhp-element-used" class="<?php echo empty($use_preconfigured_structure) ? 'mhp-sortable' : 'mhp-preconfigure' ?>" style="background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAALElEQVQYGWO8d+/efwYkoKioiMRjYGBC4WHhUK6A8T8QIJt8//59ZC493AAAQssKpBK4F5AAAAAASUVORK5CYII=)">
                                        <li class="placeholder">Drag element here</li>
                                        <?php
                                        foreach (msp_home_builder::get_instance()->get_homepage() as $index => $element) 
                                        {
                                            msp_home_builder::get_instance()->build_element_item(
                                                'appended ' . ($use_preconfigured_structure ? 'static' : '')
                                                , $element
                                                , $element['options']
                                                , uniqid('element_')
                                            );
                                        }
                                        ?>
                                    </ul>

                                    <div class="mhp-element unmovable">
                                        <div class="title-block">
                                            <span class="title">Footer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="submit">
                                <button class="button button-primary">Save Configuration</button>    
                                <a href="<?php echo msp_process_api()->mark_link(add_query_arg('mhp-reset', 1), 'mhp-reset') ?>" class="button">Reset</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>            
        </div>
        <?php
    }

    /**
     * Render element
     */
    function _element_render($index, $element_conf){
        if(!empty($element_conf['frontend']) AND is_callable($element_conf['frontend']))
        {
            do_action('msp_mhp_before_element_render', $element_conf, $index);

            call_user_func_array($element_conf['frontend'], array($element_conf['options'], $element_conf['id'], $element_conf));

            do_action('msp_mhp_after_element_render', $element_conf, $index);
        }
    }

    function _GUI_column($type, $index, $saved_options){
        $type == 'mbuilder-column-two' ? 2 : 3;
        printf('<input type="hidden" value="1" name="mhp[%s][%s][mark]"/>', $index, $type);
        echo '<ul class="mhp-sortable">';
        echo '<li class="placeholder">Drag element column here</li>';
        if(!empty($saved_options['child']))
        {
            foreach ($saved_options['child'] as $elements) {
                if(empty($this->elements[$elements['element']]))
                {
                    continue;
                }
                $element_conf = $this->elements[$elements['element']];
                $this->build_element_item('appended', $element_conf, $elements['options'], uniqid('element_'));
            }
        }
        echo '</ul>';
        ?>
        <script type="text/html" class="fake-column-template">
            <?php
            $element_conf = $this->elements['mbuilder-fake-column-element'];
            $this->build_element_item('appended', $element_conf, array());
            ?>
        </script>
        <?php
    }
    function _FE_column($type, $saved_options, $conf){
        $row_class = apply_filters('msp_mhp_column_row_class', 'mhp-column-row');

        $elements = $this->get_elements();

        echo sprintf(
            '<div class="%s" %s>'
            ,$row_class
            ,isset($conf['options']['id']) ? 'id="'.$conf['options']['id'].'"' : ''
        );
        
        do_action('msp_mhp_column_before_column', $type);

        foreach ($saved_options['child'] as $i => $element) 
        {
            $index = $i + 1;
            $column_class = apply_filters(
                'msp_mhp_column_class'
                ,sprintf(
                    'mhp-column-item column-item-%s'
                    ,$index
                )
                ,$element
                ,$index
                ,$type
            );

            $element_conf               = $elements[$element['element']];

            $element_conf['options']    = $element['options'];

            $element_conf['is_column']  = $index;

            printf('<div class="%s">', $column_class);
            
            $this->_element_render('column-child', $element_conf);

            echo '</div>';
        }
        
        do_action('msp_mhp_column_after_column', $type);

        echo '</div>';
    }
    function GUI_column_two($index, $saved_options = array()){
        $this->_GUI_column('mbuilder-column-two', $index, $saved_options);
    }
    function GUI_column_three($index, $saved_options = array()){
        $this->_GUI_column('mbuilder-column-three', $index, $saved_options);
    }
    function FE_column_two($saved_options, $id, $conf){
        $this->_FE_column('mbuilder-column-two', $saved_options, $conf);
    }
    function FE_column_three($saved_options, $id, $conf){
        $this->_FE_column('mbuilder-column-three', $saved_options, $conf);
    }
    function fake_column($index){
        if(mikro()->is_admin())
        {
            printf('<input type="text" value="1" name="mhp[%s][mbuilder-fake-column-element][mark]"/>', $index);

        }
    }
}