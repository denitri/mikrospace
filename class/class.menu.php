<?php
/**
 * Engine file of microsite builder own menu manager. 
 *
 * @author Deni Tri Hartanto <2denitri@gmail.com>
 * @version 2.0
 */
class msp_menu_manager{

    function __construct(){
        $this->integrate();
    }

    /**
     * Integrate functionality
     */
    function integrate(){
        add_action('admin_enqueue_scripts', array($this, 'assets'));
        add_action('pre_get_posts',array($this,'limit_objects'),9);
        add_action('pre_get_terms',array($this,'limit_objects'),9);
        add_filter('user_has_cap', array($this,'capability'), 10, 3);
        add_action('msp_admin_init',array($this,'box'),9);
        add_action('msp_microsite_loaded', array($this,'microsite_integrate'));
    }

    /**
     * Start function that only need to run in microsite environment
     */
    function microsite_integrate(){
        add_action('admin_init',array($this,'process'),99);
    }

    /**
     * Is current nav-menus.php environment editable
     */
    function is_editable_env(){
        if(wp_doing_ajax())
        {
            $referrer = $_SERVER['HTTP_REFERER'];

            $querystrings = wp_parse_url($referrer);

            $current_user = wp_get_current_user();

            if(!empty($querystrings['query']))
            {
                wp_parse_str($querystrings['query'], $get);

                return (
                    in_array('mbuilder-editor',$current_user->caps) OR in_array('editor',$current_user->caps) OR in_array('administrator',$current_user->caps)
                    AND
                    strpos($referrer, 'nav-menus.php') > 0
                    AND
                    !empty($get['mbuilder'])
                );
            }
        }
        else if(is_admin())
        {
            $current_user = wp_get_current_user();

            return (
                (
                    (in_array('mbuilder-editor',$current_user->caps) OR in_array('editor',$current_user->caps) OR in_array('editor',$current_user->caps) OR in_array('administrator',$current_user->caps))
                    OR
                    (isset($current_user->caps['mbuilder-editor']) OR isset($current_user->caps['editor']) OR isset($current_user->caps['editor']) OR isset($current_user->caps['administrator']))

                )
                AND
                strpos($_SERVER['PHP_SELF'], 'nav-menus.php') > 0
                AND
                mikro()->is_admin()
            );
        }
        else
        {
            return false;
        }

        return false;

    }

    /**
     * Filter objects to show only microsite owned
     * 
     * @todo apply limit on other taxonomy (currently only check on category) 
     */
    function limit_objects($query){
        global $current_user;

        if(!$this->is_editable_env())
        {
            return $query;
        }

        if(wp_doing_ajax())
        {
            $referrer = $_SERVER['HTTP_REFERER'];

            $querystrings = wp_parse_url($referrer);

            if(!empty($querystrings['query']))
            {
                wp_parse_str($querystrings['query'], $get);

                $cat = msp()->get_microsite_cat($get['mbuilder']);
            }
        }
        else
        {
            $cat = msp()->get_microsite_cat(msp()->get_microsite_id());
        }

        switch(current_action())
        {
            case 'pre_get_terms':
                if(!empty($query->query_vars['taxonomy'][0]))
                {
                    switch($query->query_vars['taxonomy'][0]) 
                    {
                        case 'category':
                            if(!isset($cat))
                            {   
                                return $query->set('offset', 999999999);
                            }
                            $query->query_vars['child_of'] = $cat;
                            break;
                        default:
                            return $query;
                    }
                }
                break;
            case 'pre_get_posts':
                if(!function_exists('get_current_screen'))
                {
                    return;
                }
                $screen = get_current_screen(  );
                
                if(mikro()->is_admin() && $screen->base == 'nav-menus' && !empty(msp()->get_microsite_id()))
                {
                    if($query->get('post_type') != 'nav_menu_item')
                    {
                        $meta_query = $query->get('meta_query');
                        if(empty($meta_query))
                        {
                            $meta_query = array();
                        }
                        $meta_query[] = array(
                            'key'       => '_mbuilder_updated'
                            ,'value'    => msp()->get_microsite_id()
                        );
                        $query->set('meta_query', $meta_query);
                    }
                }
                
                switch($query->get('post_type'))
                {
                    case 'page':
                        return $query->set('offset', 9999999999);
                        break;
                    case 'post':
                        
                        if(!isset($cat))
                        {   
                            return $query->set('offset', 999999999);
                        }
                        $query->set('cat', $cat);
                        break;
                }
                break;
        }
        
        

    }

    /**
     * Print styles
     */
    function assets(){
        if($this->is_editable_env()){
            wp_enqueue_style('mbuilder-menu-editor', msp()->plugin_url . '/view/assets/css/view/menu-editor.css');
        }
    }

    /**
     * Modify capabilty to grant access to menu editor page
     */
    function capability($allcaps, $cap, $args){
        if(isset($allcaps['edit_theme_options']) && $allcaps['edit_theme_options'] == false)
        {
            if(isset($cap[0]) && $cap[0] == 'edit_theme_options')
            {
                if($this->is_editable_env())
                {
                    if(wp_doing_ajax())
                    {
                        $referrer = $_SERVER['HTTP_REFERER'];

                        $querystrings = wp_parse_url($referrer);

                        if(!empty($querystrings['query']))
                        {
                            wp_parse_str($querystrings['query'], $get);

                            $microsite_id   = $get['mbuilder'];
                            $menu_id        = $get['menu'];
                        }
                    }
                    else
                    {
                        $microsite_id   = msp()->get_microsite_id();
                        $menu_id        = $_GET['menu'];
                    }

                    

                    if(!isset($microsite_id))
                    {
                        return $allcaps;
                    }

                    $microsite_menus = get_post_meta($microsite_id, 'menus', true);
                    
                    if(empty($microsite_menus))
                    {
                        $microsite_menus = array();
                    }

                    $is_microsite_menu = array_search($menu_id, $microsite_menus);

                    if(!empty($is_microsite_menu))
                    {
                        $allcaps[$cap[0]] = true;
                    }
                }
            }
        }
        return $allcaps;
    }

    /**
     * Register Box
     */
    function box(){
        mikro()->add_menu_page( 
            'Menus'
            ,'Menus'
            ,'manage-microsite'
            ,'menus'
            , function($microsite_id){
                wp_enqueue_style('mbuilder-menu-editor', msp()->plugin_url . '/view/assets/css/view/menu-editor.css');
                msp_render('/view/modules/menu-editor.php', array('microsite_id' => $microsite_id));
            }
            ,'<span class="dashicons the-icon dashicons-welcome-widgets-menus"></span>'
            ,'Appearance'
            ,30
        );
    }

    /**
     * Catch process request
     */
    function process(){
        msp_process_api()->process_by_link(
            array($this,'menu_create')
            ,'mbuilder-menu-create'
            ,'Menu Create'
            ,'Unable to save menu'
            ,'publish-microsite'
        );
    }

    /**
     * Create microsite menu on specific menu location
     */
    function menu_create(){
        $query_to_remove = array('location');

        if(empty($_GET['location']))
        {
            return array(
                'status'            => false
                ,'message'          => 'Invalid arguments sent'
                ,'query_to_remove'  => $query_to_remove
            );
        }

        $location       = $_GET['location'];

        $locations      = mikro()->get_registered_nav_menus();

        if(empty($locations[$location]))
        {
            return array(
                'status'            => false
                ,'message'          => 'Menu locations not found'
                ,'query_to_remove'  => $query_to_remove
            );
        }

        $location_name  = $locations[$location];

        $microsite      = msp()->get_microsite(msp()->get_microsite_id());

        $menu_name = 'Microsite ' . $microsite->post_title . ' ' . $location_name;

        $is_exists = term_exists( $menu_name, 'nav_menu' );

        if(!empty($is_exists))
        {
            return array(
                'status'    => false
                ,'message'  => sprintf('Menu with name "%s" already exists, consider delete it first',$menu_name)
                ,'query_to_remove'  => array('location')
            );
        }

        $saving = wp_insert_term($menu_name,'nav_menu');

        if(is_wp_error( $saving ))
        {
            return array(
                'status'            => false
                ,'message'          => $saving->get_error_message()
                ,'query_to_remove'  => array('location')
            );
        }
        else
        {

            $microsite_menus = get_post_meta(msp()->get_microsite_id(), 'menus', true);

            if(empty($microsite_menus))
            {
                $microsite_menus = array();
            }

            $microsite_menus[$location] = $saving['term_id'];

            update_post_meta(msp()->get_microsite_id(), 'menus', $microsite_menus);

            return array(
                'status'            => true
                ,'query_to_remove'  => array('location')
            );
        }
    }
}