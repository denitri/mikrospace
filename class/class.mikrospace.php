<?php
/**
 * Engine file of microsite builder. 
 *
 * @author Deni Tri Hartanto <2denitri@gmail.com>
 * @version 2.0
 */
class mikrospace{

    /**
     * @var mikrospace $instance class instance
     */
    static $instance;

    /**
     * @var array $user_capabilites wp-admin user capability
     */
    var $user_capabilites = array(
        'manage-microsite'                      => true
        ,'manage-microsites'                    => true
        ,'publish-microsite'                    => true
        ,'delete-microsite'                     => true
        ,'delete-microsites'                    => true
    );

    /**
     * @var string $post_type the microsite custom post post type
     */
    var $post_type = 'microsite-builded';

    /**
     * @var string $plugin_url plugin url
     */
    var $plugin_url = MSP_PLUGIN_URL;

    /**
     * @var string $plugin_dir plugin path
     */
    var $plugin_dir = MSP_PLUGIN_DIR;

    /**
     * @var string $themes_url microsite themes base url
     */
    var $themes_url;

    /**
     * @var string $themes_dir microsite themes base dir
     */
    var $themes_dir;

    /**
     * Class constructor
     */
    function __construct(){
        $this->themes_url = content_url('themes');

        $this->themes_dir = WP_CONTENT_DIR . '/themes';
    }

    /**
     * Get class instance
     *
     * @since 1.0 introduced
     * @return mikrospace the class instance
     */
    public static function get_instance(){
        if(self::$instance === null)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Microsite integrate functionality to parent site
     */
    function integrate(){

        /**
         * Activation & Deactivation
         */
        add_action('admin_init', array($this,'activation'));
        add_action('admin_init', array($this,'microsite_v1_integration'));
        add_filter('dmddb_main_button', array($this,'microsite_v1_integration'),99);
        add_filter('admin_footer_text', array($this, 'admin_footer_text'));
        register_activation_hook(MSP_PLUGIN_FILE, array($this,'activation'));
        register_deactivation_hook(MSP_PLUGIN_FILE, array($this,'deactivation'));

        /**
         * Post Type
         */
        $mikrospace_post_object = new msp_post_type();

        /**
         * Environment isolation & function start
         */
        add_action('plugins_loaded',array($this,'preparing_environment'),10);
        add_filter('redirect_canonical', array($this, 'redirect_canonical'), 10, 2);
        add_action('msp_plugin_load', array($this,'start_plugins'),10);
        add_action('msp_theme_load', array($this,'start_theme'),10);
        add_action('template_redirect',array($this,'microsite_post_redirect'),10);
        add_action('current_screen',array($this,'microsite_post_redirect'),10);
        add_action('admin_enqueue_scripts', array($this, 'assets'));
        add_filter('login_redirect', array($this, 'login_redirect'), 11,3 );
        add_action('admin_init', array($this, 'dashboard_redirect'), 11,3 );

        /**
         * Process API
         */
        msp_process_api()->integrate();

        /**
         * Rewrite Rule
         */
        msp_rewrite::get_instance()->integrate();

        /**
         * Menu manager
         */
        new msp_menu_manager();

        /**
         * Homepage Builder
         */
        msp_home_builder::get_instance()->integrate();

        /**
         * Run default modules
         */
        $microsite_own_modules = new msp_modules($mikrospace_post_object);
        add_action('init', array($microsite_own_modules,'integrate'),10);
        add_action('msp_plugin_load', array($microsite_own_modules,'microsite_integrate'),11);
    }

    function admin_footer_text($text){

        return $text . sprintf(' <span id="msp-footer-thankyou">This microsite powered by <a href="%s">Mikrospace</a>.</span>','http://mikrospace.wordpress.com');
    }

    /**
     * Redirect WP-admin to microsite admin
     */
    function dashboard_redirect(){
        $microsites = $this->get_microsites(array(
            'order'     => 'DESC'
            ,'orderby'  => 'date'
        ));

        $uri = untrailingslashit($_SERVER['REQUEST_URI']);
        
        if(is_multisite())
        {
            if(is_subdomain_install())
            {
                $uri = preg_replace('/^\//', '', $uri);
            }
            else
            {
                $siteurl_fragments  = explode('/',str_replace('http://','',  get_option( 'siteurl' )));
                array_shift($siteurl_fragments);
                $uri                = preg_replace('/^\/'.implode('\/',$siteurl_fragments) . '\/'.'/','', $uri);
                $uri                = ltrim( $uri, '/\\' );
            }
        }

        if(empty($uri))
        {
            return;
        }

        // CHECK IS THIS URI IS MICROSITE
        foreach($microsites as $microsite)
        {
            if(empty($microsite['MicrositePath']))
            {
                continue;
            }
            if(preg_match("/^".(str_replace('/', '\/', $microsite['MicrositePath']))."(?:\/$|$|\/)/i", $uri ) > 0)
            {
                $login_redirect_url = apply_filters('msp_login_redirect', admin_url('/edit.php?post_type='.$this->get_post_type().'&mbuilder='.$microsite['ID'].'&page=mbuilder-dashboard'));
                wp_redirect($login_redirect_url);
                exit;
            }
        }
    }

    /**
     * Redirect WP-admin request before login
     */
    function login_redirect($redirect_to, $requested_redirect_to, $user){
        $microsites = $this->get_microsites(array(
            'order'     => 'DESC'
            ,'orderby'  => 'date'
        ));

        $url = parse_url($requested_redirect_to);

        if(!empty($url['query']))
        {
            parse_str($url['query'], $query_strings);

            if(!empty($query_strings['post_type']) AND !empty($query_strings['mbuilder']) AND $query_strings['post_type'] == $this->get_post_type())
            {
                foreach($microsites as $microsite)
                {
                    if($query_strings['mbuilder'] == $microsite['ID'])
                    {
                        $login_redirect_url = apply_filters('msp_login_redirect', admin_url('/edit.php?post_type='.$this->get_post_type().'&mbuilder='.$microsite['ID'].'&page=mbuilder-dashboard'));

                        return $login_redirect_url;
                    }
                }
            }
        }

        $uri = untrailingslashit($url['path']);        
        
        if(is_multisite())
        {
            if(is_subdomain_install())
            {
                $uri = preg_replace('/^\//', '', $uri);
            }
            else
            {
                $siteurl_fragments  = explode('/',str_replace('http://','',  get_option( 'siteurl' )));
                array_shift($siteurl_fragments);
                $uri                = preg_replace('/^\/'.implode('\/',$siteurl_fragments) . '\/'.'/','', $uri);
                $uri                = ltrim( $uri, '/\\' );
            }
        }


        if(empty($uri))
        {
            return $redirect_to;
        }

        // CHECK IS THIS URI IS MICROSITE
        foreach($microsites as $microsite)
        {
            if(empty($microsite['MicrositePath']))
            {
                continue;
            }
            if(preg_match("/^".(str_replace('/', '\/', $microsite['MicrositePath']))."(?:\/$|$|\/)/i", $uri ) > 0)
            {
                $login_redirect_url = apply_filters('msp_login_redirect', admin_url('/edit.php?post_type='.$this->get_post_type().'&mbuilder='.$microsite['ID'].'&page=mbuilder-dashboard'));

                return $login_redirect_url;
            }
        }

        return $redirect_to;
    }

    function assets(){
        if(isset($_GET['post_type']) AND $_GET['post_type'] == $this->get_post_type())
        {
            wp_enqueue_style( 'mbuilder-assets', $this->plugin_url .'/view/assets/css/mbuilder.css' );
        }
    }

    /**
     * Get current viewing microsite ID, both in dashboard area / frontend area
     * 
     * @return int microsite ID
     */
    function get_microsite_id(){
        if(mikro()->wp_doing_ajax())
        {
            return $_REQUEST['mbuilder_ajax'];
        }
        else if(mikro()->is_admin())
        {
            return $_GET['mbuilder'];
        }
        else if(mikro()->is_microsite())
        {
            return wp_cache_get('microsite_id', 'mbuilder');
        }
        else
        {
            return 0;
        }
    }

    /**
     * Delete persistent file cache
     */
    function delete_file_cache($cache_name){
        $upload_dir = wp_upload_dir();
        
        $caches_dir = $upload_dir['basedir'] . '/mikrospace';
        if(file_exists($caches_dir))
        {
            $cache_file = $caches_dir . '/' . $cache_name;

            if(file_exists($cache_file))
            {
                unlink($cache_file);
            }
        }
    }

    /**
     * Set file cache
     */
    function set_file_cache($cache_name, $value){
        $upload_dir = wp_upload_dir();
        
        $caches_dir = $upload_dir['basedir'] . '/mikrospace';
        if(!file_exists($caches_dir))
        {
            mkdir($caches_dir,0755);
        }

        $cache_file = $caches_dir . '/' . $cache_name;

        file_put_contents($cache_file, serialize($value));
    }

    /**
     * Get cache from a persistent file
     */
    function get_file_cache($cache_name){
        $upload_dir = wp_upload_dir();
        
        $caches_dir = $upload_dir['basedir'] . '/mikrospace';
        if(!file_exists($caches_dir))
        {
            mkdir($caches_dir,0755);
        }

        $cache_file = $caches_dir . '/' . $cache_name;
        if(!file_exists($cache_file))
        {
            return false;
        }
        else
        {
            return unserialize(stripslashes(file_get_contents($cache_file)));
        }
    }

    /**
     * Emulate normal user visit in dashboard
     */
    function emulate_dashboard_visit($dashboard_path){
        $cookies = array();
        
        foreach ($_COOKIE as $name => $value)
        {
            $cookies[] = new WP_Http_Cookie( array( 'name' => $name, 'value' => $value ) );
        }

        return wp_remote_get(admin_url($dashboard_path), array( 'cookies' => $cookies ));
    }

    /**
     * Get microsites
     * 
     * @return array list of microsites
     */
    function get_microsites($args = array()){
        $cache_file     = 'msp_get_microsite_lists';
        $transient      = $this->get_file_cache($cache_file);

        if($transient === false)
        {
            $transient = array();
        }
        $cache_key      = base64_encode(serialize($args));
        $cache          = isset($transient[$cache_key]) ? $transient[$cache_key] : array();
        if(empty($cache))
        {
            $microsites     = array();
            $raw_microsites = get_posts( wp_parse_args($args, array(
                'post_type'         => $this->get_post_type()
                ,'posts_per_page'   => -1
                ,'order'            => 'ASC'
                ,'orderby'          => 'title' 
                ,'post_status'      => 'publish'
            )),ARRAY_A);

            foreach ($raw_microsites as $index => $microsite) 
            {
                $microsites[$index] = array(
                    'ID'                => $microsite->ID
                    ,'post_date'        => $microsite->post_date
                    ,'post_title'       => $microsite->post_title
                    ,'post_content'     => $microsite->post_content
                    ,'post_name'        => $microsite->post_name
                    ,'MicrositePath'    => get_post_meta($microsite->ID, '_msp_path', true)
                );
            }

            $transient[$cache_key] = $microsites;

            $this->set_file_cache($cache_file, $transient);
        }
        else
        {
            $microsites = $cache;
        }

        return $microsites;
    }

    /**
     * Get microsite by its ID
     * 
     * @param int $microsite_id the microsite ID
     * @return microsite the microsite object
     */
    function get_microsite($microsite_id){
        $cache_key  = 'microsite-' . $microsite_id;
        $cache = wp_cache_get( $cache_key, 'mbuilder' );

        if(empty($cache))
        {
            $microsite_object = get_post( $microsite_id );

            if($microsite_object->post_status == 'publish')
            {
                $microsite = json_decode(json_encode(array(
                    'ID'            => $microsite_object->ID
                    ,'post_title'   => $microsite_object->post_title
                    ,'post_name'    => $microsite_object->post_name
                    ,'post_date'    => $microsite_object->post_date
                    ,'post_author'  => $microsite_object->post_author
                    ,'post_content' => $microsite_object->post_content
                    ,'post_status'  => $microsite_object->post_status
                ),FALSE));

                wp_cache_add( $cache_key, $microsite, 'mbuilder' );
            }
            else
            {
                return null;
            }
        }
        else
        {
            $microsite = $cache;
        }

        return $microsite;
    }

    /**
     * Get microsite path by microsite ID
     * 
     * @param int $microsite_id the microsite ID
     * @return string the microsite path
     */
    function get_microsite_path($microsite_id){
        $cache_key  = 'microsite-path-' . $microsite_id;
        $cache = wp_cache_get( $cache_key, 'mbuilder' );

        if(empty($cache))
        {
            $path = untrailingslashit(get_post_meta($microsite_id, '_msp_path', true));

            wp_cache_add( $cache_key, $path, 'mbuilder' );
        }
        else
        {
            $path = $cache;
        }

        return $path;
    }

    /**
     * Get theme information from the style file
     * 
     * @param string $theme theme folder name
     * @return array theme information
     */
    function get_theme_info($theme){
        $cache_key = 'get-theme-info-' . $theme;

        $cache = wp_cache_get($cache_key, 'mbuilder');
        if(!empty($cache))
        {
            return $cache;
        }

        $themes_dir = msp()->get_themes_dir();
        $themes_url = msp()->get_themes_url();

        $theme_data = get_file_data($themes_dir . "/$theme/style.css", array(
            'ThemeName'         => 'Theme Name'
            ,'ThemeURI'         => 'Theme URI'
            ,'Description'      => 'Description'
            ,'Version'          => 'Version'
            ,'Author'           => 'Author'
            ,'AuthorURI'        => 'Author URI'
            ,'License'          => 'License'
            ,'LicenseURI'       => 'License URI'
            ,'Tags'             => 'Tags'
            ,'TextDomain'       => 'Text Domain'
            ,'Template'         => 'Template'
            ,'MikroSpace'        => 'MikroSpace'
        ));

        $theme_data = wp_parse_args( $theme_data, array(
            'Path'              => $theme
            ,'Preview'          => ''
            ,'ThemePath'        => $themes_dir . "/$theme"
            ,'ThemeURL'         => $themes_url . "/$theme"
            ,'MikroSpace'        => ''
        ));

        if(file_exists($themes_dir . "/$theme/screenshot.jpg"))
        {
            $theme_data['Preview'] = $themes_url . "/$theme/screenshot.jpg";
        }

        wp_cache_add($cache_key, $theme_data, 'mbuilder');

        return $theme_data;
    }

    /**
     * Get microsite active theme
     * 
     * @param int $microsite_id microsite ID to get theme information
     */
    function get_microsite_theme($microsite_id){
        $cache_key  = 'get-microsite-theme-' . $microsite_id;
        $cache      = wp_cache_get($cache_key, 'mbuilder');

        if(!empty($cache))
        {
            return $cache;
        }

        $themes_dir     = msp()->get_themes_dir();

        $active_theme   = get_post_meta( $microsite_id, '_mbuilder_theme', true );

        $theme_location = $themes_dir . '/' . $active_theme;

        if(!file_exists($theme_location))
        {
            delete_post_meta($microsite_id, '_mbuilder_theme');
            wp_die(
                sprintf(
                    'The theme can not be found, Microsite theme will set to none, <a href="%s">Back to Microsite</a>'
                    ,admin_url(
                        sprintf(
                            '/edit.php?post_type=%1$s&mbuilder=%2$s&page=mbuilder-dashboard'
                            ,$this->get_post_type()
                            ,$microsite_id
                        )
                    )
                )
            );
            exit;
        }

        if(!empty($active_theme))
        {
            $theme = $this->get_theme_info($active_theme);
            wp_cache_add($cache_key, $theme, 'mbuilder');
            return $theme;
        }

        wp_cache_add($cache_key, array(), 'mbuilder');
        return array();
    }

    /**
     * Get microsite active category
     * 
     * @param int $microsite_id microsite ID to get theme information
     * @param boolean $get_cat_object whether to return full category object or just the ID
     * 
     * @return int|array the category ID or category object
     */
    function get_microsite_cat($microsite_id, $get_cat_object = false){
        $cache_key = 'get-microsite-cat-' . $microsite_id . '-' . $get_cat_object;
        $cache = wp_cache_get($cache_key, 'mbuilder');
        if(!empty($cache))
        {
            return $cache;
        }

        $cat_id = get_post_meta( $microsite_id, '_mbuilder_category', true );

        $cat = get_category($cat_id);

        if(empty($cat))
        {
            wp_cache_add($cache_key, 0, 'mbuilder');
            return 0;
        }
        else
        {
            wp_cache_add($cache_key, $cat_id, 'mbuilder');
            return $cat_id;
        }
    }

    /**
     * Get microsite themes base dir
     * 
     * @return string the themes directory
     */
    function get_themes_dir(){
        return apply_filters( 'mbuilder_themes_dir', $this->themes_dir );
    }

    /**
     * Get microsite themes base URL
     * 
     * @return string the themes directory
     */
    function get_themes_url(){
        return apply_filters( 'mbuilder_themes_url', $this->themes_url );
    }

    /**
     * Compromise with microsite V2
     */
    function microsite_v1_integration(){
        switch(current_filter())
        {
            case 'dmddb_main_button':
                $buttons = func_get_arg(0);

                foreach ($buttons as &$button) 
                {
                    if($button['id'] == 'dmd-event-microsite')
                    {
                        $button['title'] = 'Microsites (Legacy)';
                    }
                }

                return $buttons;
                break;
            case 'admin_init':
                global $menu;

                if(!empty($menu) && is_array($menu))
                {
                    foreach ($menu as &$menu_item) 
                    {
                        if(isset($menu_item[5]) && $menu_item[5] == 'toplevel_page_demst-microsite')
                        {
                            $menu_item[0] = 'Microsites (Legacy)';
                        }
                    }
                }
        

                break;
        }
    }

    /**
     * Function that called when plugin activated
     */
    function activation(){
        if(!get_role('mbuilder-editor'))
        {
            // adding capability to microsite-editor role
            add_role( 'mbuilder-editor', 'Microsite-Editor', wp_parse_args(array(
                'read'                              => true
                ,'upload_files'                     => true
                ,'edit_theme_options'               => false
                ,'publish-microsite'                => false
                ,'delete-microsite'                 => false
                ,'edit_posts'                       => false
                ,'publish_posts'                    => false
                ,'edit_published_posts'             => false
                ), array(
                    'manage-microsite'                      => true
                    ,'manage-microsites'                    => false
                    ,'publish-microsite'                    => false
                    ,'delete-microsite'                     => false
                    ,'delete-microsites'                    => false
                ))
            );

            // adding capability to editor role
            $editor_role = get_role('editor');
            $editor_role->add_cap('manage-microsite', true);
            $editor_role->add_cap('manage-microsites', false);
            $editor_role->add_cap('publish-microsite', false);
            $editor_role->add_cap('delete-microsite', false);
            $editor_role->add_cap('delete-microsites', false);

            // adding capability to administrator role
            $admin_role = get_role('administrator');
            foreach ($this->user_capabilites as $capability => $grant)
            {
                $admin_role->add_cap($capability);
            }
        }
    }

    /**
     * Function that called when plugin deactivated
     */
    function deactivation(){
        if(get_role('mbuilder-editor'))
        {
            remove_role( 'mbuilder-editor' );

            // removing capability from editor role
            $editor_role = get_role('editor');
            foreach ($this->user_capabilites as $capability => $grant)
            {
                $editor_role->remove_cap($capability);
            }

            // removing capability from administrator role
            $admin_role = get_role('administrator');
            foreach ($this->user_capabilites as $capability => $grant)
            {
                $admin_role->remove_cap($capability);
            }
        }
    }

    /**
     * Get microsite post type
     * 
     * @return string the post type name
     */
    function get_post_type(){
        return $this->post_type;
    }

    /**
     * Start microsite plugin functionality
     */
    function start_plugins($microsite){
        $activated_plugins = msp_microsite_meta($microsite->ID, '_plugins', array());
    }

    /**
     * Start microsite theme functionality
     */
    function start_theme($microsite){
        $theme = $this->get_microsite_theme($microsite->ID);

        if(empty($theme))
        {
            // no theme activated
            return false;
        }
        
        $themes_dir = msp()->get_themes_dir();

        $theme_function = $themes_dir . "/" . $theme['Path'] . '/functions.php';

        // load child theme functions
        if(file_exists($theme_function))
        {
            require_once $theme_function;
        }
        
        // load parent theme function if exists
        if(!empty($theme['Template']))
        {
            $theme_template_function = $themes_dir . "/" . $theme['Template'] . '/functions.php';
            
            if(file_exists($theme_template_function))
            {
                require_once $theme_template_function;
            }
        }
    }

    /**
     * Bootstraping microsite plugin & theme function in frontend area
     */
    function frontend_bootstraping($microsite){
        $microsite_id   = $microsite['ID'];
        $microsite      = $this->get_microsite( $microsite_id );
        wp_cache_add('microsite_id', $microsite_id, 'mbuilder');
        wp_cache_add('microsite', $microsite, 'mbuilder');

        /**
         * Fire microsite plugins functionality
         */
        do_action('msp_plugin_load', $microsite);

        /**
         * Fire microsite theme functionality
         */
        do_action('msp_theme_load', $microsite);

        /**
         * Fire microsite init phase
         */
        do_action('msp_init', $microsite);

        /**
         * Fire when all functions is loaded
         */
        do_action('msp_microsite_loaded', $microsite);
    }

    /**
     * Bootstraping microsite plugin & theme function
     */
    function bootstraping(){
        if(mikro()->wp_doing_ajax())
        {
            $microsite = $this->get_microsite( $this->get_microsite_id() );

            /**
             * Fire microsite plugins functionality
             */
            do_action('msp_plugin_load', $microsite);

            /**
             * Fire microsite theme functionality
             */
            do_action('msp_theme_load', $microsite);

            /**
             * Fire microsite init phase
             */
            do_action('msp_init', $microsite);

            /**
             * Start ajax only function
             */
            do_action('msp_admin_ajax_init', $microsite);

            /**
             * Start function
             */
            do_action('msp_admin_init', $microsite);

            /**
             * Fire when all functions is loaded
             */
            do_action('msp_microsite_loaded', $microsite);

        }
        else if(mikro()->is_admin())
        {
            $microsite = $this->get_microsite( $this->get_microsite_id() );

            /**
             * Fire microsite plugins functionality
             */
            do_action('msp_plugin_load', $microsite);

            /**
             * Fire microsite theme functionality
             */
            do_action('msp_theme_load', $microsite);

            /**
             * Fire microsite init phase
             */
            do_action('msp_init', $microsite);

            /**
             * Start function
             */
            do_action('msp_admin_init', $microsite);

            /**
             * Fire when all functions is loaded
             */
            do_action('msp_microsite_loaded', $microsite);
        }
        else
        {

        }
    }

    /**
     * Redirect microsite post in host site into its microsite URL, hook into template_redirect
     */
    function microsite_post_redirect(){
        switch(current_action())
        {
            case 'current_screen':
                
                $screen = get_current_screen();

                if($screen->base == 'post' AND $screen->post_type == 'post' AND $screen->action != 'add' AND $screen->is_network == false and !empty($_GET['post']))
                {
                    $post_id = $_GET['post'];
                    $is_microsite_post = get_post_meta($post_id, '_mbuilder_updated', true);

                    if(!empty($is_microsite_post))
                    {
                        $microsite = $this->get_microsite($is_microsite_post);

                        if(!empty($microsite) AND $microsite->post_status != 'trash')
                        {
                            $post_edit_redirect_url = apply_filters('msp_post_edit_redirect', admin_url('/edit.php?post_type='.$this->get_post_type().'&mbuilder='.$microsite->ID.'&page=mbuilder-dashboard&subpage=post&edit=' . $post_id));
    
                            wp_redirect($post_edit_redirect_url);
                            exit;
                        }

                    }
                }
                

                break;
            case 'template_redirect':
                if(is_singular('post') AND !mikro()->is_microsite() AND !is_admin())
                {
                    $is_microsite_post = get_post_meta(get_the_ID(), '_mbuilder_updated', true);

                    
                    if(!empty($is_microsite_post))
                    {
                        $microsite = $this->get_microsite($is_microsite_post);
                        if(!empty($microsite))
                        {
                            global $post;
                            
                            wp_cache_add('microsite_id', $is_microsite_post, 'mbuilder');                    
                            $microsite_post_url = apply_filters('mbuilder_post_at_host_redirect_url', mikro()->get_permalink($post));
                            
                            wp_redirect($microsite_post_url);
                            exit;
                        }
                    }
                }
                break;
        }
    }

    /**
     * Disable wordpress canonical function on microsite URL
     */
    function redirect_canonical($redirected, $requested){

        $microsites = $this->get_microsites(array(
            'order'     => 'DESC'
            ,'orderby'  => 'date'
        ));


        $uri = str_replace(home_url(),'', untrailingslashit($requested)) ;            
        
        if(is_multisite())
        {
            if(is_subdomain_install())
            {
                $uri = preg_replace('/^\//', '', $uri);
            }
            else
            {
                $siteurl_fragments  = explode('/',str_replace('http://','',  get_option( 'siteurl' )));
                array_shift($siteurl_fragments);
                $uri                = preg_replace('/^\/'.implode('\/',$siteurl_fragments) . '\/'.'/','', $uri);
                $uri                = ltrim( $uri, '/\\' );
            }
        }        

        if(empty($uri))
        {
            return;
        }

        // CHECK IS THIS URI IS MICROSITE
        
        foreach($microsites as $microsite)
        {
            if(empty($microsite['MicrositePath']))
            {
                continue;
            }
            if(preg_match("/^".(str_replace('/', '\/', $microsite['MicrositePath']))."(?:\/$|$|\/)/i", $uri ) > 0)
            {
                return $requested ;
            }
        }



        return $redirected;
    }

    /**
     * Setup microsite environment by disabling parent functionality and others.
     * Disabling the functionality by changing host theme template_url and stylesheet_url causing wordpress not load the theme
     */
    function preparing_environment(){
        if(is_admin())
        {
            /**
             * Set IFRAME REQUEST Condition
             */
            if(isset($_GET['IFRAME_REQUEST']))
            {
                define('IFRAME_REQUEST', true);
            }

            /**
             * admin ajax request environment, the difference between this and 
             * is_admin condition is in this environment you can include host function
             * to run on microsite environment 
             */
            if(mikro()->wp_doing_ajax())
            {
                if(!isset($_REQUEST['load-host-functions']))
                {
                    add_filter('template_directory', function(){return uniqid();});
                    add_filter('stylesheet_directory', function(){return uniqid();});
                }
    
                $this->bootstraping();
            }
            

            /**
             * Dashboard admin environment
             */
            else if(mikro()->is_admin())
            {
                add_filter('template_directory', function(){return uniqid();});
                add_filter('stylesheet_directory', function(){return uniqid();});
    
                $this->bootstraping();
            }
        }
        /**
         * Frontend microsite environment
         */
        else
        {
            $microsites = $this->get_microsites(array(
                'order'     => 'DESC'
                ,'orderby'  => 'date'
            ));
            


            $uri = untrailingslashit($_SERVER['REQUEST_URI']);            
            
            if(is_multisite())
            {
                if(is_subdomain_install())
                {
                    $uri = preg_replace('/^\//', '', $uri);
                }
                else
                {
                    $siteurl_fragments  = explode('/',str_replace('http://','',  get_option( 'siteurl' )));
                    array_shift($siteurl_fragments);
                    $uri                = preg_replace('/^\/'.implode('\/',$siteurl_fragments) . '\/'.'/','', $uri);
                    $uri                = ltrim( $uri, '/\\' );
                }
            }

            
    
            if(empty($uri))
            {
                return;
            }
    
            // CHECK IS THIS URI IS MICROSITE
            
            foreach($microsites as $microsite)
            {
                if(empty($microsite['MicrositePath']))
                {
                    continue;
                }
                if(preg_match("/^".(str_replace('/', '\/', $microsite['MicrositePath']))."(?:\/$|$|\/)/i", $uri ) > 0)
                {
                    add_filter('template_directory', function(){return uniqid();});
                    add_filter('stylesheet_directory', function(){return uniqid();});
                    return $this->frontend_bootstraping($microsite);
                }
            }
        }
    }
}
?>