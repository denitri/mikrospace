<?php
/**
 * Engine file of microsite builder own modules. 
 *
 * @author Deni Tri Hartanto <2denitri@gmail.com>
 * @version 2.0
 */
class msp_modules{

    /**
     * @var msp_post_type $msp_post_type
     */
    private $msp_post_type;

    function __construct($msp_post_type){
        $this->msp_post_type = $msp_post_type;
    }

    /**
     * Start integrating function
     */
    function integrate(){
        add_action('admin_menu', array($this, 'users_settings'));
        add_action('admin_menu', array($this, 'themes_settings'));
        add_action('admin_init', array($this, 'process'));
    }

    /**
     * Start function that only need to run in microsite environment
     */
    function microsite_integrate(){
        add_action('admin_init', array($this, 'process'));
        add_action('admin_menu', array($this, 'register_box'));
        add_action('init', array($this,'register_post'));
        
        add_filter('msp_post_object_taxonomies', array($this,'post_type_post_filters'), 10, 2);
        add_filter('mbuilder_generic_editor_object_taxonomies', array($this,'post_type_post_filters'), 10, 2);
        add_filter('mbuilder_generic_editor_terms_args', array($this,'post_type_post_filters'), 10, 3);
        add_filter('mbuilder_term_ui_list_terms_args', array($this,'post_type_post_filters'), 10, 3);
        add_filter('mbuilder_term_ui_list_terms_count_args', array($this,'post_type_post_filters'), 10, 3);
        add_filter('msp_post_term_update_args', array($this,'post_type_post_filters'), 10, 3);
        add_filter('msp_post_get_object_args', array($this,'post_type_post_filters'), 10, 3);
        add_filter('msp_post_object_taxonomies_sent', array($this,'post_type_post_filters'), 10, 3);
        
    }

    /**
     * Register microsite post
     */
    function register_post(){
        $microsite_cat = msp()->get_microsite_cat(msp()->get_microsite_id());

        if(!empty($microsite_cat))
        {
            mikro()->register_post_type('post',array(
                'label'         => 'Posts'
                ,'menu_icon'    => '<span class="dashicons the-icon dashicons-format-aside"></span>'
                ,'supports'     => array(
                    'editor','thumbnail','taxonomy'
                )
                ,'skip_register_object' => true
                ,'rewrite'              => array(
                    'has_archive'   => true
                    ,'slug'  => 'post'
                )
            )); 
        }


    }

    /**
     * Add microsite network users settings
     */
    function users_settings(){
        add_submenu_page(
            'edit.php?post_type=' . msp()->get_post_type()
            , 'Users'
            , 'Users'
            , 'manage_options'
            , 'mbuilder-users'
            , function() {
                msp_render('/view/admin/users.php');
            }
        );
    }

    /**
     * Add microsite network themes settings
     */
    function themes_settings(){
        add_action('admin_enqueue_scripts', function(){
            wp_enqueue_style('mbuilder-theme-list', msp()->plugin_url . '/view/assets/css/view/themes-list.css' );
        });
        add_submenu_page(
            'edit.php?post_type=' . msp()->get_post_type()
            , 'Themes'
            , 'Themes'
            , 'manage_options'
            , 'mbuilder-themes'
            , function() {
                msp_render('/view/admin/themes.php');
            }
        );
    }

    /**
     * Register own module
     * 
     * @var microsite $microsite microsite object
     */
    function register_box(){
    
        mikro()->add_menu_page( 
            'Setups'
            ,'Setups'
            ,'manage_options'
            ,'setups'
            , function($microsite_id){
                msp_render('/view/modules/setups.php', array('microsite_id' => $microsite_id));
            }
            ,'<span class="dashicons the-icon dashicons-admin-settings"></span>'
            ,'Settings'
            ,40
        );

        mikro()->add_menu_page( 
            'Plugins'
            ,'Plugins'
            ,'manage_options'
            ,'plugins'
            , function(){

            }
            ,'<span class="dashicons the-icon dashicons-admin-plugins"></span>'
            ,'Settings'
            ,40
        );

        mikro()->add_menu_page( 
            'Block Host Plugins'
            ,'Block Host Plugins'
            ,'manage_options'
            ,'block-host-plugins'
            , function($microsite_id){
                msp_render('/view/modules/block-host-plugins.php', array('microsite_id' => $microsite_id));
            }
            ,'<span class="dashicons the-icon dashicons-admin-plugins"></span>'
            ,'Settings'
            ,40
        );
        mikro()->add_menu_page( 
            'Users'
            ,'Users'
            ,'manage_options'
            ,'users-management'
            , function($microsite_id){
                msp_render('/view/modules/users.php', array('microsite_id' => $microsite_id));
            }
            ,'<span class="dashicons the-icon dashicons-admin-users"></span>'
            ,'Settings'
            ,40
        );

        mikro()->add_menu_page( 
            'Themes'
            ,'Themes'
            ,'publish-microsite'
            ,'themes'
            , function($microsite_id){
                msp_render('/view/modules/themes.php', array('microsite_id' => $microsite_id));
            }
            ,'<span class="dashicons the-icon dashicons-admin-appearance"></span>'
            ,'Appearance'
            ,30
        );
    }

    /**
     * Bootstrap module saving process
     */
    function process(){
        msp_process_api()->process(
            array($this,'save_settings')
            ,'mbuilder-settings-microsite'
            ,'Settings Saved'
            ,'Unable to save capabilities'
            ,'manage_options'
        );
        msp_process_api()->process(
            array($this,'save_users_roles')
            ,'mbuilder_save_users_role'
            ,'Capabilites Saved'
            ,'Unable to save capabilities'
            ,'manage_options'
        );
        msp_process_api()->process(
            array($this,'add_microsite_user')
            ,'mbuilder_add_manageable_microsite'
            ,'User added'
            ,'Unable to add user'
            ,'manage_options'
        );
        msp_process_api()->process(
            array($this,'save_configuration')
            ,'mbuilder-config-microsite'
            ,'Configuration saved'
            ,'failed to save configuration'
            ,'manage_options'
        );
        msp_process_api()->process_by_link(
            array($this,'disabling_host_plugin')
            ,'mbuilder-disable-host-plugin'
            ,'Plugin configuration saved'
            ,'failed to save configuration'
            ,'manage_options'
        );
        msp_process_api()->process_by_link(
            array($this,'activate_network_theme')
            ,'mbuilder-network-theme-activation'
            ,'Theme saved'
            ,'Unable to save theme'
            ,'manage_options'
        );
        msp_process_api()->process_by_link(
            array($this,'activate_theme')
            ,'mbuilder_module_activate_theme'
            ,'Theme saved'
            ,'Unable to save theme'
            ,'publish-microsite'
        );
    }

    /**
     * Disabling host plugin toggler
     */
    function disabling_host_plugin(){
        $query_to_remove = array('disable-host-plugins','validity');

        if(empty($_GET['disable-host-plugins']) OR empty($_GET['validity']))
        {
            return array(
                'status'    => false
                ,'message'  => 'Invalid arguments sent'
                ,'query_to_remove'  => $query_to_remove
            );
        }

        $plugin = urldecode( $_GET['disable-host-plugins'] );

        if(!wp_verify_nonce($_GET['validity'], $plugin))
        {
            return array(
                'status'    => false
                ,'message'  => 'Security check failed'
                ,'query_to_remove'  => $query_to_remove
            );
        }

        $microsite_id = msp()->get_microsite_id();

        $disabled_plugins = get_post_meta($microsite_id, 'disabled-host-plugins', true);
        if(empty($disabled_plugins))
        {
            $disabled_plugins = array();
        }

        if(isset($disabled_plugins[$plugin]))
        {
            unset($disabled_plugins[$plugin]);
        }
        else
        {
            $disabled_plugins[$plugin] = array();
        }

        update_post_meta($microsite_id, 'disabled-host-plugins', $disabled_plugins);

        return array(
            'status'    => true
            ,'query_to_remove'  => $query_to_remove
        );
    }

    /**
     * Process save configuration form
     */
    function save_configuration(){
        $post_data = apply_filters('mbuilder_microsite_settings_form_data', $_POST);

        require_once msp()->plugin_dir . '/vendor/autoload.php';

        $validator = array(
            'rules'     => array(
                'id' => array(
                    'required'
                )
                ,'validity' => array(
                    'required'
                    ,'validity-check' => function($value) use ($post_data){
                        return wp_verify_nonce($value, $post_data['id'] . '-save-config');
                    }
                )
            )
            ,'naming'   => array(
                
            )
            ,'errors'   => array(
                'validity-check'    => 'Validity check failed'
            )
        );

        $validator = apply_filters('mbuilder_microsite_config_validation_rules', $validator, $post_data);

        $validation_result = SimpleValidator\Validator::validate($post_data, $validator['rules'], $validator['naming']);

        $validation_result->customErrors($validator['errors']);

        if(!$validation_result->isSuccess())
        {
            $errors = $validation_result->getErrors();

            return array(
                'status'    => false
                ,'message'  => $errors[0]
                ,'data'     => $post_data
            );
        }
        else
        {

            $saving = apply_filters('mbuilder_microsite_config_save', true, $post_data);

            return array(
                'status'    => !empty($saving)
            );
        }
    }

    /**
     * Process microsite settings form
     */
    function save_settings(){
        $post_data = apply_filters('mbuilder_microsite_settings_form_data', $_POST);

        require_once msp()->plugin_dir . '/vendor/autoload.php';

        $validator = array(
            'rules'     => array(
                'id' => array(
                    'required'
                )
                ,'validity' => array(
                    'required'
                    ,'validity-check' => function($value) use ($post_data){
                        return wp_verify_nonce($value, $post_data['id'] . '-save-settings');
                    }
                )
                ,'post_title' => array(
                    'required'
                )
                ,'post_name' => array(
                    'required'
                )
                ,'post_content' => array(
                    'required'
                )
                ,'category'     => array(
                    'required'
                )
            )
            ,'naming'   => array(
                'post_title'    => 'Title'
                ,'post_name'    => 'Path'
                ,'post_content' => 'Tagline'
                ,'category'     => 'Category'
            )
            ,'errors'   => array(
                'validity-check'    => 'Validity check failed'
            )
        );


        $validator = apply_filters('mbuilder_microsite_settings_validation_rules', $validator, $post_data);

        $validation_result = SimpleValidator\Validator::validate($post_data, $validator['rules'], $validator['naming']);

        $validation_result->customErrors($validator['errors']);

        if(!$validation_result->isSuccess())
        {
            $errors = $validation_result->getErrors();

            return array(
                'status'    => false
                ,'message'  => $errors[0]
                ,'data'     => $post_data
            );
        }
        else
        {

            $saving = wp_update_post(array(
                'post_title'    => $post_data['post_title']
                ,'post_content' => $post_data['post_content']
                ,'ID'           => $post_data['id']
            ));

            do_action('mbuilder_microsite_settings_pre_category_change', $post_data['id'], $post_data['category']);
            
            update_post_meta($post_data['id'], '_msp_path', $post_data['post_name']);
            
            update_post_meta($post_data['id'], '_mbuilder_category', $post_data['category']);
            
            do_action('mbuilder_microsite_settings_post_category_change', $post_data['id'], $post_data['category']);

            $this->msp_post_type->refresh_microsites_cache();

            return array(
                'status'    => !empty($saving)
                ,'message'  => 'Success save settings'
            );
        }
    }

    /**
     * Save custom user capabilities
     */
    function save_users_roles(){
        $publishable_users  = isset($_POST['publishable']) ? $_POST['publishable'] : array();
        $moderate_all_users = isset($_POST['manage-all']) ? $_POST['manage-all'] : array();

        $users = get_users( array(
            'role'  => 'editor'
        ) );

        

        foreach ($users as $user) 
        {
            if(isset($publishable_users[$user->data->ID]))
            {
                $user->add_cap('publish-microsite');
                $user->add_cap('delete-microsite');
            }
            else 
            {
                $user->remove_cap('publish-microsite');
                $user->remove_cap('delete-microsite');
            }

            if(isset($moderate_all_users[$user->data->ID]))
            {
                $user->add_cap('manage-microsites');
            }
            else 
            {
                $user->remove_cap('manage-microsites');
            }
        }


        return array(
            'status'    => true
            ,'message'  => 'Success change capabilities'
        );
    }
    
    /**
     * Add low level user to manage a microsite
     *
     * Adding meta '_manageable_microsite' to every editor users, 
     * @todo create cleanup action on deactivation but with question
     */
    function add_microsite_user(){
        $manageable_users  = isset($_POST['manageable']) ? $_POST['manageable'] : array();


        $non_global_manage_users = array();
        $editors = get_users(array(
            'role__in'  => array('editor', 'mbuilder-editor') 
        ));
        foreach ($editors as $editor) 
        {
            if(empty($editor->allcaps['manage-microsites']))
            {
                array_push($non_global_manage_users, $editor);
            }
        }

        foreach($non_global_manage_users as $user) 
        {
            $manageable_sites = get_user_meta( $user->data->ID, '_manageable_microsite', true );
            if(empty($manageable_sites))
            {
                $manageable_sites = array();
            }

            if(isset($manageable_users[$user->data->ID]))
            {
                $manageable_sites[$_POST['microsite_id']] = array();
                update_user_meta( $user->data->ID, '_manageable_microsite', $manageable_sites );
            }
            else if(isset($manageable_sites[$_POST['microsite_id']]))
            {
                unset($manageable_sites[$_POST['microsite_id']]);
                update_user_meta( $user->data->ID, '_manageable_microsite', $manageable_sites );
            }
        }


        return array(
            'status'    => true
        );
    }

    /**
     * Made a theme become network available that can be set by user with permission publish-microsite
     */
    function activate_network_theme(){
        if(empty($_GET['theme_path']))
        {
            return array(
                'status'            => false
                ,'message'          => 'Unknown theme to process'
                ,'query_to_remove'  => array('theme_path', 'theme_action') 
            );
        }
        if(empty($_GET['theme_action']))
        {
            return array(
                'status'            => false
                ,'message'          => 'Unknown action to process'
                ,'query_to_remove'  => array('theme_path', 'theme_action') 
            );
        }

        $network_themes = get_option( 'mbuilder_network_themes', array() );
        if(empty($network_themes))
        {
            $network_themes = array();
        }

        if($_GET['theme_action'] == 'activate')
        {
            $network_themes[$_GET['theme_path']] = array();
        }
        else if(isset($network_themes[$_GET['theme_path']]))
        {
            unset($network_themes[$_GET['theme_path']]);
        }


        update_option( 'mbuilder_network_themes', $network_themes, false );

        return array(
            'status'            => true
            ,'message'          => 'Theme Saved'
            ,'query_to_remove'  => array('theme_path') 
        );
    }

    /**
     * Activate a theme in a microsite
     */
    function activate_theme(){
        $query_to_remove = array('theme','microsite_id','validation');

        if(empty($_GET['theme']) OR empty($_GET['microsite_id']) OR empty($_GET['validation']))
        {
            return array(
                'status'    => false
                ,'message'  => 'Invalid arguments sent'
                ,'query_to_remove'  => $query_to_remove
            );
        }

        $microsite_id   = $_GET['microsite_id'];
        $theme          = $_GET['theme'];

        if(!wp_verify_nonce( $_GET['validation'], $microsite_id . '_themes' ))
        {
            return array(
                'status'    => false
                ,'message'  => 'Bad argument sent'
                ,'query_to_remove'  => $query_to_remove
            );
        }

        $themes_dir     = msp()->get_themes_dir();

        $current_theme = msp()->get_microsite_theme( $microsite_id );
        if(!empty($current_theme))
        {
            $theme_function = $themes_dir . "/".$current_theme['Path']."/functions.php";

            if(file_exists($theme_function))
            {
                require_once $theme_function;
            }

            do_action( 'mbuilder_theme_changed', $microsite_id );

        }

        update_post_meta( $microsite_id, '_mbuilder_theme', $_GET['theme'] );


        $theme_function = $themes_dir . "/$theme/functions.php";

        if(file_exists($theme_function))
        {
            require_once $theme_function;
        }

        do_action( 'mbuilder_after_setup_theme', $microsite_id );

        return array(
            'status'    => true
            ,'message'  => 'Theme Activated'
            ,'query_to_remove'  => $query_to_remove
        );
    }

    /**
     * Modify POST UI module for normal post
     */
    function post_type_post_filters($args){
        $microsite_cat = msp()->get_microsite_cat(msp()->get_microsite_id());

        switch(current_filter())
        {
            /**
             * Filter the post type object taxonomies
             */
            case 'msp_post_object_taxonomies':
            case 'mbuilder_generic_editor_object_taxonomies':
                $post_type  = func_get_arg(1);

                if($post_type == 'post')
                {
                    $used_taxonomy = array();
                    $allowed_tax    = array('category');
                    foreach ($args as $tax) 
                    {
                        if(in_array($tax->name,$allowed_tax))
                        {
                            $used_taxonomy[$tax->name] = $tax;
                        }
                    }
                    return $used_taxonomy;
                }

                return $args;

            /**
             * Filter available category term on create post form
             */
            case 'mbuilder_generic_editor_terms_args':
                $tax        = func_get_arg(1);
                $post_type  = func_get_arg(2);

                if($tax == 'category')
                {
                    $args['child_of'] = $microsite_cat;

                    if(empty($microsite_cat))
                    {
                        $args['name'] = 'xxxx';
                    }
                }
                
                return $args;
            /**
             * Filter category term on category list
             */
            case 'mbuilder_term_ui_list_terms_args':
            case 'mbuilder_term_ui_list_terms_count_args':
                $tax        = func_get_arg(1);
                $post_type  = func_get_arg(2);

                if($tax == 'category')
                {
                    $args['child_of'] = $microsite_cat;

                    if(empty($microsite_cat))
                    {
                        $args['name'] = 'xxxx';
                    }
                }

                return $args;
            /**
             * Filter posts shown up on post list table to only post under microsite category
             */
            case 'msp_post_get_object_args':
                $post_type  = func_get_arg(1);

                if($post_type == 'post')
                {
                    $args['cat'] = $microsite_cat;
                    if(empty($microsite_cat))
                    {
                        $args['pagename'] = 'xxxxx';
                    }
                }
                
                return $args;
            /**
             * Set category parent when saving category
             */
            case 'msp_post_term_update_args':
                $term   = func_get_arg(1);
                $tax    = func_get_arg(2);

                if($tax == 'category')
                {
                    if(empty($microsite_cat))
                    {
                        wp_die('This microsite not set with any parent category, thus category creating is forbidden');
                        exit;
                    }
                    $args['parent'] = $microsite_cat;

                    $parent_cat = get_term_by('id', $microsite_cat, 'category');

                    $parent_slug = $parent_cat->slug;

                    $cat_slug = $parent_slug . '-' . sanitize_title($term['title']);

                    $args['slug'] = $cat_slug;

                }


                return $args;
            /**
             * Filter saved post data to always have main microsite category
             */
            case 'msp_post_object_taxonomies_sent':
                $post_type  = func_get_arg(1);
                
                $microsite_cat = msp()->get_microsite_cat(msp()->get_microsite_id());

                if($post_type == 'post' && !empty($microsite_cat))
                {
                    if(isset($args['category']))
                    {
                        if(!in_array($microsite_cat, $args['category']))
                        {
                            $args['category'][] = $microsite_cat;
                        }
                    }
                    else
                    {
                        $args['category'] = array($microsite_cat);
                    }
                }
                return $args;
            default:
                return $args;
        }

        return $args;

    }
}