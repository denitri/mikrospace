<?php
/**
 * Engine file of microsite builder post type. 
 *
 * @author Deni Tri Hartanto <2denitri@gmail.com>
 * @version 2.0
 */
class msp_post_type{

    function __construct(){
        /**
         * RUN !
         */
        $this->integrate();
    }

    /**
     * Microsite post type integrate functionality to parent site
     */
    function integrate(){
        add_action('init', array(&$this,'post_type'));
        add_action('add_meta_boxes', array(&$this,'post_type'));
        add_action('enter_title_here', array(&$this,'post_type'));
        add_action('post_row_actions', array(&$this,'post_type'));
        add_action('post_row_actions', array(&$this,'post_type'));
        add_action('pre_get_posts', array(&$this,'post_type'));
        add_action('current_screen', array(&$this,'post_type'));
        add_action('admin_enqueue_scripts', array(&$this,'post_type'));

        add_filter('transition_post_status', array($this,'taken_down') , 10, 3 );
        add_action('save_post_'.msp()->get_post_type(),array($this,'save_post'), 10, 3);
        add_action('wp_insert_post', array($this,'save_post') , 10, 3 );
        add_filter('wp_insert_post_data', array($this,'post_type_save') , 10, 2 );

        add_action('manage_'.msp()->get_post_type().'_posts_columns',array($this,'post_type_column'), 10, 1);
        add_filter('manage_posts_custom_column',array($this,'post_type_column'), 10, 2);

        add_action('admin_menu', array($this, 'dashboard'));
        
        add_filter('user_has_cap',array($this,'manageable_microsite'), 10 , 3);
    }

    /**
     * Refresh microsite detection and URL caches
     */
    function refresh_microsites_cache(){
        // Remove curren caches
        msp()->delete_file_cache('msp_get_microsite_lists');

        // Remove limiting hooks
        remove_action('pre_get_posts', array($this,'post_type'));

        // Refresh caches on class.mikrospace.php method login_redirect, dashboard_redirect & preparing_environment
        $microsites = msp()->get_microsites(array(
            'order'     => 'DESC'
            ,'orderby'  => 'date'
        ));

        // Refresh persistent rewrite rules
        msp_rewrite::get_instance()->refresh_persistent_rules();

        // Restore limiting hooks previously removed
        add_action('pre_get_posts', array($this,'post_type'));
    }

    /**
     * Manage save post action
     * 
     * @param int $microsite_id The microsite ID.
     * @param post $microsite The microsite object.
     * @param bool $update Whether this is an existing microsite being updated or not.
     */
    function save_post(){
        switch(current_action(  ))
        {
            case 'save_post_'.msp()->get_post_type():
                if(isset($_POST['microsite_name']))
                {
                    $microsite_id   = func_get_arg(0);
                    $microsite      = func_get_arg(1); 
                    $update         = func_get_arg(2);
                    update_post_meta($microsite_id, '_msp_path', $_POST['microsite_name']);
        
                    if(isset($_POST['microsite_theme']))
                    {
                        update_post_meta($microsite_id, '_mbuilder_theme', $_POST['microsite_theme']);
                    }
                    
                    if(!empty($_POST['microsite_category']))
                    {
                        update_post_meta($microsite_id, '_mbuilder_category', $_POST['microsite_category']);
                    }
                    else if(!empty($_POST['microsite_category_create']))
                    {
                        $insert_category = wp_insert_category(array(
                            'cat_ID'                => 0
                            ,'cat_name'             => $_POST['microsite_category_create']
                            ,'category_description' => ''
                            ,'category_nicename'    => sanitize_title($_POST['microsite_category_create'])
                            ,'category_parent'      => 0
                            ,'taxonomy'             => 'category'
                        ), true);
        
                        if(!is_wp_error($insert_category))
                        {
                            update_post_meta($microsite_id, '_mbuilder_category', $insert_category);
                        }
                    }
                }
                break;
            case 'wp_insert_post':
                $post_ID    = func_get_arg(0);
                $post       = func_get_arg(1);
                $update     = func_get_arg(2);

                /**
                 * Only execute on microsite post type
                 */
                if($post->post_type != msp()->get_post_type())
                {
                    return;
                }

                if($post->post_status != 'publish')
                {
                    return;
                }

                $this->refresh_microsites_cache();

                msp_rewrite::get_instance()->flush_microsite_activation($post_ID);

                $key = wp_create_nonce();
            
                $response = msp()->emulate_dashboard_visit(add_query_arg( 
                    array(
                        'action'                                => 'mbuilder_flush_rule'
                        ,'mbuilder_ajax'                        => $post_ID
                        ,'msp_rewrite_permalink_access'         => $key
                        ,'msp_rewrite_permalink_access_token'   => md5($key . 'msp0') 
                    )
                    ,'admin-ajax.php' 
                ));
                
                break;
        }
    }

    /**
     * Get current logged in user manageable microsite
     */
    function get_user_manageable_site(){
        $cache = wp_cache_get( 'get_user_manageable_site', 'mbuilder' );
        if(empty($cache) AND !is_array($cache))
        {
            global $wpdb;
            $own_microsites = $wpdb->get_col(
                '
                SELECT
                    p.ID
                FROM
                    '.$wpdb->posts.' p
                WHERE
                    p.post_type = "'.msp()->get_post_type().'"
                    AND
                    p.post_status IN ("publish", "draft")
                    AND
                    p.post_author = '.get_current_user_id().'
                '
            );
            $manageable_sites = get_user_meta( get_current_user_id(), '_manageable_microsite', true );
            if(empty($manageable_sites))
            {
                $manageable_sites = array();
            }

            $cache = array_merge($own_microsites, array_keys($manageable_sites));

            wp_cache_set( 'get_user_manageable_site', $cache, 'mbuilder' );
        }
        
        return $cache;

    }

    /**
     * Initialize custom post type that this microsite use
     */
    function post_type(){
        switch(current_action())
        {
            case 'admin_enqueue_scripts':
                global $post;
                if(!is_null($post) AND $post->post_type == msp()->get_post_type())
                {
                    wp_deregister_script('autosave');
                }
                break;
            case 'init':
                $args = array(
                    'labels'                => array(
                        'name'              => 'Microsite'
                        ,'add_new_item'     => 'Build New Microsite'
                        ,'edit_item'        => 'Modify Microsite'
                    )
                    ,'description'          => 'Microsite'
                    ,'public'               => true
                    ,'has_archive'          => false
                    ,'show_in_menu'         => true
                    ,'exclude_from_search'  => true
                    ,'publicly_queryable'   => false
                    ,'show_in_nav_menus'    => false
                    ,'show_in_admin_bar'    => true
                    ,'supports'             => array('title')
                    ,'rewrite'              => false
                    ,'menu_icon'            => 'dashicons-admin-multisite'
                    ,'map_meta_cap'         => null
                    ,'menu_position'        => 100
                    ,'capability_type'      => array('microsite','microsites')
                    ,'capabilities'         => array(
                        'edit_post'                 => 'manage-microsite'
                        ,'read_post'                => 'manage-microsite'
                        ,'delete_post'              => 'delete-microsite'

                        ,'edit_posts'               => 'manage-microsite'
                        ,'edit_others_posts'        => 'manage-microsites'
                        ,'publish_posts'            => 'publish-microsite'
                        ,'read_private_posts'       => 'manage-microsites'
                        
                        ,'read'                     => 'manage-microsite'
                        ,'delete_posts'             => 'delete-microsites'
                        ,'delete_private_posts'     => 'delete-microsites'
                        ,'delete_published_posts'   => 'delete-microsites'
                        ,'delete_others_posts'      => 'delete-microsites'
                        ,'edit_private_posts'       => 'manage-microsites'
                        ,'edit_published_posts'     => 'manage-microsites'
                        ,'create_posts'             => 'manage-microsite'
                        ,'read_private_posts'       => 'manage-microsite'
                    )
                );
                register_post_type( msp()->get_post_type(), $args );
                break;
            case 'enter_title_here':
                $title = func_get_arg(0);
                if ( msp()->get_post_type() === get_post_type() ) 
                {
                    return 'Type microsite name...';
                }
                return $title;
            case 'current_screen':
                $screen = func_get_arg(0);

                $manageable_sites = $this->get_user_manageable_site();

                if($screen->base == 'post' && $screen->id == msp()->get_post_type() && !empty($_GET['post']) )
                {

                    if( !current_user_can( 'publish-microsite' ) )
                    {

                    }
                    else if(in_array($_GET['post'], $manageable_sites))
                    {
                        
                    }
                }
                break;
            case 'post_row_actions':
                $actions = func_get_arg(0);
                if ( msp()->get_post_type() === get_post_type() ) 
                {
                    unset($actions['edit']);
                    unset($actions['inline hide-if-no-js']);
                }
                return $actions;
            case 'add_meta_boxes':
                if(current_user_can('publish-microsite'))
                {
                    add_meta_box( 'mbuilder-microsite-slug', 'Microsite Information', function($post){
                        printf(
                            '<link href="%s" rel="stylesheet">'
                            ,msp()->plugin_url . '/view/assets/css/view/post-build.css'
                        );
                        msp_render('/view/admin/build-microsite.php', array(
                            'post'  => $post
                            ,'path'     => msp()->get_microsite_path($post->ID)
                            ,'category' => msp()->get_microsite_cat($post->ID)
                            ,'theme'            => msp()->get_microsite_theme($post->ID)
                            ,'dashboard_url'    => add_query_arg(array(
                                'post_type' => msp()->get_post_type()
                                ,'mbuilder' => $post->ID
                                ,'page'     => 'mbuilder-dashboard'
                            ), admin_url('edit.php'))
                        ));
                    }, array(msp()->get_post_type()),'normal','high');
                }
                break;
            case 'pre_get_posts':
                $query = func_get_arg(0);

                /**
                 * Filter manageable microsite
                 */
                if(is_admin() && !current_user_can('manage-microsites') && $query->query_vars['post_type'] == msp()->get_post_type())
                {
                    
                    global $wpdb;
                    $own_microsites = $wpdb->get_col(
                        '
                        SELECT
                            p.ID
                        FROM
                            '.$wpdb->posts.' p
                        WHERE
                            p.post_type = "'.msp()->get_post_type().'"
                            AND
                            p.post_status IN ("publish", "draft", "trash", "pending")
                            AND
                            p.post_author = '.get_current_user_id().'
                        '
                    );
                    $manageable_sites = get_user_meta( get_current_user_id(), '_manageable_microsite', true );
                    if(empty($manageable_sites))
                    {
                        $manageable_sites = array(0);
                    }

                    $query->set('post__in', array_merge($own_microsites, array_keys($manageable_sites)) );
                }

                break;
        }
    }

    /**
     * Modify what meta column is shown in post type list table
     */
    function post_type_column($columns){

        switch(current_action())
        {
            case 'manage_posts_custom_column':
                $column     = func_get_arg(0);
                $post_id    = func_get_arg(1);
                $cache_key  = 'post-list-table-the-post-' . $post_id;
                $post = wp_cache_get( $cache_key, 'mbuilder');
                if(empty($post))
                {
                    $post = get_post( $post_id );
                    wp_cache_add( $cache_key, $post, 'mbuilder');
                }

                if($post->post_status != 'publish')
                {
                    return;
                }

                switch($column){
                    case 'microsite-link':
                        $path = msp()->get_microsite_path($post->ID);
                        if(!empty($path))
                        {
                            printf('<a href="%1$s" target="blank">%1$s</a>', home_url( $path ));
                        }
                        break;
                    case 'customize-link':
                        $path = msp()->get_microsite_path($post->ID);
                        printf(
                            '<a href="%1$s" class="button button-primary">Enter Dashboard</a> <a href="%2$s" class="button button-primary">Visit Microsite</a>'
                            ,add_query_arg(
                                array(
                                    'mbuilder'  => $post_id
                                    ,'page'     => 'mbuilder-dashboard' 
                                )
                            )
                            ,home_url( $path )
                        );

                        break;
                    default:
                        break;
                }
                break;
            default:
                $columns = func_get_arg(0);
                unset($columns['cb']);
                unset($columns['date']);
                $columns['microsite-link'] = 'URL';
                $columns['customize-link'] = ' ';
                return $columns;
        }
    }

    /**
     * Run action when post type is taken down, this function responsibility is remove and flush the rule. (appending & flush rule now moved to 'save_post') 
     * this action executed before post is saved, this also affect the quick edit
     */
    function taken_down($new_status, $old_status, $post){

        /**
         * Only execute on microsite post type
         */
        if($post->post_type != msp()->get_post_type())
        {
            return;
        }

        switch($new_status)
        {
            case 'trash':
            case 'draft':
            case 'pending':
                $this->refresh_microsites_cache();
                msp_rewrite::get_instance()->flush_microsite_activation($post->ID, true);
                break;
            default:
                break;
        }
    }

    /**
     * Modify post data so post name is post path
     */
    function post_type_save($data, $post_args){
        
        if($post_args['post_type'] == msp()->get_post_type())
        {
            /**
             * Run action when post type is updated / first publish
             */
            if($post_args['post_status'] == 'publish')
            {
                
                if(isset($_POST['microsite_name']))
                {    
                    $data['post_content'] = $_POST['description'];
                }
            }
        }

        return $data;

    }

    /**
     * Modify cap to allow specific manageable user edit microsite
     */
    function manageable_microsite($allcaps, $cap, $args){
        if(isset($allcaps['manage-microsites']) AND $allcaps['manage-microsites'] === true)
        {
            return $allcaps;
        }

        if(empty($cap[0]))
        {
            return $allcaps;
        }

        if($cap[0] == 'manage-microsites')
        {
            if(is_admin())
            {
                global $current_user;

                if(isset($_GET['post_type']) AND $_GET['post_type'] == msp()->get_post_type() AND !empty($_GET['mbuilder']) )
                {
                    if(
                        in_array('mbuilder-editor',$current_user->caps) 
                        OR 
                        in_array('editor',$current_user->caps) 
                        OR 
                        isset($current_user->caps['editor'])
                        OR 
                        isset($current_user->caps['mbuilder-editor'])
                    )
                    {
                        $manageable_sites = get_user_meta( get_current_user_id(), '_manageable_microsite', true );
                        
                        if(empty($manageable_sites))
                        {
                            $manageable_sites = array();
                        }
                        if(isset($manageable_sites[$_GET['mbuilder']]))
                        {
                            $allcaps[$cap[0]] = true;
                        }
                    }

                    $microsite = get_post( $_GET['mbuilder'] );

                    if($microsite->post_author == get_current_user_id())
                    {
                        $allcaps[$cap[0]] = true;
                    }
                }
            }

        }

        return $allcaps;
    }

    /**
     * Add Microsite admin screen, at this moment all page is unreachable
     * 
     * Hooked @admin_menu:6
     */
    function dashboard(){
        if(
            
            (isset($_GET['post_type']) AND $_GET['post_type'] == msp()->get_post_type())
            AND
            (mikro()->is_admin())
        )
        {
            $microsite = get_post( $_GET['mbuilder'] ); 

            if($microsite->post_status != 'publish')
            {
                return;
            }
            
            /**
             * Print Dashboard Style
             */
            add_action('admin_enqueue_scripts', function(){
                wp_enqueue_media();
                echo '<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans|Open+Sans|Quattrocento+Sans" rel="stylesheet">';
                wp_enqueue_style( 'mikrospace-dashboard', msp()->plugin_url . '/view/assets/css/view/dashboard.css' );
                wp_enqueue_style( 'mikrospace-jquery-ui', msp()->plugin_url . '/view/assets/css/jquery-ui/jquery-ui.min.css' );
                wp_enqueue_style( 'mikrospace-jquery-ui.structure', msp()->plugin_url . '/view/assets/css/jquery-ui/jquery-ui.structure.min.css' );
                wp_enqueue_style( 'mikrospace-jquery-ui.theme', msp()->plugin_url . '/view/assets/css/jquery-ui/jquery-ui.theme.min.css' );
                wp_enqueue_script( 'mikrospace-dashboard', msp()->plugin_url . '/view/assets/js/dashboard.js', array('jquery-ui-core', 'jquery-ui-datepicker'));
            });


            /**
             * Dashboard Page
             */
            add_submenu_page(
                'edit.php?post_type=' . msp()->get_post_type()
                , $microsite->post_title
                , $microsite->post_title
                , 'manage-microsites'
                , 'mbuilder-dashboard'
                , function() use ($microsite) {
                    msp_render('/view/admin/dashboard.php', array('microsite' => $microsite));
                }
                , 0
            );

            /**
             * Return to parent page menu
             */
            add_menu_page( 
                'Return to ' . get_bloginfo()
                , 'Return to ' . get_bloginfo()
                , 'read'
                ,sprintf(
                    'mikrospace-return-wpmenu'

                )
                ,function(){
                    echo 'a';
                }
                , 'dashicons-undo'
                , 100 
            );

            /**
             * Remove non microsite menu
             */
            global $menu;
            foreach ($menu as $index => $_menu) 
            {
                switch($_menu[2])
                {
                    case 'mikrospace-return-wpmenu':
                        $menu[$index][2] = admin_url('edit.php?post_type=' . msp()->get_post_type());
                        break;
                    case 'edit.php?post_type=microsite-builded':
                        $menu[$index][0] = 'Mikrospace';
                        break;
                    default:
                        remove_menu_page( $_menu[2] );
                        break;
                }
            }

        }
    }
}