<?php

class msp_post{

    /**
     * @var string $post_type the object post type name
     */
    var $post_type;

    /**
     * @var array $post_type_args post type args
     */
    var $post_type_args;

    /**
     * @var array $post_type_support post type support field
     */
    var $post_type_support;

    /**
     * @var array $post_meta_args custom meta args
     */
    var $post_meta_args;

    /**
     * @var array $rewrite_args parameter for building object rewrite
     */
    var $rewrite_args;

    /**
     * @var Callable $meta_validator Callback function that validate metadata
     */
    var $meta_validator;

    /**
     * @var Callable $validator Callback function that validate post save
     */
    var $validator;

    /**
     * @var string $name the object name
     */
    var $name;

    /**
     * @var WP_Query $wp_query the query used
     */
    var $wp_query;

    /**
     * @var int $microsite_category current microsite main category
     */
    private $microsite_category;

    /**
     * @var int $microsite_id current microsite id
     */
    private $microsite_id;

    /**
     * @var array $microsite current microsite data
     */
    private $microsite;

    /**
     * @var array $args post type args
     */
    private $args;

    /**
     * @var string $logo_placeholder the logo default image
     */
    private $logo_placeholder;

    /**
     * @var array $object_taxonomies current object registered taxonomy
     */
    private $object_taxonomies = array();

    private $class_stamp = '';

    function __construct($microsite_id, $post_type, $name, $args){
        $this->post_type    = $post_type;

        $this->class_stamp  = md5($post_type);

        $this->args = $args;

        $this->name = $name;

        /**
         * Post type configuration parameter
         */
        $this->post_type_args = wp_parse_args($args, array(
            'menu_icon'         => ''
            ,'menu_position'    => 5
            ,'menu_group_label' => $name
        ));

        /**
         * Post validator
         */
        $this->validator = isset($args['validator']) ? $args['validator'] : false;

        /**
         * Post type support configuration parameter
         */
        $this->post_type_support = isset($args['supports']) ? $args['supports'] : array();

        /**
         * Post meta configuration parameter
         */
        $this->post_meta_args = wp_parse_args(isset($args['metas']) ? $args['metas'] : array()  , array(
        ));

        $this->post_meta_args = apply_filters('msp_post_meta_args', $this->post_meta_args, $this->post_type);

        /**
         * Post meta validator setup
         */
        $this->meta_validator = isset($args['meta_validator']) ? $args['meta_validator'] : false;

        /**
         * Object rewrite rule parameter
         */
        $this->rewrite_args = wp_parse_args(isset($args['rewrite']) ? $args['rewrite'] : array()  , array(
            'slug'                  => false
        ));
        
        if(isset($args['rewrite']) && !is_array($args['rewrite']))
        {
            if(!empty($args['rewrite']))
            {
                $args['rewrite'] = array(
                    'slug'          => $this->post_type
                    ,'has_archive'  => true
                    ,'pages'        => true
                );
            }
            else
            {
                $args['rewrite'] = array(
                    'slug'          => false
                    ,'has_archive'  => false
                    ,'pages'        => false
                );
            }
        }
        
        $this->rewrite_args = wp_parse_args($args['rewrite'], array(
            'slug'          => $this->post_type
            ,'has_archive'  => true
            ,'pages'        => true
        ));

        $this->microsite_id = $microsite_id;

        $this->initialize();
    }

    /**
     * Get current microsite category
     */
    function _load_env(){
        $microsite                  = wp_cache_get( 'microsite', 'dmd-event-microsite' );
        $meta                       = wp_cache_get( 'microsite-meta', 'dmd-event-microsite' );
        $this->microsite            = msp()->get_microsite($this->microsite_id);
        $this->microsite_category   = msp()->get_microsite_cat($this->microsite_id);
        $this->logo_placeholder     = '';
    }

    function initialize(){
        $this->_register_post_type();
        add_action('admin_init', array($this,'_register_modules'));
        add_action('admin_init', array($this,'_process'));
        add_action('init', array($this,'_rewrite'),90);
    }

    /**
     * Modify microsite rewrite rule to add object single and archive url
     * 
     * @param array $rules current microsite rules
     * @return array modified microsite rules
     */
    function _rewrite(){
        if($this->rewrite_args['slug'] === false)
        {
            return;
        }

        // register single permalink
        msp_rewrite::get_instance()->add_rewrite_rule(
            '/'.$this->rewrite_args['slug'].'/([^&]+)/?$'
            ,'&mbuilder-page=post&name=$matches[1]&post_type=' . $this->post_type
        );

        // register archive permalink
        
        if($this->rewrite_args['has_archive'])
        {
            msp_rewrite::get_instance()->add_rewrite_rule(
                '/'.$this->rewrite_args['slug'].'/?$'
                ,'&mbuilder-page=archive&post_type=' . $this->post_type
            );
        }

        // register archive pages permalink
        if($this->rewrite_args['has_archive'] && $this->rewrite_args['pages'])
        {
            msp_rewrite::get_instance()->add_rewrite_rule(
                '/'.$this->rewrite_args['slug'].'/page/([0-9]{1,})/?$'
                ,'&mbuilder-page=archive&paged=$matches[1]&post_type=' . $this->post_type
            );
        }
    }

    function _process(){

        if(!mikro()->is_admin())
        {
            return false;
        }

        $class_stamp_against = isset($_POST['_class_stamp']) ? $_POST['_class_stamp'] : '';

        if($class_stamp_against == $this->class_stamp)
        {
            msp_process_api()->process(
                array($this,'object_update')
                ,'mbuilder-post-ui-editor-' . $this->post_type
                ,'Success save changes'
                ,'Failed to save your changes'
                ,'manage-microsite'
            );
        }
        
        msp_process_api()->process_by_link(
            array($this,'object_delete')
            ,'mbuilder-post-ui-delete-' . $this->post_type
            ,$this->name . ' has been deleted'
            ,'Failed to remove ' . $this->name
            ,'manage-microsite'
        );

        msp_process_api()->process_by_link(
            array($this,'object_save_draft')
            ,'mbuilder-post-ui-editor-save-draft' . $this->post_type
            ,$this->name . ' has been set as draft'
            ,'Failed to process ' . $this->name
            ,'manage-microsite'
        );
        
        $tax = isset($_POST['taxonomy']) ? $_POST['taxonomy'] : '';
        if(!empty($tax) AND isset($this->object_taxonomies[$tax]) AND $class_stamp_against == $this->class_stamp)
        {
            msp_process_api()->process(
                array($this,'term_update')
                ,'mbuilder-post-ui-term-' . $tax
                ,'Success'
                ,'Failed to save your changes'
                ,'manage-microsite'
            );
        }

        $tax_to_delete = !empty($_GET['tax-to-delete']) ? $_GET['tax-to-delete'] : '';
        msp_process_api()->process_by_link(
            array($this,'term_delete')
            ,'mbuilder-post-ui-term-delete-' . $tax_to_delete . $this->class_stamp
            ,'Term deleted'
            ,'Failed to remove term'
        );

    }

    /**
     * Register object post type
     */
    function _register_post_type(){
        $this->_load_env();

        if(empty($this->args['skip_register_object']))
        {
            register_post_type( $this->post_type , array(
                'public'    => true
                ,'labels'   => array(
                    'name'  => $this->name
                )
                ,'rewrite'      => false
                ,'has_archive'  => false
                ,'query_var'    => false
                ,'show_in_menu' => false
                ,'show_in_admin_bar'    => false
            ));
        }

    }

    /**
     * Register the management module
     */
    function _register_modules(){
        mikro()->add_menu_page( 
            $this->name
            ,$this->name
            ,'manage-microsite'
            ,$this->post_type
            ,function($microsite_id){
                if(isset($_GET['edit']))
                {
                    if(!empty($_GET['edit']))
                    {
                        $data = $this->get_object(array(
                            'p'                 => $_GET['edit']
                            ,'post_status'      => array('draft', 'publish')
                        ));

                        if(!empty($data))
                        {
                            $data = $data[0];
                            $data['title']      = $data['post_title'];
                            $data['content']    = $data['post_content'];
                        }
                        else
                        {
                            wp_die( 'Unknown object to edit' );
                            exit;
                        }
                    }
                    else
                    {
                        $data = array();
                    }

                    $this->post_meta_args['_class_stamp']  = array(
                        'type'      => 'hidden'
                        ,'default'  => $this->class_stamp
                        ,'required' => true
                    );

                    $view_args = array(
                        'support'       => $this->post_type_support
                        ,'id'           => 'mbuilder-post-ui-editor-' . $this->post_type
                        ,'data'         => $data
                        ,'object_id'    => $_GET['edit']
                        ,'post_type'    => $this->post_type
                        ,'metas'        => $this->post_meta_args
                        ,'add_url'      => add_query_arg('edit', 0)
                    );

                    if(!empty($_GET['edit']))
                    {
                        $view_args['second_button'] = array(
                            'url'   => msp_process_api()->mark_link(
                                add_query_arg(array(
                                    'edit-validity' => wp_create_nonce($_GET['edit'])
                                ))
                                ,'mbuilder-post-ui-editor-save-draft' . $this->post_type
                            )
                            ,'text' => 'Save as Draft'
                        );
                    }

                    msp_render('/view/posts/editor.php', $view_args);
                }
                else
                {
                    msp_render('/view/posts/posts-ui-list.php', array(
                        'microsite_id'  => $microsite_id
                        ,'post_type'    => $this->post_type
                        ,'label'        => $this->name
                        ,'posts'        => $this->get_object(array(
                            'post_status'       => array('draft','publish')
                            ,'posts_per_page'   => 10
                            ,'paged'            => isset($_GET['paged']) ? $_GET['paged'] : 1
                            ,'s'                => isset($_POST['s']) ? $_POST['s'] : ''
                            ,'post_type'        => $this->post_type
                        ))
                        ,'total_posts' => $this->get_object(array(
                            'post_status'       => array('draft','publish')
                            ,'posts_per_page'   => 10
                            ,'paged'            => isset($_GET['paged']) ? $_GET['paged'] : 1
                            ,'s'                => isset($_POST['s']) ? $_POST['s'] : ''
                            ,'post_type'        => $this->post_type
                            ,'count'            => true
                        ))
                        ,'delete_mark'  => 'mbuilder-post-ui-delete-' . $this->post_type
                    ));
                }
            }
            ,$this->post_type_args['menu_icon']
            ,$this->post_type_args['menu_group_label']
        );

        $this->object_taxonomies = apply_filters('msp_post_object_taxonomies', get_object_taxonomies($this->post_type, 'objects'), $this->post_type) ;

        foreach($this->object_taxonomies as $tax)
        {
            $post_type = $this->post_type;

            mikro()->add_menu_page( 
                $tax->label
                ,$tax->label
                ,'manage-microsite'
                ,$tax->name
                ,function($microsite_id) use($tax, $post_type){
                    $metas = isset($tax->metas) ? $tax->metas : array();

                    $metas['_class_stamp']  = array(
                        'type'      => 'hidden'
                        ,'default'  => $this->class_stamp
                        ,'required' => true
                    );

                    msp_render('/view/posts/terms-ui-list.php', array(
                        'label'         => $tax->label
                        ,'tax'          => $tax
                        ,'taxonomy'     => $tax->name
                        ,'post_type'    => $post_type
                        ,'delete_mark'  => 'mbuilder-post-ui-term-delete-' . $tax->name . $this->class_stamp
                        ,'metas'        => $metas
                    ));
                }
                ,isset($tax->menu_icon) ? $tax->menu_icon : '<span class="dashicons the-icon dashicons-tag"></span>'
                ,$this->name
            );
        }

    }

    function get_args($args = array()){
        return wp_parse_args($args,array(
            'post_type'             => $this->post_type
            ,'paged'                => 1
            ,'posts_per_page'       => 10
            ,'count'                => false
            ,'tax_query'            => array()
            ,'orderby'              => 'date'
            ,'order'                => 'DESC'
            ,'post_status'          => 'publish'
            ,'meta_key'             => '_mbuilder_updated'
            ,'meta_value'           => $this->microsite_id
            ,'meta_query'           => array()
            ,'no_found_rows'        => true
        ));
    }

    /**
     * Wrapper function to retrieve objects
     */
    function get_object($args = array()){

        $args = apply_filters('msp_post_get_object_args', $this->get_args($args), $this->post_type);

        do_action('msp_post_' . $this->post_type . '_get_object', $args);

        if($args['count'])
        {    
            $args['no_found_rows'] = false;
            
            $this->wp_query = new WP_Query( $args );
            
            return $this->wp_query->found_posts;
        }
        else
        {
            $posts = get_posts($args);

            foreach($posts as &$post)
            {

                $post = (array)$post;

                $metas = $this->get_object_meta($post);

                $post = array_merge($post, $metas);

                // retrieve registered taxonomy
                $post['tax'] = $this->get_object_tax($post['ID']);
            }

            return $posts;
        }
    }

    /**
     * Retrieve registered meta for this post type
     * 
     * @param $post WP_Post $post WordPress post object
     */
    function get_object_meta($post){
        $post = (array)$post;

        $metas = array();

        if(has_post_thumbnail( $post['ID'] ))
        {
            $metas['_thumbnail_id']          = get_post_thumbnail_id( $post['ID'] );
            $metas['_thumbnail_url']         = wp_get_attachment_image_url( $metas['_thumbnail_id'], 'full' );
        }
        else
        {
            $metas['_thumbnail']        = 0;
            $metas['_thumbnail_url']    = '';

        }

        $metas['permalink'] = '';

        if($this->rewrite_args['slug'] != false)
        {
            $metas['permalink'] = mikro()->home_url() . '/'.$this->rewrite_args['slug'].'/' . $post['post_name'] . '/';
        }

        // retrieve registered custom meta
        foreach ($this->post_meta_args as $key => $meta_args) 
        {
            $metas[$key] = get_post_meta( $post['ID'], $key, true );
        }

        return apply_filters('msp_post_' . $this->post_type . '_metas', $metas, $post);
    }

    /**
     * Retrieve registered taxonomy for this post
     * 
     * @param int $post_id the post id to get the taxonomy
     */
    function get_object_tax($post_id){
        $t = array();
        $taxes = apply_filters('msp_post_object_taxonomies', get_object_taxonomies($this->post_type,'objects'), $this->post_type );
        foreach ($taxes as $tax => $tax_args) 
        {
            $tax_terms = wp_get_object_terms( $post_id, $tax, array(
                'fields'    => 'ids'
            ));

            $t[$tax] = $tax_terms;
        }

        return $t;
    }

    /**
     * Get custom post permalink
     * 
     * @param msp_post msp post object
     * @deprecated function moved to mikrospace emulate
     */
    function get_permalink($post){
        if($this->rewrite_args['slug'] === false)
        {
            return false;
        }

        return mikro()->home_url('/' . $this->rewrite_args['slug'] . '/' . $post['post_name']);
    }

    /**
     * Get custom post archive permalink
     * 
     * @param int|WP_Post $post
     */
    function get_post_type_archive_link(){
        if($this->rewrite_args['slug'] === false)
        {
            return false;
        }
        
        if($this->rewrite_args['has_archive'])
        {
            return mikro()->home_url('/' . $this->rewrite_args['slug']);
        }

        return false;
    }

    /**
     * Process saving object
     */
    function object_update(){
        $object = apply_filters('msp_post_form_data', $_POST);

        require_once msp()->plugin_dir . '/vendor/autoload.php';

        $validator = array(
            'rules'     => array(
                'post_type' => array(
                    'required'
                )
                ,'validity' => array(
                    'required'
                    ,'validity-check' => function($value) use ($object){
                        if(empty($object['id']))
                        {
                            return true;
                        }
                        return wp_verify_nonce($value, $object['id'] . '-post-ui');
                    }
                )
                ,'title' => array(
                    'required'
                )
                ,'status' => array(
                    'required'
                )
            )
            ,'naming'   => array(
                'title'         => 'Title'
                ,'post_type'    => 'Post Type'
                ,'status'       => 'Status'
            )
            ,'errors'   => array(
                'validity-check'    => 'Validity check failed'
                ,'array-not-empty'  => 'one of :attribute field is required'
            )
        );

        foreach ($this->post_meta_args as $key => $args) 
        {
            $args = wp_parse_args($args, array(
                'required'  => false
                ,'label'    => ''
                ,'type'     => 'text'
            ));

            if($args['required'])
            {
                if($args['type'] == 'checkbox')
                {
                    $validator['rules'][$key] = array(
                        'array-not-empty' => function($value){
                            return count($value) > 0;
                        }
                    );
                }
                else
                {
                    $validator['rules'][$key] = array(
                        'required'
                    );
                }
                $validator['naming'][$key] = $args['label'];
            }
        }

        if(is_callable($this->validator))
        {
            $valid = call_user_func_array($this->validator, array($id, $to_save, $object));

            if(isset($valid['status']) && $valid['status'] == false)
            {
                wp_die( $valid['message'], $valid['message'] );
            }
        }

        $validator = apply_filters('msp_post_validation_rules', $validator, $object);

        $validation_result = SimpleValidator\Validator::validate($object, $validator['rules'], $validator['naming']);

        $validation_result->customErrors($validator['errors']);

        if(!$validation_result->isSuccess())
        {
            $errors = $validation_result->getErrors();

            return array(
                'status'    => false
                ,'message'  => $errors[0]
                ,'data'     => $object
            );
        }
        else
        {
            $id         = $object['ID'];

            foreach ($this->post_meta_args as $key => $meta_args) 
            {
                if(isset($meta_args['required']) && $meta_args['required'] && empty($object[$key]))
                {
                    wp_die( $meta_args['label'] . ' is Required', $meta_args['label'] . ' is Required' );
                }
            }

            $to_save = array(
                'post_title'        => $object['title']
                ,'post_content'     => !empty($object['content']) ? $object['content'] : ''
                ,'post_name'        => sanitize_title( $object['title'] )
                ,'post_status'      => $object['status']
                ,'post_type'        => $this->post_type
            );

            if(is_callable($this->validator))
            {
                $valid = call_user_func_array($this->validator, array($id, $to_save, $object));
    
                if(isset($valid['status']) && $valid['status'] == false)
                {
                    wp_die( $valid['message'], $valid['message'] );
                }
            }

            if(empty($id))
            {
                $saving = wp_insert_post( $to_save, true );
            }
            else
            {
                unset($to_save['post_name']);
                $saving = wp_update_post(wp_parse_args( array(
                    'ID' => $id
                ),$to_save), true);
            }

            if(is_wp_error( $saving ))
            {
                return array(
                    'status'    => false
                    ,'message'  => sprintf('Something wrong saving your data (%s)', $saving->get_error_message()) 
                    ,'data'     => $object
                );
            }
            else
            {
    
                // Mark this post as microsite post
                update_post_meta($saving, '_mbuilder_updated', $this->microsite_id);
    
                // saving thumbnail
                update_post_meta($saving, '_thumbnail_id', !empty($object['_thumbnail']) ? $object['_thumbnail'] : 0 );
    
                // saving registered custom meta
                foreach ($this->post_meta_args as $key => $meta_args) 
                {
                    if(isset($meta_args['save_as_meta']) && !$meta_args['save_as_meta'])
                    {
                        continue;
                    }
    
                    update_post_meta($saving, $key, !empty($object[$key]) ? $object[$key] : '' );
                }
    
                // saving registered taxonomy
                $taxs           = apply_filters('msp_post_object_taxonomies', get_object_taxonomies($this->post_type,'objects'), $this->post_type);
                $to_save_tax    = isset($object['tax']) ? $object['tax'] : array();
                $to_save_tax    = apply_filters('msp_post_object_taxonomies_sent', $to_save_tax, $this->post_type);
                foreach ($taxs as $tax => $tax_args) 
                {
                    if(isset($to_save_tax[$tax]))
                    {
                        wp_set_object_terms($saving,array_map('intval',$to_save_tax[$tax]) , $tax);
                    }
                }

                do_action('msp_post_' . $this->post_type . '_updated', $saving, $object);
    
                return array(
                    'status'    => true
                    ,'message'  => 'Sucess saving your ' . $this->name
                    ,'redirect' => add_query_arg('edit', $saving)
                );
            }
        }
    }

    /**
     * Process deleting an object
     */
    function object_delete(){
        require_once msp()->plugin_dir . '/vendor/autoload.php';

        $post_type = $this->post_type;

        $validator = array(
            'rules'     => array(
                $this->post_type . '_delete_id' => array(
                    'required'
                )
                ,'delete_validity' => array(
                    'required'
                    ,'validity-check' => function($value) use ($post_type){
                        return wp_verify_nonce($value, $_GET[$post_type . '_delete_id']);
                    }
                )
            )
            ,'naming'   => array(
                $this->post_type . '_delete_id'         => 'ID'
                ,'delete_validity'                      => 'ID Validation'
            )
            ,'errors'   => array(
                'delete_validity'    => 'Validity check failed'
            )
        );

        $validation_result = SimpleValidator\Validator::validate($_GET, $validator['rules'], $validator['naming']);

        $validation_result->customErrors($validator['errors']);

        if(!$validation_result->isSuccess())
        {
            $errors = $validation_result->getErrors();

            return array(
                'status'                => false
                ,'message'              => $errors[0]
                ,'query_to_remove'      => array($this->post_type . '_delete_id', 'delete_validity')
            );
        }
        else
        {
            $object_to_delete = $_GET[$this->post_type . '_delete_id'];
    
            $deleting = wp_delete_post( $object_to_delete, true );
    
            return array(
                'status'                => $deleting !== false
                ,'query_to_remove'      => array($this->post_type . '_delete_id', 'delete_validity')
            );   

        }

    }

    /**
     * Process an object set to draft
     */
    function object_save_draft(){
        require_once msp()->plugin_dir . '/vendor/autoload.php';

        $validator = array(
            'rules'     => array(
                'edit' => array(
                    'required'
                )
                ,'edit-validity' => array(
                    'required'
                    ,'validity-check' => function($value){
                        return wp_verify_nonce($value, $_GET['edit']);
                    }
                )
            )
            ,'naming'   => array(
                'edit'              => 'Post ID'
                ,'edit-validity'    => 'ID Validation'
            )
            ,'errors'   => array(
                'edit-validity'    => 'Validity check failed'
            )
        );

        $validation_result = SimpleValidator\Validator::validate($_GET, $validator['rules'], $validator['naming']);

        $validation_result->customErrors($validator['errors']);

        if(!$validation_result->isSuccess())
        {
            $errors = $validation_result->getErrors();

            return array(
                'status'                => false
                ,'message'              => $errors[0]
                ,'query_to_remove'      => array('edit-validity')
            );
        }
        else
        {
            $object_to_delete = $_GET['edit'];
    
            $drafting = wp_update_post(array(
                'ID'            => $_GET['edit']
                ,'post_status'  => 'draft'
            ));

            if(is_wp_error( $drafting ))
            {
                return array(
                    'status'    => false
                    ,'message'  => sprintf('Something wrong set your post as draft (%s)', $drafting->get_error_message()) 
                    ,'query_to_remove'      => array('edit-validity')
                );
            }
            else
            {
                return array(
                    'status'                => true
                    ,'query_to_remove'      => array('edit-validity')
                );
            }
    
        }
    }

    /**
     * Term update process function
     */
    function term_update(){
        $term = $_POST;

        if(empty($term['title']))
        {
            return array(
                'status'    => false
                ,'message'  => 'Name is required'
                ,'data'     => $term
            );
        }

        if(!empty($term['ID']) && !wp_verify_nonce($_GET['edit_validity'], $term['ID']))
        {
            return array(
                'status'    => false
                ,'message'  => 'Term validity invalid'
                ,'data'     => $term
            );
        }

        $tax = get_taxonomy($term['taxonomy']);

        $term_meta = (isset($tax->metas) && is_array($tax->metas)) ? $tax->metas : array();

        $term_meta = array_merge($term_meta, array(
            '_parent'       => array(
                'type'      => 'select'
                ,'label'    => 'Parent'
                ,'required' => false
            )
        ));

        $term_meta = apply_filters( 'mbuilder-term-editor-' . $term['taxonomy'] . 'meta', $term_meta );
        

        foreach ($term_meta as $key => $meta_args) 
        {
            if(isset($meta_args['required']) && $meta_args['required'] && empty($term[$key]))
            {
                return array(
                    'status'    => false
                    ,'message'  => $meta_args['label'] . ' is required'
                    ,'data'     => $term
                );
            }
        }

        $term_save_args = apply_filters('msp_post_term_update_args', array(
            'name'      => $term['title']
            ,'parent'   => !empty($term['_parent']) ? $term['_parent'] : 0
        ),$term,$term['taxonomy']);

        if(!empty($term['ID']))
        {
            $saving = wp_update_term( $term['ID'], $term['taxonomy'], $term_save_args);
        }
        else
        {
            $saving = wp_insert_term( $term['title'], $term['taxonomy'], $term_save_args );
        }

        if(is_wp_error( $saving ))
        {
            return array(
                'status'    => false
                ,'message'  => $saving->get_error_message()
                ,'data'     => $term
            );
        }
        else
        {

            foreach ($term_meta as $key => $meta_args) 
            {
                if(isset($term[$key]))
                {
                    update_term_meta( $saving['term_id'], $key, $term[$key] );
                }
            }

            update_term_meta($saving['term_id'], '_mbuilder_updated', $this->microsite_id);
            
            return array(
                'status'    => true
                ,'message'  => 'Success save your data'
                ,'query_to_remove'  => array('term')
            );
        }
    }

    /**
     * Remove term process
     */
    function term_delete(){
        $tax                = $_GET['tax-to-delete'];

        if(!empty($_GET['delete_id']) AND wp_verify_nonce($_GET['delete_validity'], $_GET['delete_id']))
        {
            $term_to_delete     = $_GET['delete_id'];
            
            $deleting = wp_delete_term( $term_to_delete, $tax );
    
            if($deleting == false)
            {
                return array(
                    'status' => false
                    ,'message' => sprintf('Unable to delete term')
                    ,'query_to_remove' => array(
                        'tax-to-delete'
                        ,'delete_id'
                        ,'delete_validity'
                    )
                );
            }
            else
            {
                return array(
                    'status' => true
                    ,'query_to_remove' => array(
                        'tax-to-delete'
                        ,'delete_id'
                        ,'delete_validity'
                    )
                );
            }
        }
        else
        {
            return array(
                'status' => false
                ,'message' => sprintf('Unable to delete term')
                ,'query_to_remove' => array(
                    'tax-to-delete'
                    ,'delete_id'
                    ,'delete_validity'
                )
            );
        }
    }
}