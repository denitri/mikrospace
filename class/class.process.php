<?php
/**
 * Engine file of microsite builder process API. 
 *
 * @author Deni Tri Hartanto <2denitri@gmail.com>
 * @version 2.0
 */
class msp_process{

    /**
     * @var msp_process $instance class instance
     */
    static $instance;

    /**
     * @var mixed $_stored_flash_message  Temporary variable that hold flash message on next request
     */
    var $_stored_flash_message;

    function __construct(){
        /**
         * RUN !
         */
        $this->integrate();
    }

    /**
     * Get class instance
     *
     * @since 1.0 introduced
     * @return msp_process the class instance
     */
    public static function get_instance(){
        if(self::$instance === null)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Microsite post type integrate functionality to parent site
     */
    function integrate(){
        add_action('plugins_loaded', array($this, 'flush_flash_message') );
    }

    /**
     * Building module processing nonce, the function inline with 'process' function
     * @param type $action
     */
    function mark($action = 'mbuilder_nonce'){
        wp_nonce_field($action, 'nonce');
        wp_nonce_field('iaduahi73672', 'mbuilder_nonce_nonce');
    }

    /**
     * Mark an URL as process API to be process URL
     * 
     * @param string $url the url to add necessary parameter
     * @param string $action nonce action identifier
     * @return string added parameter url
     */
    function mark_link($url, $action){
        return add_query_arg( array(
            'module-process-nonce'  => wp_create_nonce( $action )
        ),$url);
    }

    /**
     * Set flash message to display in the next request page only
     * 
     * @param string $message_id the message ID
     * @param string $message message to store
     * @param array $data optional data to store
     */
    function set_flash_message($message_id, $message, $data = array()){
        $previously_set_flash_message = !empty($_COOKIE['mbuilder_flash_message']) ? unserialize( base64_decode($_COOKIE['mbuilder_flash_message']) ) : array();
        $previously_set_flash_message[$message_id] =  array(
            'message'   => $message
            ,'data'     => $data
        );

        setcookie(
            'mbuilder_flash_message'
            ,base64_encode(serialize($previously_set_flash_message))
            ,time() + 1 * DAY_IN_SECONDS 
            ,COOKIEPATH
            ,COOKIE_DOMAIN
            ,false
            ,true
        );
    }

    /**
     * Store temporarily (and clean) flash message that previously stored
     */
    function flush_flash_message(){
        if(empty($_COOKIE['mbuilder_flash_message']))
        {
            return $this->_stored_flash_message = array();
        }

        $this->_stored_flash_message = unserialize(base64_decode($_COOKIE['mbuilder_flash_message']));

        unset($_COOKIE['mbuilder_flash_message']);

        return setcookie(
            'mbuilder_flash_message'
            ,""
            ,time() - 60000
            ,COOKIEPATH
            ,COOKIE_DOMAIN
            ,false
            ,true
        );
    }

    /**
     * Get (and clean) flash message that previously stored
     * 
     * @param string $message_id the message ID
     * @return array the message and optionally stored data
     */
    function flash_message($message_id){

        if(isset($this->_stored_flash_message[$message_id]))
        {
            $message = $this->_stored_flash_message[$message_id];
            unset($this->_stored_flash_message[$message_id]);
            return $message;
        }
        else
        {
            return array();
        }
    }

    /**
     * Echo process flash message
     * 
     * @return mixed process data
     */
    function respond($_respond_renderer = false){
        $fm = $this->flash_message('mbuilder_process_message');
        
        if(!empty($fm['message']))
        {
            if($_respond_renderer !== false and is_callable($_respond_renderer))
            {
                call_user_func_array($_respond_renderer, array($fm['message']['status'], $fm['message']['message']));
            }
            else
            {
                msp_render('/view/admin/element/respond-message.php', array(
                    'is_error'  => !$fm['message']['status']
                    ,'message'  => $fm['message']['message']
                ));
            }
            
            return $fm['data'];
        }

        return array();

    }

    

    /**
     * Refresh current page
     * 
     * @param array $args refreh parameter
     * @param boolean whether to return the refresh URL or skip to page refresh
     * @return void|string 
     */
    private function refresh($args = array(), $get_url = false){
        $refresh_args = wp_parse_args( $args, array(
            'query_to_remove'   => array()
        ));

        if(!empty($refresh_args['query_to_remove']))
        {
            $refresh_url = remove_query_arg( $refresh_args['query_to_remove'] );
        }
        else
        {
            $refresh_url = add_query_arg(array());
        }

        if($get_url)
        {
            return $refresh_url;
        }


        wp_redirect($refresh_url);
        exit;
    }

    /**
     * Process any module posted data, to activate the post data must contain nonce
     * from 'nonce_field' function
     *
     * @param type $callable
     * @param type $action
     * @param type $true_message
     * @param type $false_message
     * @return boolean
     */
    function process($callable, $action, $true_message = '', $false_message = 'Failed to process', $capability = 'manage_options'){
        if(!empty($capability) && !current_user_can( $capability ))
        {
            return false;
        }

        /**
         * Check is the function callable
         */
        if(!is_callable($callable))
        {
            return false;
        }

        /**
         * Check Default Nonce
         */
        if(isset($_POST['mbuilder_nonce_nonce']) && wp_verify_nonce($_POST['mbuilder_nonce_nonce'],'iaduahi73672'))
        {
            /**
             * Check Action Nonce
             */
            if(isset($_POST['nonce']) AND wp_verify_nonce($_POST['nonce'],$action))
            {
                $process = call_user_func($callable);

                $query_to_remove    = array();
                $data               = array();

                if(is_array($process))
                {
                    $true_message       = isset($process['message']) ? $process['message'] : '';
                    $false_message      = isset($process['message']) ? $process['message'] : '';
                    $data               = isset($process['data']) ? $process['data'] : array();
                    $redirect           = isset($process['redirect']) ? $process['redirect'] : '';
                    $query_to_remove    = isset($process['query_to_remove']) ? $process['query_to_remove'] : array();
                    $process            = $process['status'];
                }

                if($process)
                {
                    $this->set_flash_message('mbuilder_process_message', array('status' => true ,'message' => $true_message));
                }
                else if($process === false)
                {
                    $this->set_flash_message('mbuilder_process_message', array('status' => false, 'message' => $false_message) , $data);
                }

                if(!empty($redirect))
                {
                    wp_redirect($redirect);
                    exit;
                }

                $this->refresh(array(
                    'query_to_remove'   => $query_to_remove
                ));

            }
        }

    }

    /**
     * Catch URL that generated by 'generate_process_link'
     *
     * @param type $callable
     * @param type $action
     * @param type $true_message
     * @param type $false_message
     * @return boolean
     */
    function process_by_link($callable, $action, $true_message = '', $false_message = '', $capability = 'manage_options'){
        
        if(!current_user_can( $capability ))
        {
            return false;
        }
        if(!is_callable($callable))
        {
            return false;
        }


        if(isset($_GET['module-process-nonce']) && wp_verify_nonce($_GET['module-process-nonce'],$action))
        {
            $process = call_user_func($callable);

            $query_to_remove = array();

            $true_message       = isset($process['message']) ? $process['message'] : $true_message;
            $false_message      = isset($process['message']) ? $process['message'] : $false_message;
            $query_to_remove    = isset($process['query_to_remove']) ? $process['query_to_remove'] : array();
            $process            = $process['status'];

            if($process)
            {
                $this->set_flash_message('mbuilder_process_message', array('status' => false ,'message' => $true_message));
            }
            else if($process === false)
            {
                $this->set_flash_message('mbuilder_process_message', array('status' => true, 'message' => $false_message));
            }
            

            $refresh_url = remove_query_arg(
                array_merge(
                    array('module-process-nonce')
                    ,$query_to_remove
                )
            );

            wp_redirect($refresh_url);
            exit;
        }
    }
}