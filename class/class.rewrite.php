<?php
/**
 * Engine file of microsite builder rewrite API. 
 *
 * @author Deni Tri Hartanto <2denitri@gmail.com>
 * @version 1.0
 */
class msp_rewrite{

    /**
     * @var msp_rewrite $instance class instance
     */
    static $instance;

    /**
     * @var mixed $_registered_rules  Temporary variable that plugins and theme function custom rule
     */
    var $_registered_rules;

    /**
     * @var string $base_rewrite base rewrite rull
     */
    private $base_rewrite = 'index.php?mbuilder=';

    /**
     * @var string $persisted_option_name the option name where rewrite rule is stored 
     */
    private $persisted_option_name = 'mbuilder_rewrite_rules';

    /**
     * Get class instance
     *
     * @since 1.0 introduced
     * @return msp_rewrite the class instance
     */
    public static function get_instance(){
        if(self::$instance === null)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Start integrating function
     */
    function integrate(){
        add_action('plugins_loaded', array($this,'register_default_rule'),5);
        add_action('init', array($this,'integrate_rules'),10);
        add_action('wp_loaded', array($this,'flush_current_microsite'),10);
        add_action('msp_init', array($this,'microsite_integrate'));
        add_filter('user_has_cap',array($this,'temporary_permalink_access'), 10 , 3);
    }

    /**
     * Start function that only need to run in microsite environment
     */
    function microsite_integrate(){
        add_filter('template_include', array($this,'template_include'), 1);
        add_filter('pre_get_posts', array($this,'pre_get_posts'), 1);
        add_filter('pre_get_document_title', array($this,'wp_title'), 20);
    }

    /**
     * Modify default page title
     */
    function wp_title($title){
        $page               = get_query_var('mbuilder-page');
        $microsite          = msp()->get_microsite(msp()->get_microsite_id());
        switch($page)
        {
            case 'post':
                global $post;
                
                return $post->post_title . ' | ' . $microsite->post_title;
            case 'category':
                $category   = get_category_by_slug(get_query_var('category_name'));

                $page       = get_query_var('paged');

                return ($page ? 'Page : ' . $page . ' | ' : '') . $category->name . ' | ' . $microsite->post_title;
            case 'archive':
                $post_type = get_post_type_object(get_query_var('post_type'));

                $page       = get_query_var('paged');

                return ($page ? 'Page : ' . $page . ' | ' : '') . $post_type->label . ' | ' . $microsite->post_title;
            case 'other':
                return 'Page not found | ' . $microsite->post_title;
            default:
                return $microsite->post_title . ' | ' . $microsite->post_content;
        }

        return $title;
    }

    /**
     * Always mark main query to get microsite posts only
     */
    function pre_get_posts(&$query){
        if($query->is_main_query() && !mikro()->is_admin())
        {
            $meta_query = $query->get('meta_query');
            if(empty($meta_query))
            {
                $meta_query = array();
            }
            $meta_query[] = array(
                'key'              => '_mbuilder_updated'
                ,'value'           => msp()->get_microsite_id()
            );
            $query->set('meta_query', $meta_query);
        }
    }

    /**
     * Template include controller
     * 
     * @param string $template original template
     * @return string modified template
     */
    function template_include($template){
        $microsite_id       = msp()->get_microsite_id();
        $page               = get_query_var('mbuilder-page');
        $microsite_theme    = msp()->get_microsite_theme(msp()->get_microsite_id());


        if(empty($microsite_theme))
        {
            wp_die('This microsite has not set with any theme');exit;
        }

        switch($page)
        {
            case 'wp-json':
                if ( empty( $GLOBALS['wp']->query_vars['msp_rest_route'] ) ) {
                    return;
                }
	
                define( 'REST_REQUEST', true );
	
                // Initialize the server.
                $server = rest_get_server();
	
                $route = untrailingslashit( $GLOBALS['wp']->query_vars['msp_rest_route'] );

                if(empty($route))
                {
                    $route = '/';
                }

                $server->serve_request( $route );
	
                die();
                return;
            case 'post':
                $post_type = get_query_var('post_type');
                $templates = array('single.php');
                if(!empty($post_type))
                {
                    array_unshift($templates, 'single-' . $post_type . '.php');
                }
                $template = $this->_template_series($templates, $microsite_theme['Path'], $microsite_theme['Template'] );
                break;
            case 'category':
                $template = $this->_template_series(array('category.php'), $microsite_theme['Path'], $microsite_theme['Template'] );
                break;
            case 'archive':
                $post_type = get_query_var('post_type');
                $templates = array('archive.php');
                if(!empty($post_type))
                {
                    array_unshift($templates, 'archive-' . $post_type . '.php');
                }
                $template = $this->_template_series($templates, $microsite_theme['Path'], $microsite_theme['Template'] );
                break;
            case 'other':
                header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found"); 
                $template = $this->_template_series(array('404.php'), $microsite_theme['Path'], $microsite_theme['Template'] );
                break;
            case 'wpadmin':
                wp_redirect(admin_url('/edit.php?post_type='.msp()->get_post_type().'&mbuilder='.$microsite_id.'&page=mbuilder-dashboard'));
                exit;
            default:
                $template = $this->_template_series(array('home.php'), $microsite_theme['Path'], $microsite_theme['Template'] );
                break;
        }

        return $template;
    }

    /**
     * Recursive load theme template file.
     * 
     * @param array $templates list of file name to load
     * @param string $path the theme path
     * @param string $parent_path the parent theme path
     * @return string file template absolute path 
     */
    function _template_series($templates, $path, $parent_path = ''){
        $themes_dir = msp()->get_themes_dir();

        foreach ($templates as $template ) 
        {
            $template_path = $themes_dir . '/' . $path . '/' . $template; 
            if(file_exists($template_path))
            {
                return apply_filters('msp_template_series', $template_path, $path);
            }
        }

        if(!empty($parent_path))
        {
            return $this->_template_series($templates, $parent_path);
        }

        return apply_filters('msp_template_series', $themes_dir . '/' . $path . '/index.php', $path) ;
    }

    /**
     * Respond function of a link generated by microsite_flush_rule_link().
     * Flush current microsite rewrite rules
     */
    function flush_current_microsite(){
        
        if( current_user_can( 'manage_options' ) && mikro()->wp_doing_ajax() && isset($_GET['action']) && $_GET['action'] == 'mbuilder_flush_rule')
        {
            $microsite_id = msp()->get_microsite_id();            
            
            $this->save_rules();

            wp_redirect(
                admin_url(
                    sprintf(
                        'edit.php?post_type=%1$s&mbuilder=%2$s&page=mbuilder-dashboard&subpage=setups'
                        , msp()->get_post_type()
                        ,$microsite_id
                    )
                )
            );
            die();
        }
    }

    /**
     * Build an URL that can be used to flush a microsite rewrite rule
     * 
     * @param int $microsite_id microsite ID to generate the ID
     * @return string the flush rule URL
     */
    function microsite_flush_rule_link($microsite_id){
        return admin_url( '/admin-ajax.php?action=mbuilder_flush_rule&mbuilder_ajax=' . $microsite_id );
    }

    /**
     * Get microsite base rewrite rule
     * 
     * @param int $microsite_id microsite ID to get the base rewrite
     * @return string the base rewrite rule
     */
    function get_microsite_base_rewrite($microsite_id){
        return $this->base_rewrite . $microsite_id;
    }

    /**
     * Add new rule into current microsite 
     */
    function add_rewrite_rule($rule, $rewrite_to, $rewrite_tags = array()){
        $microsite      = msp()->get_microsite(msp()->get_microsite_id());
        $microsite_path = msp()->get_microsite_path(msp()->get_microsite_id());

        array_unshift($this->_registered_rules[$microsite->ID],array(
            sprintf('^(?i)%1$s' . $rule, $microsite_path)
            ,$this->get_microsite_base_rewrite($microsite->ID) . $rewrite_to
            ,$rewrite_tags
        ));
    }

    /**
     * Curlimatically call parent web permalink refresh
     */
    function _refresh_parent_permalinks(){
        flush_rewrite_rules();

        $key = wp_create_nonce();

        msp()->emulate_dashboard_visit( add_query_arg( array(
            'msp_rewrite_permalink_access'          => $key
            ,'msp_rewrite_permalink_access_token'   => md5($key . 'msp0') 
        ), '/options-permalink.php' ) );
    }


    /**
     * Temporarily allow acces user to permalinks.page on mikrospace-flagged URL
     */
    function temporary_permalink_access($allcaps, $cap, $args){
        if(!isset($_GET['msp_rewrite_permalink_access']) && !isset($_GET['msp_rewrite_permalink_access_token']))
        {
            return $allcaps;
        }

        if( md5($_GET['msp_rewrite_permalink_access'] . 'msp0') !=  $_GET['msp_rewrite_permalink_access_token'] )
        {
            return $allcaps;
        }

        if(isset($allcaps['manage_options']) AND $allcaps['manage_options'] === true)
        {
            return $allcaps;
        }

        if(empty($cap[0]))
        {
            return $allcaps;
        }

        if($cap[0] == 'manage_options')
        {
            $allcaps[$cap[0]] = true;
        }

        return $allcaps;
    }
    
    /**
     * Save all appended rules into persisted option
     */
    function save_rules(){
        foreach ($this->_registered_rules as $microsite_id => $rules) 
        {
            $dup_free_rules = array();
            foreach ($rules as $rule) 
            {
                if(!isset($dup_free_rules[$rule[0]]))
                {
                    $dup_free_rules[$rule[0]] = $rule;
                }
            }
            $this->_registered_rules[$microsite_id] = $dup_free_rules;
        }

        update_option( $this->persisted_option_name, $this->_registered_rules );

        $this->integrate_rules();

        $this->_refresh_parent_permalinks();
    }

    /**
     * Integrate persisted rule onto host site
     */
    function integrate_rules(){
        $persisted_rules = get_option( $this->persisted_option_name, array() );

        if(empty($persisted_rules))
        {
            $persisted_rules = array();
        }

        krsort($persisted_rules);

        foreach ($persisted_rules as $microsite_id => $microsite_rules) 
        {
            foreach ($microsite_rules as $rule) 
            {
                if(strpos($rule[1], 'mbuilder-page=other') === false)
                {
                    $pos = 'top';
                }
                else
                {
                    $pos = 'bottom';
                }
                add_rewrite_rule($rule[0], $rule[1], $pos);

                if(!empty($rule[2]))
                {
                    foreach ($rule[2] as $rewrite_tags) 
                    {
                        add_rewrite_tag($rewrite_tags[0], $rewrite_tags[1]);
                    }
                }
            }
        }
    }

    /**
     * Refresh check for invalid base microsite URL
     */
    function refresh_persistent_rules(){
        $persisted_rules = get_option( $this->persisted_option_name, array() );

        if(empty($persisted_rules))
        {
            $persisted_rules = array();
        }

        $active_microsites = msp()->get_microsites(array(
            'post_type'         => msp()->get_post_type()
            ,'posts_per_page'   => -1
            ,'order'            => 'DESC'
            ,'orderby'          => 'date'
        ));

        if(empty($active_microsites))
        {
            $active_microsites = array();
        }

        $active_microsites_id = wp_list_pluck( $active_microsites, 'ID' );

        foreach ($persisted_rules as $microsite_id => $rules) 
        {
            if(!in_array($microsite_id, $active_microsites_id))
            {
                unset($persisted_rules[$microsite_id]);
            }
        }

        update_option( $this->persisted_option_name, $persisted_rules );
    }

    /**
     * Remove / add microsite from persistent rules
     * 
     * @param int $microsite_id microsite ID to add / remove
     * @param boolean $is_remove remove the microsite ?
     * @return void
     */
    function flush_microsite_activation($microsite_id, $is_remove = false){
        $persistent_rules = get_option( $this->persisted_option_name, array() );
        if(empty($persistent_rules))
        {
            $persistent_rules = array();
        }

        if($is_remove)
        {
            // remove microsite rules
            if(isset($persistent_rules[$microsite_id]))
            {
                unset($persistent_rules[$microsite_id]);
            }
        }
        else
        {
            // add microsite rules
            $microsite = msp()->get_microsite($microsite_id);
            $persistent_rules[$microsite_id] = $this->_register_microsite_default_rule($microsite_id, msp()->get_microsite_path($microsite_id));
        }

        $this->_registered_rules = $persistent_rules;
        update_option( $this->persisted_option_name, $persistent_rules );
        $this->_refresh_parent_permalinks();
    }

    /**
     * Create microsite default rules
     * 
     * @param int $microsite_id microsite ID to create the rule
     * @param string $microsite_path microsite rule base path
     */
    function _register_microsite_default_rule($microsite_id, $microsite_path){
        $base_rewrite = $this->get_microsite_base_rewrite($microsite_id);

        $rules = array();

        $rules[] = array(
            sprintf('^(?i)%1$s/?$', $microsite_path)
            ,$base_rewrite . '&mbuilder-page=home'
            ,array(
                array('%mbuilder%', '([^&]+)')
                ,array('%mbuilder-page%', '([^&]+)')
            )
        );
        $rules[] = array(
            sprintf('^(?i)%1$s/wp-json/(.*)?', $microsite_path)
            ,$base_rewrite . '&mbuilder-page=wp-json&msp_rest_route=/$matches[1]'
            ,array(
                array('%msp_rest_route%', '([^&]+)')
            )
        );
        $rules[] = array(
            sprintf('^(?i)%1$s/post/([^&]+)/?$', $microsite_path)
            ,$base_rewrite . '&mbuilder-page=post&name=$matches[1]'
        );
        $rules[] = array(
            sprintf('^(?i)%1$s/category/(.+?)/page/?([0-9]{1,})/?$', $microsite_path)
            ,$base_rewrite . '&mbuilder-page=category&category_name=$matches[1]&paged=$matches[2]'
        );
        $rules[] = array(
            sprintf('^(?i)%1$s/category/(.+?)/?$', $microsite_path)
            ,$base_rewrite . '&mbuilder-page=category&category_name=$matches[1]'
        );
        $rules[] = array(
            sprintf('^(?i)%1$s/wp-admin/?$', $microsite_path)
            ,$base_rewrite . '&mbuilder-page=wpadmin'
        );
        $rules[] = array(
            sprintf('^(?i)%1$s/(.?.+?)/?$', $microsite_path)
            ,$base_rewrite . '&mbuilder-page=other&name=$matches[1]'
        );

        return $rules;
    }

    /**
     * Integrate microsite rewrite rules
     */
    function register_default_rule(){
        $microsites = msp()->get_microsites(array(
            'post_type'         => msp()->get_post_type()
            ,'posts_per_page'   => -1
            ,'order'            => 'DESC'
            ,'orderby'          => 'date'
        ));


        $current_microsite = msp()->get_microsite_id();

        $this->_registered_rules = get_option( $this->persisted_option_name, array() );
        
        if(empty($this->_registered_rules))
        {
            $this->_registered_rules = array();
        }

        foreach ($microsites as $microsite) 
        {
            // Current microsite always revert to default rule first
            if($current_microsite == $microsite['ID'])
            {
                $this->_registered_rules[$microsite['ID']] = $this->_register_microsite_default_rule($microsite['ID'], msp()->get_microsite_path($microsite['ID']));                
            }
            // Dont revert to default rule for other microsite because their function could not be run
            else if(empty($this->_registered_rules[$microsite['ID']]))
            {
                $this->_registered_rules[$microsite['ID']] = $this->_register_microsite_default_rule($microsite['ID'], msp()->get_microsite_path($microsite['ID']));
            }
        }
    }
}