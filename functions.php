<?php

/**
 * Get microsite builder engine instance
 *
 * @since 1.0 introduced
 * @return mikrospace the class instance
 */
function msp(){
    return mikrospace::get_instance();
}

/**
 * Load template file and extract() $param to use inside the file.
 *
 * @param string $relative_file the file anchor that define the base path
 * @param string $template_file the template file name
 * @param array $params parameter to send to template
 * @since 1.0 introduced
 */
function msp_render($template_file, $params = array()) {

    extract($params, EXTR_SKIP);

    require(MSP_PLUGIN_DIR . $template_file);
}

/**
 * Get microsite meta
 * 
 * @var int $microsite_id the microsite ID to get the meta
 * @var string $meta_key meta key to get
 * @var mixed $default default value to return if meta is empty (empty())
 * @since 1.0 introduced
 * @return mixed microsite meta
 */
function msp_microsite_meta($microsite_id, $meta_key, $default = null){
    $meta = get_post_meta( $microsite_id, $meta_key, true );

    if(empty($meta))
    {
        return $default;
    }

    return $meta;
}

/**
 * Get microsite builder WP Native function emulator container
 */
function mikro(){
    return msp_emulate::get_instance();
}

/**
 * Get microsite builder process helper
 */
function msp_process_api(){
    return msp_process::get_instance();
}

/**
 * Render image picker field
 * 
 * @since 1.0.0
 * @param string @id image picker ID
 * @param string $name input field name
 * @param string $value saved database value
 * @param string $src saved image src 
 */
function msp_image_picker($id, $name, $value, $src, $parameter = array()){
    $parameter = wp_parse_args( $parameter, array(
        'caption'       => 'Use Image'
        ,'width'        => 200
        ,'description'  => ''
        ,'placeholder'  => ''
    ));
    ?>
    <div class="mbuilder-image-picker mbuilder-image-picker-<?php echo $id ?>">
        <input
            type="hidden"
            name="<?php echo $name ?>"
            class="input-ip-<?php echo $id ?>"
            value="<?php echo $value ?>"
        />
        <div class="image-box boxed" style="background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAALElEQVQYGWO8d+/efwYkoKioiMRjYGBC4WHhUK6A8T8QIJt8//59ZC493AAAQssKpBK4F5AAAAAASUVORK5CYII=)">
            <img
                src="<?php echo $src ? $src : $parameter['placeholder'] ?>"
                id="img-<?php echo $id ?>"
                class="image-picker img-ip-<?php echo $id ?>"
                width="<?php echo $parameter['width'] ?>"
                data-imagepicker-input=".input-ip-<?php echo $id ?>"
                data-imagepicker-caption="<?php echo $parameter['caption'] ?>"
                data-imagepicker-parent=".mbuilder-image-picker-<?php echo $id ?>"
                data-default-src="<?php echo $parameter['placeholder'] ?>"
            />
        </div>
        <a
            data-imagepicker-input=".input-ip-<?php echo $id ?>"
            data-imagepicker-img=".img-ip-<?php echo $id ?>"
            data-imagepicker-parent=".mbuilder-image-picker-<?php echo $id ?>"
            class="remove-imagepicker button"
            style="<?php echo empty($src) ? 'display:none' : '' ?>"
        >Remove</a>
        
        <p class="description">
            <?php echo $parameter['description'] ?> 
        </p>
    </div>
    <?php
}

/**
 * Render file picker field
 * 
 * @since 1.0.0
 * @param string @id image picker ID
 * @param string $name input field name
 * @param string $value saved database value
 * @param string $src saved image src 
 */
function msp_file_picker($id, $name, $value, $src, $parameter = array()){
    $parameter = wp_parse_args( $parameter, array(
        'caption'       => 'Use File'
        ,'description'  => ''
        ,'placeholder'  => ''
    ));
    ?>
    <div class="mbuilder-image-picker file mbuilder-image-picker-<?php echo $id ?>">
        <input
            type="hidden"
            name="<?php echo $name ?>"
            class="input-ip-<?php echo $id ?>"
            value="<?php echo $value ?>"
        />
        <span 
            class="select-file image-picker button <?php echo !empty($src) ? 'selected' : '' ?>"
            data-imagepicker-input=".input-ip-<?php echo $id ?>"
            data-imagepicker-caption="<?php echo $parameter['caption'] ?>"
            data-imagepicker-parent=".mbuilder-image-picker-<?php echo $id ?>"
        ><?php echo !empty($src) ? $src : 'Select File' ?></span>
        <a
            data-imagepicker-input=".input-ip-<?php echo $id ?>"
            data-imagepicker-parent=".mbuilder-image-picker-<?php echo $id ?>"
            class="remove-imagepicker button"
            style="<?php echo empty($src) ? 'display:none' : '' ?>"
        >Remove</a>
        
        <p class="description">
            <?php echo $parameter['description'] ?> 
        </p>
    </div>
    <?php
}

/**
 * Register component to Homepage Builder
 */
function msp_register_home_component($component_id, $component_name, $gui_renderer, $frontend, $parameter = array()){
    add_filter('mhp_homepage_element', function($elements) use ($component_id, $component_name, $gui_renderer, $frontend, $parameter) {
        
        $parameter = wp_parse_args( $parameter, array(
            'multiple'          => true
            ,'icon'             => 'dashicons-screenoptions'
            ,'_system'          => false
        ));
        $elements[$component_id] = array(
            'name'          => $component_name
            ,'id'           => $component_id
            ,'renderer'     => $gui_renderer
            ,'frontend'     => $frontend
            ,'parameter'    => $parameter
        );

        return $elements;
    });
}

/**
 * Set Predefined home layout structure
 * 
 * @param array $elements the element structure
 */
function msp_home_structure($elements){
    add_filter('mbuilder_home_layout_element', function() use ($elements){
        foreach ($elements as &$element) 
        {
            if(!isset($element['options']))
            {
                $element['options'] = array();
            }

            if($element['element'] == 'column-two')
            {
                $element['element'] = 'mbuilder-column-two';
            }
            else if($element['element'] == 'column-three')
            {
                $element['element'] = 'mbuilder-column-three';
            }

            if($element['element'] == 'mbuilder-column-two' || $element['element'] == 'mbuilder-column-two')
            {
                if(!isset($element['options']['child']))
                {
                    $element['options']['child'] = array();
                }
                if(!isset($element['options']['mark']))
                {
                    $element['options']['mark'] = 1;
                }
            }
            if($element['element'] == 'mbuilder-column-two')
            {
                if(count($element['options']['child']) > 2)
                {
                    array_splice($element['options']['child'],0,1);
                }
            }
            else if($element['element'] == 'mbuilder-column-three')
            {
                if(count($element['options']['child']) > 3)
                {
                    array_splice($element['options']['child'],0,2);
                }
            }
        }
        return $elements;
    });
}

/**
 * Register configuration field into microsite builder configuration module
 * 
 * @param string $configuration_id config ID
 * @param string $name Config menu name
 * @param array $fields list of config fields
 * @param array $args configuration args
 */
function msp_register_config($configuration_id, $name, $fields, $args = array()){
    return new mbuilder_config_ui(msp()->get_microsite_id(), $configuration_id, $name, $fields, $args);
}

/**
 * Homepage builder class helper
 * 
 * Render the homepage
 */
function msp_homepage(){
    msp_home_builder::get_instance()->render();
}
?>