<?php
/*
Plugin Name: MikroSpace
Plugin URI: http://mikrospace.wordpress.com
Description: Microsite foundation & CMS
Version: 0.8.0
Author: Deni Tri Hartanto
*/

require_once 'class/class.process.php';
require_once 'class/class.rewrite.php';
require_once 'class/class.post.php';
require_once 'class/class.config.php';
require_once 'class/class.homebuilder.php';
require_once 'class/class.menu.php';
require_once 'class/class.modules.php';
require_once 'class/class.post-type.php';
require_once 'class/class.emulate.php';
require_once 'class/class.mikrospace.php';
require_once 'functions.php';

if (!defined('MSP_PLUGIN_DIR'))
    define('MSP_PLUGIN_DIR', wp_normalize_path( untrailingslashit (plugin_dir_path( __FILE__ )) ) );

if (!defined('MSP_PLUGIN_URL'))
    define('MSP_PLUGIN_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );

if (!defined('MSP_PLUGIN_FILE'))
    define('MSP_PLUGIN_FILE', __FILE__ );

/**
 * RUN !
 */
msp()->integrate();