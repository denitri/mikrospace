<?php
if($post->post_status == 'publish')
{
    ?>
    <div id="build-goto-dashboard">
        <a href="<?php echo home_url($path) ?>" class="the-btn">
            <span class="dashicons dashicons-admin-home"></span> View Microsite
        </a>
        <a href="<?php echo $dashboard_url ?>" class="the-btn">
            <span class="dashicons dashicons-dashboard"></span> Go To Dashboard
        </a>
    </div>
    <?php
}
?>
<table class="form-table">
    <tr>
        <th>Path</th>
        <td>
            <input type="text" class="regular-text" name="microsite_name" value="<?php echo $path ?>">
            <p class="description">
                The path of this microsite, ex : <?php echo home_url('/{path}') ?>
            </p>
        </td>
    </tr>
    <tr>
        <th>Category</th>
        <td>
            <fieldset>
                <p>
                <?php $cats = get_categories(array('hide_empty' => false)) ?>
                    <select name="microsite_category">
                        <option value="">Select Category</option>
                        <?php 
                        foreach ($cats as $cat) 
                        {
                            printf(
                                '<option value="%s" %s>%s</option>'
                                ,$cat->term_id
                                ,selected($category, $cat->term_id, false)
                                ,$cat->name
                            );
                        }
                        ?>
                    </select>
                </p>
                <ul>
                    <li>
                        <label for="create-microsite-cat">or, Create new category : </label>
                        <input type="text" class="regular-text" name="microsite_category_create" id="create-microsite-cat" value="">
                    </li>
                </ul>
            </fieldset>
            <p class="description">
                Select or create a category that will hold all post in this microsite
            </p>
        </td>
    </tr>
    <tr>
        <th>Description</th>
        <td>
            <textarea class="large-text" name="description" id="" cols="30" rows="3"><?php echo $post->post_content ?></textarea>
            <p class="description">
                Describe your microsite in 20 word
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <h3>Theme</h3>
            <p class="description">Select theme for this microsite</p><br>
            <div class="theme-browser rendered">
                <div class="themes wp-clearfix">
                    <?php 
                    $current_theme          = msp()->get_microsite_theme( $post->ID );
                    $current_theme_name     = isset($current_theme['Path']) ? $current_theme['Path'] : '';

                    $network_themes = get_option( 'mbuilder_network_themes', array() );

                    if(empty($network_themes))
                    {
                        $network_themes = array();
                    }

                    $themes     = array();
                    $themes_dir = msp()->get_themes_dir();
                    $dirs       = scandir($themes_dir);
                    
                    foreach($dirs as $t)
                    {
                        if(is_dir($themes_dir . "/". $t) AND file_exists($themes_dir . "/$t/style.css"))
                        {
                            $theme_data = msp()->get_theme_info($t);
                            if($theme_data['MikroSpace'] == 'Yes')
                            {
                                $themes[$theme_data['ThemeName']]  = $theme_data;
                                
                                if(!isset($network_themes[$t]))
                                {
                                    unset($themes[$theme_data['ThemeName']]);
                                }
                            }
                        }

                    }

                    asort($themes);

                    if(empty($themes))
                    {
                        echo 'No network enabled theme found';
                    }

                    foreach ($themes as $t) 
                    {
                        ?>
                        <div class="theme <?php echo $t['Path'] == $current_theme_name ? 'active' : '' ?>" data-slug="<?php echo $t['Path'] ?>">
                            <?php
                            if(empty($t['Preview']))
                            {
                                echo '<div class="theme-screenshot blank"></div>';
                            }
                            else
                            {
                                printf(
                                    '<div class="theme-screenshot"><img src="%s" alt=""></div>'
                                    ,$t['Preview']
                                );
                            }
                            ?>
                            <div class="theme-id-container">
                                <h2 class="theme-name" id="<?php echo $t['Path'] ?>" style="padding: 10px 15px;">
                                    <?php echo $t['Path'] == $current_theme_name ? '<span>Active</span> : ' : '' ?><?php echo $t['ThemeName'] ?>			
                                </h2>
                                <div class="theme-actions" style="opacity:1;background:#333">
                                    <?php
                                    printf(
                                        '<input type="radio" value="%1$s" id="theme-select-%1$s" %2$s name="microsite_theme"/>'
                                        ,$t['Path']
                                        ,!empty($theme) ? checked($theme['Path'], $t['Path'], false) : ''
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }            
                    ?>
                </div>
            </div>
        </td>
    </tr>
</table>