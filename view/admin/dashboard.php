<div id="mbuilder-dashboard">
    <div id="dashboard-header">
        <a href="#" id="mobile-control-toggle">
            <span class="dashicons dashicons-menu"></span>
        </a>
    </div>
    <div id="dashboard-controls">
        <?php
        $controls = apply_filters( 'mbuilder_microsite_controls', array() );
        $sections       = array(
            'dashboard' => array('Dashboard',remove_query_arg( array('subpage') ), array())
        );
        $control_groups = array();
        foreach ($controls as $control_slug => $control) 
        {
            if(!isset($control_groups[$control['group']]))
            {
                $control_groups[$control['group']] = array(
                    'controls'  => array()
                    ,'_sort'    => $control['group_sort']
                    ,'name'     => $control['group']
                );
            }

            $control_groups[$control['group']]['controls'][$control_slug] = $control;
        }

        $sections['dashboard'][2] = $control_groups;

        foreach ($sections as $section_key => $section) 
        {
            $section_control_groups = $section[2];
            uasort($section_control_groups, function($group_a, $group_b){
                if($group_a['_sort'] == $group_b['_sort'])
                {
                    return strcmp($group_a['name'], $group_b['name']);
                }

                return $group_a['_sort'] < $group_b['_sort'] ? -1 : 1;
            });
            ?>
            <div class="section" id="msp-section-<?php echo $section_key ?>">
                <h1 class="section-title" >
                    <a href="<?php echo $section[1] ?>">
                        <?php echo $section[0] ?>
                    </a>
                </h1>
                <?php
                foreach($section_control_groups as $group_name => $section_control_group)
                {
                    ?>
                    <ul class="control-group">
                        <h2 class="group-title"><?php echo $group_name ?></h2>
                        <ul class="controls">
                            <?php
                            foreach ($section_control_group['controls'] as $control_slug => $control) 
                            {
                                $control_url = add_query_arg(array(
                                    'post_type' => msp()->get_post_type()
                                    ,'mbuilder' => msp()->get_microsite_id()
                                    ,'page'     => 'mbuilder-dashboard'
                                    ,'subpage'  => $control_slug
                                ),admin_url('/edit.php'));

                                ?>
                                <li class="control <?php echo (isset($_GET['subpage']) AND $_GET['subpage'] == $control_slug) ? 'active' : '' ?>">
                                    <?php echo $control['icon'] ?>
                                    <a href="<?php echo $control_url ?>" class="text">
                                        <?php echo $control['menu'] ?>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </ul>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>
    </div>
    <div id="dashboard-content">
        <?php
        if(empty(mikro()->home_url()))
        {
            msp_render('/view/admin/element/respond-message.php', array(
                'is_error'  => true
                ,'message'  => sprintf(
                    'Microsite not yet activated, please complete some information <a href="%s">here</a>'
                    ,add_query_arg('subpage','setups')
                )
            ));
        }
        ?>
        <?php
        if(isset($_GET['subpage']))
        {
            if(isset($controls[$_GET['subpage']]))
            {
                $current_page = $controls[$_GET['subpage']];
                ?>
                <div id="dashboard-content-title" class="<?php echo empty($current_page['icon']) ? 'no-icon' : '' ?>">
                    <div class="the-title-block">
                        <a class="the-microsite-home" title="View Microsite" href="<?php echo mikro()->home_url() ?>">
                            <span class="dashicons dashicons-admin-site"></span>    
                        </a>
                        <span class="the-icon-block">
                            <?php echo $current_page['icon'] ?>
                        </span>
                        <h2 class="the-title">
                            <?php echo $current_page['page'] ?>
                        </h2>
                    </div>
                </div>
                <div id="dashboard-content-block">
                    <?php call_user_func_array($current_page['callback'], array(msp()->get_microsite_id())); ?>
                </div>
                <?php
            }
            else
            {
                msp_render('/view/admin/element/respond-message.php', array(
                    'is_error'  => true
                    ,'message'  => 'Page is removed or you do not have access'
                ));
            }
        }
        else
        {
            $microsite = msp()->get_microsite(msp()->get_microsite_id());
            ?>
            <div id="microsite-dashboard">
                <div id="microsite-dashboard-identity">
                    <div class="the-information">
                        <h1 class="the-microsite-title"><?php echo $microsite->post_title ?></h1>
                        <?php
                        if(!empty(mikro()->home_url()))
                        {
                            printf(
                                '<a class="the-home-url" href="%1$s"><span class="dashicons dashicons-admin-site"></span> View Microsite</a>'
                                , mikro()->home_url() 
                            );
                        }
                        ?>
                    </div>
                </div>
                
                <ul id="microsite-dashboard-info">
                    <li class="the-info color-scheme-1">
                        <div class="wrapper">
                            <span class="info-title">Posts</span>
                            <span class="info-value">~</span>
                        </div>
                    </li>
                    <li class="the-info color-scheme-2">
                        <div class="wrapper">
                            <span class="info-title">Category</span>
                            <span class="info-value">~</span>
                        </div>
                    </li>
                    <li class="the-info color-scheme-3">
                        <div class="wrapper">
                            <span class="info-title">Themes</span>
                            <span class="info-value">~</span>
                        </div>
                    </li>
                    <li class="the-info color-scheme-4">
                        <div class="wrapper">
                            <span class="info-title">Author</span>
                            <span class="info-value">~</span>
                        </div>
                    </li>
                </ul>
                <div id="microsite-widget">
                    
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>