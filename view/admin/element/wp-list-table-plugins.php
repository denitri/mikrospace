<?php
class mbuilder_plugins_list extends WP_List_Table{
    
    /**
     * @var array $plugins plugins to display
     */
    var $plugins = array();

    /**
     * @var array $args table args
     */
    var $args = array();

    function __construct($plugin_list, $args){
        parent::__construct();

        $this->plugins = $plugin_list;

        $this->args = $args;
    }

    function get_columns(){
        return array(
            'plugin'                => 'Plugin'
            ,'description'          => 'Description'
        );
    }

    function column_default($row, $column){
        switch($column)
        {
            case 'plugin':
                $row_actions = array();
                if(!empty($this->args['actions']))
                {
                    $row_actions = call_user_func_array($this->args['actions'], array($row, $column));
                }

                $plugin_name_column = sprintf('<strong>%s</strong>  %s', $row['PluginName'], $this->row_actions($row_actions));

                if(!empty($this->args['plugin_name_render']))
                {
                    $plugin_name_column = call_user_func_array($this->args['plugin_name_render'], array($plugin_name_column, $row));
                }
                
                return $plugin_name_column;
            case 'description':
                $info = sprintf('
                    <div class="inactive second plugin-version-author-uri">
                        %1$s %2$s
                    </div>'
                    ,!empty($row['Version']) ? 'Version ' . $row['Version'] : ''
                    ,!empty($row['Author']) ? '| By ' . (!empty($row['AuthorURI']) ? sprintf('<a href="%s">%s</a>', $row['AuthorURI'], $row['Author']) : $row['Author']) : ''
                );
                return '<p>'. $row['description'] . '</p>' . $info;
            default:
                return '';
        }
    }

    function prepare_items(){
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = array();


        
        
        foreach ($this->plugins as $plugin_path) 
        {            
            $plugin_data = $theme_data = get_file_data($plugin_path, array(
                'PluginName'   => 'Plugin Name'
                ,'PluginURI'    => 'Plugin URI'
                ,'description'  => 'description'
                ,'Version'      => 'Version'
                ,'Author'       => 'Author'
                ,'AuthorURI'    => 'Author URI'
                ,'License'      => 'License'
            ));

            $plugin_data['path'] = str_replace(WP_CONTENT_DIR . '/plugins/', '', $plugin_path);

            array_push($this->items, $plugin_data);
        }
    }
}
?>