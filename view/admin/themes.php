<?php
class mbuilder_theme_list extends WP_List_Table{
    
    var $network_themes = array();

    function __construct(){
        parent::__construct();

        $this->network_themes = get_option( 'mbuilder_network_themes', array() );
        if(empty($this->network_themes))
        {
            $this->network_themes = array();
        }
    }

    function get_columns(){
        return array(
            'thumbnail'             => ''
            ,'name'                 => 'Name'
            ,'version'              => 'Version'
        );
    }

    function column_default($row, $column){
        switch($column)
        {
            case 'thumbnail':
                return sprintf('<span class="image-box theme-preview"><img src="%s"/></span> ', $row['Preview']) ;
            case 'name':
                ?>
                <div class="theme-name-box">
                    <h2 class="the-name"><?php echo $row['ThemeName'] ?></h2>
                    <span class="the-path"><?php echo $row['Path'] ?></span>
                    <span class="the-author">
                        Created By : <a href="<?php echo $row['AuthorURI'] ?>"><?php echo  $row['Author'] ?></a>
                    </span>
                </div>
                <div class="theme-actions">
                    <?php
                    printf(
                        '<a href="%s" class="button button-primary">%s</a>'
                        ,msp_process_api()->mark_link(add_query_arg(array(
                            'theme_path'    => $row['Path']
                            ,'theme_action' => !isset($this->network_themes[$row['Path']]) ? 'activate' : 'deactivate'
                        )), 'mbuilder-network-theme-activation')
                        ,!isset($this->network_themes[$row['Path']]) ? 'Enable in Network' : 'Network Disable'
                    )
                    ?>
                </div>
                <?php
                return '';
            case 'version':
                return $row['Version'];
            default:
                return '';
        }
    }

    function prepare_items(){
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $themes     = array();
        $themes_dir = msp()->get_themes_dir();
        $themes_url = msp()->get_themes_url();
        $dirs = scandir($themes_dir);
        
        foreach($dirs as $theme)
        {
            if(is_dir($themes_dir . "/". $theme) AND file_exists($themes_dir . "/$theme/style.css"))
            {
                $theme_data = msp()->get_theme_info($theme);
                if($theme_data['MikroSpace'] == 'Yes')
                {
                    $themes[$theme_data['ThemeName']]  = $theme_data;
                }
            }
        }

        asort($themes);

        $this->items = $themes;
    }
}
?>
<div class="wrap">
    <h1 class="wp-heading-inline">Themes</h1>
    <p class="description">
        Enable/Disable theme(s) for use in microsite network
    </p>
    <div id="mbuilder-themes-list">
        <?php
        msp_process_api()->respond();
        $table = new mbuilder_theme_list();
        $table->prepare_items();
        $table->display(); 
        ?>
    </div>
</div>