<?php
class mbuilder_users_list extends WP_List_Table{
    function __construct(){
        parent::__construct();
    }

    function get_columns(){
        return array(
            'username'  => 'Username'
            ,'publish-microsite'    => 'Publish'
            ,'manage-microsites'    => 'Manage All'
        );
    }

    function column_default($row, $column){
        switch($column)
        {
            case 'username':
                return $row->data->user_login;
            case 'publish-microsite':
                return sprintf(
                    '
                    <div class="can-toggle">
                        <input id="publish-%1$s-toggle" type="checkbox" name="publishable[%1$s]" %2$s>
                        <label for="publish-%1$s-toggle">
                            <div class="can-toggle__switch" data-checked="on" data-unchecked="off"></div>
                        </label>
                    </div>
                    '
                    ,$row->data->ID
                    ,checked( !empty($row->allcaps['publish-microsite'])  ? 1 : 0 , 1, false )
                );
            case 'manage-microsites':
                return sprintf(
                    '
                    <div class="can-toggle">
                        <input id="manage-all-%1$s-toggle" type="checkbox" name="manage-all[%1$s]"  %2$s>
                        <label for="manage-all-%1$s-toggle">
                            <div class="can-toggle__switch" data-checked="on" data-unchecked="off"></div>
                        </label>
                    </div>
                    '
                    ,$row->data->ID
                    ,checked( !empty($row->allcaps['manage-microsites']) ? 1 : 0 , 1, false )
                );
            default:
                return '';
        }
    }

    function prepare_items(){
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = get_users(array(
            'role'  => 'editor'
        ));
    }
}
?>
<div class="wrap">
    <h1 class="wp-heading-inline">Users</h1>
    <p class="description">
        Manage per user on editor level capabilities
    </p>
    <div class="mbuilder-users">
        <form action="" method="post">
            <?php
            msp_process_api()->mark('mbuilder_save_users_role');
            $data = msp_process_api()->respond();
            $table = new mbuilder_users_list();
            $table->prepare_items();
            $table->display(); 


            ?>
            <div class="submit">
                <input type="submit" class="button button-primary" value="Save Settings">
            </div>
        </form>
    </div>
</div>