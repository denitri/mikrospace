(function($){
    function imagepicker(e){
        e.preventDefault();
        e.stopPropagation();    
        var wp_uploader;
        var $this       = $(this);
        var input       = $this.attr('data-imagepicker-input');
        var parent      = $this.attr('data-imagepicker-parent');
        var $input      = $(input);
        var caption     = $this.attr('data-imagepicker-caption') || 'Use This Image';
        if(typeof parent != 'undefined')
        {
            var $parent = $this.parents(parent);
            var $input  = $parent.find(input);
        }
        else
        {
            var $parent = $(parent);
            var $input  = $(input);
        }
        
        if(typeof $input == 'undefined')
            return;
        
        if ( wp_uploader )
        {
            wp_uploader.open();
            return;
        }
 
        wp_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Select'
            ,button: {
                text: 'Select'
            }
            ,multiple: false  
        });

        wp_uploader.on('select', function(){
            var attachment  = wp_uploader.state().get('selection').first().toJSON();
                        
            $input.val(attachment.id);
            $input.trigger("input");
            if($this.is('img'))
            {
                $this
                    .hide()
                    .attr('src',attachment.url)
                    .fadeIn();
            }
            else
            {
                $this.text(attachment.url).addClass('selected');
            }
            $parent.find('.remove-imagepicker').show();
        });

        wp_uploader.open();
    }

    function removeImagePicker(e){
        e.preventDefault();
        var $this   = $(this);
        var parent  = $this.attr('data-imagepicker-parent');
        var input   = $this.attr('data-imagepicker-input');
        var img     = $this.attr('data-imagepicker-img');
        if(typeof parent != 'undefined')
        {
            var $parent = $this.parents(parent);
            var $input  = $parent.find(input);
            var $img    = $parent.find(img);
        }
        else
        {
            var $parent = $(parent);
            var $input  = $(input);
            var $img    = $(img);
        }
        
        var defaultSrc  = $img.attr('data-default-src') || '';
        
        if(typeof input != 'undefined')
        {
            $input.val('');
            $input.trigger("input");
        } 
        if(typeof img != 'undefined')
        {
            $img
                .hide()
                .attr('src',defaultSrc)
                .fadeIn();
        } 
        $parent.find('.remove-imagepicker').hide();
        $parent.find('.select-file').removeClass('selected').text('Select File');

    }

    function createDatePicker(i,obj){
        var $obj         = $(obj);
        var altField     = $obj.attr('data-datepicker-alt');
        var defField     = $obj.attr('data-datepicker-def');    
        
        if(typeof altField == 'undefined' || typeof defField == 'undefined')
            return;
        
        var options = {
                altField : '#'+altField
                ,defaultDate : $('#'+defField).val()
                ,changeYear : true
                ,changeMonth : true
                ,dateFormat    : 'yy-mm-dd'
        };
        
        $(obj).datepicker(options);
    }

    function createTimePicker(i, obj){
        $(obj).timepicker();
        console.log(obj);
    }

    $(document).ready(function(){
        $('#dashboard-content').on('click','.mbuilder-image-picker .image-box',function(){$(this).parent().find('.image-picker').trigger('click')});
        $('#dashboard-content').on('click','.mbuilder-image-picker .image-picker',imagepicker);
        $('#dashboard-content').on('click','.mbuilder-image-picker .remove-imagepicker',removeImagePicker);
        $('.metabox-datepicker .datepicker').each(createDatePicker);
    });
})(jQuery);