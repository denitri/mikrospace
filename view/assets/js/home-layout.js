(function($){
    $(document).ready(function(){
        var $elementContainer = $( "#mhp-element-used" );
        
        function sortableReceive(event, ui){
            var index = _.uniqueId('element_');
                    
            ui.helper[0].innerHTML = ui.helper[0].innerHTML.replace(/<% index %>/g,index);
            
            var $element = $(ui.helper);

            var elementID = $element.attr('data-element-id');

            $element
                .addClass('appended')
                .removeClass('available')
                .attr('data-index-id', index)
                ;

            $elementContainer.trigger('mhp-element-dropped', [index, elementID, $element ] );

            window.mhp.elemConvertor($element, 'append');

            if($element.attr('data-multiple') != 1)
            {
                if($elementContainer.find('.mhp-element.' + elementID).length > 1)
                {
                    $element.remove();
                    alert('This element can only have 1 instance');
                }
            }
        };

        function columnFaker(){
            var $columns = $('#mhp-element-used .mhp-element.mbuilder-column-two, #mhp-element-used .mhp-element.mbuilder-column-three');
            _.each($columns, function(column){
                var $column = $(column);
                var childHas    = $column.find('.mhp-element').length;
                var column_index = $column.attr('data-index-id');
                var capacity    = $column.hasClass('mbuilder-column-two') ? 2 : 3;
                var toFake      = capacity - childHas;
                if(toFake > 0)
                {
                    var template = $column.find('.fake-column-template').html();
                    for(var i = 1; i <= toFake; i++)
                    {
                        var index = _.uniqueId('element_');
                        $column.find('.mhp-sortable').append($(template.replace(column_index,index)));
                    }
                }
            })
        } 

        window.mhp = {
            elemConvertor : function($element, mode){
                /**
                 * TinyMCE Convertor
                 */
                $convertToMCE = $element.find('textarea.tinymce');
                if($convertToMCE.length)
                {
                    if(!$convertToMCE.hasClass('converted'))
                    {
                        var MCEId = _.uniqueId('mhpmce');
                        $convertToMCE.attr('id', MCEId);
                        $convertToMCE.addClass('converted');
                        wp.editor.initialize(MCEId,{
                            tinymce     : true
                            ,quicktags  : true
                            ,content    : $convertToMCE.text()
                            ,mediaButtons : true
                        });
                    }
                }

                /**
                 * Datepicker Enabler
                 */
                $datepicker = $element.find('.text.datepicker');
                if($datepicker.length)
                {
                    if(!$datepicker.hasClass('converted'))
                    {
                        var MCEId = _.uniqueId('mhpdtp');
                        $datepicker
                            .attr('id', MCEId)
                            .addClass('converted')
                            .datepicker();
                    }
                }

                /**
                 * Convert element into movable
                 */
                if(!$element.hasClass('static'))
                {
                    var isInsideColumn = $element.closest('.mhp-element');
                    if(isInsideColumn.length == 0 || (isInsideColumn.length > 0 && !isInsideColumn.hasClass('static')))
                    {
                        $element.draggable({
                            connectToSortable   : '.mhp-sortable'
                            ,cancel             : '.content-block'
                        });

                        /**
                         * Droppable Column
                         */
                        $element.find('.mhp-sortable').sortable({
                            placeholder: "mhp-element-placeholder"
                            ,receive : function(event,ui){
                                sortableReceive(event,ui);
                                var $child      = $(ui.helper);
                                var isColumn    = $child.hasClass('mbuilder-column-two') || $child.hasClass('mbuilder-column-three');
                                var columnCapacity = 0;
                                if($element.hasClass('mbuilder-column-two'))
                                {
                                    columnCapacity = 2;
                                }
                                else if($element.hasClass('mbuilder-column-three'))
                                {
                                    columnCapacity = 3;
                                }

                                if(isColumn)
                                {
                                    $child.remove();
                                    alert('You can not put column inside a column');
                                }
                                
                                $element.find('.mhp-element.mbuilder-fake-column-element').eq(0).remove();

                                if($element.find('.mhp-element').length > columnCapacity)
                                {
                                    $child.remove();
                                    alert('Column is full');
                                }

                            } 
                        });
                    }
                }

                
                columnFaker();
            }
        }

        $("#element-available .mhp-element").draggable({
            connectToSortable   : '.mhp-sortable'
            ,helper             : 'clone'
            ,appendTo           : '#element-available'
            ,cancel             : '.content-block'
        });

        $('#mhp-element-used .mhp-element').each(function( i, $element){
            window.mhp.elemConvertor($($element), 'init');
        });
 
        $elementContainer
            .sortable({
                handle : '.element-reorder'
                ,containment : '#mhp-element-used'
                ,placeholder: "mhp-element-placeholder"
                ,receive :sortableReceive
            })
            .on('click','.element-config', function(e){
                e.preventDefault();
                var $element = $(this).closest('.mhp-element');
                $element.find('> .content-block').toggleClass('display');
                $element.trigger('mhp-open-config');
            })
            .on('click','.element-delete', function(e){
                e.preventDefault();
                var ask = confirm('Are you sure ? ');
                
                if(!ask)
                {
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    return;
                }
                
                $(this).closest('.mhp-element').remove();
            })
            .on('click','.mbuilder-column-two .element-delete, .mbuilder-column-three .element-delete', columnFaker)
            ;
    });
})(jQuery)