<div class="wrap">
    <p class="description">
        Disable host plugin from loading into microsite environment
    </p>
    <div class="mbuilder-block-plugins">
        <?php
        msp_process_api()->respond();

        $plugins = array_merge(wp_get_active_and_valid_plugins(), wp_get_active_network_plugins());

        foreach ($plugins as $index => $plugin) 
        {
            if( strpos($plugin, 'mikrospace') !== false )
            {
                unset($plugins[$index]);
            }
        }

        require_once msp()->plugin_dir . '/view/admin/element/wp-list-table-plugins.php';
        $table = new mbuilder_plugins_list( $plugins, array(
            'actions' => function($row, $column) use ($microsite_id){
                $actions = array();
                $disabled_plugins = wp_cache_get('disabled-host-plugins', 'mbuilder');
                if(empty($disabled_plugins))
                {
                    $disabled_plugins = get_post_meta($microsite_id, 'disabled-host-plugins', true);
                    if(empty($disabled_plugins))
                    {
                        $disabled_plugins = array();
                    }
                    wp_cache_add('disabled-host-plugins', $disabled_plugins, 'mbuilder');
                }

                if(isset($disabled_plugins[$row['path']]))
                {
                    $actions['enable'] = sprintf('<a href="%s">Enable</a>',msp_process_api()->mark_link(add_query_arg(array(
                        'disable-host-plugins'  => urlencode($row['path'])
                        ,'validity'             => wp_create_nonce($row['path'])
                    )),'mbuilder-disable-host-plugin'));
                }
                else
                {
                    $actions['disable'] = sprintf('<a href="%s">Disable</a>',msp_process_api()->mark_link(add_query_arg(array(
                        'disable-host-plugins'  => urlencode($row['path'])
                        ,'validity'             => wp_create_nonce($row['path'])
                    )),'mbuilder-disable-host-plugin'));
                }
                return $actions;
            }
            ,'plugin_name_render' => function($plugin_name, $row) use ($microsite_id){
                $disabled_plugins = wp_cache_get('disabled-host-plugins', 'mbuilder');
                if(empty($disabled_plugins))
                {
                    $disabled_plugins = get_post_meta($microsite_id, 'disabled-host-plugins', true);
                    if(empty($disabled_plugins))
                    {
                        $disabled_plugins = array();
                    }
                    wp_cache_add('disabled-host-plugins', $disabled_plugins, 'mbuilder');
                }

                if(isset($disabled_plugins[$row['path']]))
                {
                    $plugin_name = '<strong style="color:red">[DISABLED]</strong> ' .$plugin_name;
                }

                return $plugin_name;

            }
        ));
        $table->prepare_items();
        $table->display(); 
        ?>
    </div>
</div>