<?php $microsite = msp()->get_microsite($microsite_id) ?>
<form action="" method="post">
    <input type="hidden" name="id" value="<?php echo $microsite_id ?>">
    <input type="hidden" name="validity" value="<?php echo wp_create_nonce('config-ui-' . $class_stamp) ?>">
    <input type="hidden" name="_class_stamp" value="<?php echo $class_stamp ?>">
    <?php msp_process_api()->respond() ?>
    <?php msp_process_api()->mark('mbuilder-config-ui') ?>
    <table class="form-table">
        <?php
        if(!empty($configs))
        {
            
            foreach ($configs as $key => $meta_args) 
            {
                $args = wp_parse_args( $meta_args, array(
                    'required'  => false
                    ,'label'    => ''
                    ,'type'     => 'text'
                    ,'default'  => ''
                    ,'caps'     => 'manage-microsite'
                ));

                if(!mikro()->current_user_can($args['caps']))
                {
                    continue;
                }

                // special meta is thumbnail and post content
                if($args['type'] != 'hidden')
                {
                    echo sprintf('<tr id="field-%s">', $key);
                    echo '<th scope="row">';
                    printf('<label for="%1$s">%2$s %3$s</div>',$key, $args['label'], $args['required'] ? '*' : '');
                    echo '</th>';
                    echo '<td>';
                }
                switch($args['type'])
                {
                    case 'datepicker':
                        ?>
                        <input type="hidden" id="<?php echo $key ?>" name="<?php echo $key ?>" value="<?php echo isset($data[$key]) ? $data[$key] : '' ?>"/>
                        <div 
                            class="datepicker"
                            data-datepicker-def="<?php echo $key ?>"
                            data-datepicker-alt="<?php echo $key ?>"
                        ></div>
                        <?php
                        break;
                    case 'hidden':
                        printf(
                            '<input type="hidden" name="'.$key.'" class="regular-text" value="%s"/>'
                            , isset($data[$key]) ? esc_attr($data[$key])  : $args['default']
                        );
                        break;
                    case 'text':
                        printf(
                            '<input type="text" name="'.$key.'" class="regular-text" value="%s"/>'
                            , isset($data[$key]) ? esc_attr($data[$key])  : $args['default']
                        );
                        break;
                    case 'textarea':
                        printf(
                            '<textarea name="%s" class="regular-textarea">%s</textarea>'
                            ,$key
                            , isset($data[$key]) ? esc_html( $data[$key] )  : $args['default']
                        );
                        break;
                    case 'select':
                        echo '<select name="'.$key.'">';
                        foreach ($args['options'] as $option_index => $option_label ) 
                        {
                            printf(
                                '<option value="%s" %s>%s</option>'
                                , $option_index
                                , selected( (isset($data[$key]) ? $data[$key] : $args['default']), $option_index, false )
                                , $option_label
                            );
                        }
                        echo '</select>';
                        break;
                    case 'mce': 
                        wp_editor(isset($data[$key]) ? $data[$key] : $args['default'],$key,array(
                            'textarea_name'     => $key
                            ,'textarea_rows'    => 3
                        ));
                        break;
                    case 'image-picker':
                        $data[$key]                 = isset($data[$key]) ? $data[$key] : $args['default'];
                        $data[$key . '-src']        = wp_get_attachment_url( $data[$key] );
                        $data[$key . '-src']        = !empty($data[$key . '-src']) ? $data[$key . '-src'] : '';
                        ?>
                        <?php msp_image_picker('meta-imagepicker-' . $key, $key, $data[$key], $data[$key . '-src']); ?>
                        <?php
                        break;
                    case 'file-picker':
                        $data[$key]                 = isset($data[$key]) ? $data[$key] : $args['default'];
                        $data[$key . '-src']        = wp_get_attachment_url( $data[$key] );
                        $data[$key . '-src']        = !empty($data[$key . '-src']) ? $data[$key . '-src'] : '';
                        ?>
                        <?php msp_file_picker('meta-imagepicker-' . $key, $key, $data[$key], $data[$key . '-src']); ?>
                        <?php
                        break;
                    default:
                        if(is_callable($args['render']))
                        {
                            call_user_func_array($args['render'], array($key, $args, $data));
                        }
                        break;
                }
                if($args['type'] != 'hidden')
                {
                    printf('<p class="description">%s</p>', isset($args['desc']) ? $args['desc'] : '');
                    echo '</td>';
                    printf('</tr>');
                }
            }
        }
        ?>
    </table>
    <div class="submit">
        <input type="submit" class="button button-primary" value="Save">
    </div>
</form>