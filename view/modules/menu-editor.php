<?php
class mbuilder_menu_list extends WP_List_Table{

    var $menus;

    var $microsite_menus;

    function __construct(){
        parent::__construct();

        $this->menus = mikro()->get_registered_nav_menus();;

        $this->microsite_menus = get_post_meta(msp()->get_microsite_id(), 'menus', true);

        if(empty($this->microsite_menus))
        {
            $this->microsite_menus = array();
        }
    }

    function get_columns(){
        $columns = array(
            'location'              => 'Menu ID'
            ,'manage-menu'          => ''
        );

        if(current_user_can('publish-microsite'))
        {
            $columns['create-menu'] = '';
        }

        return $columns;

    }

    function column_default($row, $column){
        switch($column)
        {
            case 'location':
                return $row;
            case 'create-menu':
                $location = array_search($row, $this->menus);

                if(empty($this->microsite_menus[$location]))
                {
                    return sprintf('<a class="button button-primary" href="%s">Setup</a>',  msp_process_api()->mark_link(
                        add_query_arg(array(
                            'location'      => $location
                        ))
                        ,'mbuilder-menu-create'
                    ));
                }

                return '';
            case 'manage-menu':
                $location = array_search($row, $this->menus);

                if(!empty($this->microsite_menus[$location]))
                {
                    return sprintf('<a class="button button-primary" href="%s">Manage</a>',  add_query_arg(array(
                        'menuid'       => $this->microsite_menus[$location]
                    )));
                }
                return '';
            default:
                return '';
        }
    }

    function prepare_items(){
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $this->menus;
    }
}
?>

<div class="mbuilder-menus">
    <?php
    if(isset($_GET['menuid']))
    {
        printf('<a class="button button-primary" href="%s">View All Menus</a>', remove_query_arg(array('menuid')));

        echo '<p class="description"><br/>Manage the menu using below box</p>';

        printf('<iframe src="%s" id="mbuilder-menu-manage-frame"></iframe>', add_query_arg(
            array(
                'mbuilder'          => msp()->get_microsite_id()
                ,'IFRAME_REQUEST'   => ''
                ,'menu'             => $_GET['menuid']
                ,'action'           => 'edit'
            )
            ,admin_url('/nav-menus.php') )
        );
    }
    else
    {
        echo '<p class="description">Manage this microsite menus</p>';

        msp_process_api()->respond();
        $table = new mbuilder_menu_list();
        $table->prepare_items();
        $table->display(); 
    }
    ?>
</div>