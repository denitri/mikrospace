<?php $microsite    = msp()->get_microsite($microsite_id) ?>
<?php $path         = msp()->get_microsite_path($microsite_id); ?>
<form action="" method="post">
    <input type="hidden" name="id" value="<?php echo $microsite_id ?>">
    <input type="hidden" name="validity" value="<?php echo wp_create_nonce($microsite_id . '-save-settings') ?>">
    <?php $responds = msp_process_api()->respond() ?>
    <?php 
    if(empty($responds))
    {
        $responds = array();
    }
    ?>
    <?php msp_process_api()->mark('mbuilder-settings-microsite') ?>
    <p class="description">Adjust your microsite settings here.</p>
    <table class="form-table">
        <tr>
            <th>Title</th>
            <td>
                <input type="text" class="regular-text" name="post_title" value="<?php echo isset($responds['post_title']) ? $responds['post_title'] : $microsite->post_title ?>">
                <p class="description">
                    Your microsite title
                </p>
            </td>
        </tr>
        <tr>
            <th>Path</th>
            <td>
                <input type="text" class="regular-text" name="post_name" value="<?php echo isset($responds['post_name']) ? $responds['post_name'] : $path ?>">
                <p class="description">
                    The path of this microsite, ex : <?php echo home_url('/{path}') ?>
                </p>
            </td>
        </tr>
        <tr>
            <th>Tagline</th>
            <td>
                <textarea class="large-text" name="post_content" cols="30" rows="10"><?php echo isset($responds['post_title']) ? $responds['post_title'] : $microsite->post_content ?></textarea>
                <p class="description">
                    Microsite meta description
                </p>
            </td>
        </tr>
        <tr>
            <th>Category @Host</th>
            <td>
                <?php $categories       = get_categories( array(
                    'hide_empty'    => false
                ) );?>
                <?php $microsite_cat = msp()->get_microsite_cat($microsite_id) ?>
                <select name="category" id="">
                    <option value="">Select Category</option>
                    <?php
                    foreach ($categories as $category) 
                    {
                        printf(
                            '<option value="%s" %s>%s</option>'
                            ,$category->term_id
                            ,selected( (isset($responds['post_title']) ? $responds['post_title'] : $microsite_cat) ,$category->term_id, false)
                            ,$category->name
                        );
                    }
                    ?>
                </select>
                <p class="description">
                    Select category at host what will be use by this microsite to store and get the posts
                </p>
            </td>
        </tr>
    </table>
    <div class="submit">
        <input type="submit" class="button button-primary" value="Save">
        <?php
        if(current_user_can( 'manage_options' ))
        {
            ?>
            <a href="<?php echo msp_rewrite::get_instance()->microsite_flush_rule_link($microsite_id) ?>" class="button">Flush Rules</a>
            <?php
        }
        ?>
    </div>
</form>