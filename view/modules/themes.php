<p class="description">
    Select from list of available themes below
</p>

<div class="mbuilder-microsite-themes-selection">
    &nbsp;
    <?php msp_process_api()->respond(); ?>
    &nbsp;
    <div class="theme-browser rendered">
        <div class="themes wp-clearfix">
            <?php 
            $current_theme          = msp()->get_microsite_theme( $microsite_id );
            $current_theme_name     = isset($current_theme['Path']) ? $current_theme['Path'] : '';

            $network_themes = get_option( 'mbuilder_network_themes', array() );
            if(empty($network_themes))
            {
                $network_themes = array();
            }

            $themes     = array();
            $themes_dir = msp()->get_themes_dir();
            $themes_url = msp()->get_themes_url();
            $dirs       = scandir($themes_dir);
            
            foreach($dirs as $theme)
            {
                if(is_dir($themes_dir . "/". $theme) AND file_exists($themes_dir . "/$theme/style.css"))
                {
                    $theme_data = msp()->get_theme_info($theme);
                    if($theme_data['MikroSpace'] == 'Yes')
                    {
                        $themes[$theme_data['ThemeName']]  = $theme_data;
                        
                        if(!current_user_can( 'manage_options' ) AND !isset($network_themes[$theme]))
                        {
                            unset($themes[$theme_data['ThemeName']]);
                        }
                    }
                }

            }

            asort($themes);

            foreach ($themes as $theme) 
            {
                ?>
                <div class="theme <?php echo $theme['Path'] == $current_theme_name ? 'active' : '' ?>" data-slug="<?php echo $theme['Path'] ?>">
                    <?php
                    if(empty($theme['Preview']))
                    {
                        echo '<div class="theme-screenshot blank"></div>';
                    }
                    else
                    {
                        printf(
                            '<div class="theme-screenshot"><img src="%s" alt=""></div>'
                            ,$theme['Preview']
                        );
                    }
                    ?>
                    <div class="theme-id-container">
                        <h2 class="theme-name" id="<?php echo $theme['Path'] ?>">
                            <?php echo $theme['Path'] == $current_theme_name ? '<span>Active</span> : ' : '' ?><?php echo $theme['ThemeName'] ?>			
                        </h2>
                        <div class="theme-actions">
                            <?php
                            printf(
                                '<a class="button button-primary" href="%s">Activate</a>'
                                ,msp_process_api()->mark_link(add_query_arg(array(
                                    'theme'             => $theme['Path']
                                    ,'microsite_id'     => $microsite_id
                                    ,'validation'       => wp_create_nonce( $microsite_id . '_themes' )
                                )), 'mbuilder_module_activate_theme')
                            )
                            ?>
                            
                        </div>
                    </div>
                </div>
                <?php
            }            
            ?>
        </div>
    </div>
</div>