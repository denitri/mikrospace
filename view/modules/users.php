<?php
class mbuilder_microsie_admin_list extends WP_List_Table{

    var $microsite_id;

    function __construct($microsite_id){
        parent::__construct();

        $this->microsite_id = $microsite_id;
    }

    function get_columns(){
        return array(
            'username'              => 'Username'
            ,'add'                  => 'Allow'
        );
    }

    function column_default($row, $column){
        switch($column)
        {
            case 'username':
                return sprintf('<strong>%s</strong>', $row->data->user_login);
            case 'add':
                $current_microsite = msp()->get_microsite($this->microsite_id);
                $manageable_sites = get_user_meta( $row->data->ID, '_manageable_microsite', true );
                if(empty($manageable_sites))
                {
                    $manageable_sites = array();
                }

                if($current_microsite->post_author == $row->data->ID)
                {
                    return 'Owner';
                }

                return sprintf(
                    '
                    <div class="can-toggle">
                        <input id="publish-%1$s-toggle" type="checkbox" name="manageable[%1$s]" %2$s>
                        <label for="publish-%1$s-toggle">
                            <div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
                        </label>
                    </div>
                    '
                    ,$row->data->ID
                    ,checked( isset($manageable_sites[$this->microsite_id]) ? 1 : 0 , 1, false )
                );
            default:
                return '';
        }
    }

    function prepare_items(){
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $non_global_manage_users = array();
        $editors = get_users(array(
            'role__in'  => array('editor', 'mbuilder-editor') 
        ));
        foreach ($editors as $editor) 
        {
            if(empty($editor->allcaps['manage-microsites']))
            {
                array_push($non_global_manage_users, $editor);
            }
        }
        $this->items = $non_global_manage_users;
    }
}
?>
<p class="description">
    Add editor/microsite editor users to manage this microsite
</p>
<div class="mbuilder-users">
    <form action="" method="post">
        <input type="hidden" name="microsite_id" value="<?php echo $microsite_id ?>">
        <?php
        msp_process_api()->mark('mbuilder_add_manageable_microsite');
        // $data = msp_process_api()->respond();
        $table = new mbuilder_microsie_admin_list($microsite_id);
        $table->prepare_items();
        $table->display(); 


        ?>
        <div class="submit">
            <input type="submit" class="button button-primary" value="Save">
        </div>
    </form>
</div>