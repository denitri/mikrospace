<?php
printf(
    "<link rel='stylesheet' id='mbuilder-assets-css'  href='%s' type='text/css' media='all' />"
    ,msp()->plugin_url . '/view/assets/css/view/posts-editor.css'
);

function _print_fields($data, $key, $args, $field_key = false){
    if($field_key === false)
    {
        $field_key = $key;
    }
    switch($args['type'])
    {
        case 'datepicker':
            ?>
            <input type="hidden" id="<?php echo $key ?>" name="<?php echo $field_key ?>" value="<?php echo esc_attr( isset($data[$key]) ? $data[$key] : '' ) ?>"/>
            <div 
                class="datepicker"
                data-datepicker-def="<?php echo $key ?>"
                data-datepicker-alt="<?php echo $key ?>"
            ></div>
            <?php
            break;
        case 'timepicker':
            ?>
            <input type="time" id="<?php echo $key ?>" name="<?php echo $field_key ?>" value="<?php echo esc_attr( isset($data[$key]) ? $data[$key] : '' ) ?>"/>
            <?php
            break;
        case 'hidden':
            printf(
                '<input type="hidden" name="'.$field_key.'" class="regular-text" value="%s"/>'
                ,esc_attr(isset($data[$key]) ? $data[$key] : $args['default']) 
            );
            break;
        case 'text':
            printf(
                '<input type="text" name="'.$field_key.'" class="regular-text" value="%s"/>'
                ,esc_attr(isset($data[$key]) ? $data[$key] : $args['default']) 
            );
            break;
        case 'textarea':
            printf(
                '<textarea name="%s" class="regular-textarea">%s</textarea>'
                ,$field_key
                , isset($data[$key]) ? $data[$key] : $args['default']
            );
            break;
        case 'select':
            echo '<select name="'.$field_key.'">';
            foreach ($args['options'] as $option_index => $option_label ) 
            {
                printf(
                    '<option value="%s" %s>%s</option>'
                    , esc_attr($option_index)
                    , selected( (isset($data[$key]) ? $data[$key] : $args['default']), $option_index, false )
                    , $option_label
                );
            }
            echo '</select>';
            break;
        case 'mce': 
            wp_editor(isset($data[$key]) ? $data[$key] : $args['default'],$key,array(
                'textarea_name' => $field_key
            ));
            break;
        case 'checkbox':
            if(!is_array($args['default']))
            {
                $args['default'] = array();
            }
            
            echo '<ul class="post-checkbox" id="checkbox-'.$key.'">';
            foreach ($args['options'] as $option_index => $option_label) 
            {
                printf(
                    '<li class="the-checkbox"><label><input name="%3$s[]" type="checkbox" value="%2$s" %4$s/>%1$s</label></li>'
                    ,$option_label
                    ,esc_attr($option_index)
                    ,$field_key
                    ,checked( (in_array($option_index, !empty($data[$key]) ? $data[$key] : $args['default']) ? 1 : 0) , 1, false )
                );
            }
            echo '</ul>';
            break;
        case 'radio':
            echo '<ul class="post-radio" id="radio-'.$key.'">';
            foreach ($args['options'] as $option_index => $option_label) 
            {
                printf(
                    '<li class="the-checkbox"><label><input name="%3$s" type="radio" value="%2$s" %4$s/>%1$s</label></li>'
                    ,$option_label
                    ,esc_attr($option_index)
                    ,$field_key
                    ,checked( isset($data[$key]) ? $data[$key] : $args['default'] , $option_index, false )
                );
            }
            echo '</ul>';
            break;
        case 'group':
            $available  = binus_array_isset($args['options'], 'available', 5);
            $keys       = binus_array_isset($args['options'], 'fields', array());
            $sets = isset($data[$key]) && is_array($data[$key]) ? $data[$key] : array();
            $sets = array_merge( array_fill(0, $available - count($sets), ''), $sets );
            foreach ($sets as $index => $set) 
            {
                printf('<fieldset class="field-group">');
                printf('<label>%s #%s</label>',$args['label'], $index + 1);
                foreach ($keys as $k => $k_args) 
                {
                    echo '<div class="fieldset-item">';
                        printf( '<label>%s</label>', $k_args['label'] );
                        echo '<div>';
                        _print_fields($set, $k, $k_args, $field_key.'['.$index.']['.$k.']');
                        echo '</div>';
                    echo '</div>';
                }
                printf('</fieldset>');
            }
            break;
        case 'image-picker':
            $data[$key]                 = isset($data[$key]) ? $data[$key] : $args['default'];
            $data[$key . '-src']        = wp_get_attachment_url( $data[$key] );
            $data[$key . '-src']        = !empty($data[$key . '-src']) ? $data[$key . '-src'] : '';
            ?>
            <?php msp_image_picker('meta-imagepicker-' . $key, $field_key, $data[$key], $data[$key . '-src']); ?>
            <?php
            break;
        case 'file-picker':
            $data[$key]                 = isset($data[$key]) ? $data[$key] : $args['default'];
            $data[$key . '-src']        = wp_get_attachment_url( $data[$key] );
            $data[$key . '-src']        = !empty($data[$key . '-src']) ? $data[$key . '-src'] : '';
            ?>
            <?php msp_file_picker('meta-imagepicker-' . $key, $field_key, $data[$key], $data[$key . '-src']); ?>
            <?php
            break;
        default:
            if(is_callable($args['render']))
            {
                call_user_func_array($args['render'], array($key, $args, $data, $field_key));
            }
            break;
    }
}
?>
<form action="" method="post" class="mbuilder-post-form">
    <?php
    if(!empty($object_id) AND !empty($add_url))
    {
        printf(
            '<a href="%s" class="button button-small" id="add-new-object" role="button" aria-expanded="false">Add New</a>'
            ,$add_url
        );
    }
    ?>
    
    <input type="hidden" name="ID" value="<?php echo !empty($object_id) ? $object_id : 0 ?>">
    <input type="hidden" name="status" value="publish">
    <input type="hidden" name="validity" value="<?php echo wp_create_nonce( (!empty($object_id) ? $object_id : 0) . '-post-ui') ?>">
    <?php
    if(isset($post_type))
    {
        printf('<input type="hidden" name="post_type" value="%s">', $post_type);
    }
    ?>
    <?php $submit_status = msp_process_api()->respond() ?>
    <?php 
    $data = wp_parse_args($data, array(
        'title'             => ''
        ,'content'          => ''
        ,'_thumbnail_id'    => ''
        ,'tax'              => array()
    ));
    ?>
    <?php msp_process_api()->mark($id) ?>
    <div class="title-box editor-box">
        <input type="text" name="title" value="<?php echo stripslashes($data['title'])  ?>" class="title-field" placeholder="Title..">
    </div>

        <?php
        if(in_array('editor',$support))
        {
            echo '<div class="editor-box">
                <span class="box-title">Description</span>';

            wp_editor(stripslashes($data['content']), 'post-content', array(
                'textarea_name' => 'content'
            ));
            echo '</div>';
        }
        ?>

    <?php
    if(isset($post_type))
    {
        $taxes = apply_filters('mbuilder_generic_editor_object_taxonomies', get_object_taxonomies( $post_type, 'objects' ), $post_type ) ;
        foreach ($taxes as $tax => $tax_args) 
        {
            $terms_args = apply_filters('mbuilder_generic_editor_terms_args', array(
                'hide_empty'    => false
                ,'taxonomy'     => $tax
                ,'multiple'     => true
                ,'meta_key'     => '_mbuilder_updated'
                ,'meta_value'   => msp()->get_microsite_id()
            ), $tax, $post_type);

            $tax_args = wp_parse_args( $tax_args, array(
                'multiple'  => true
            ) );


            $terms = apply_filters('mbuilder_generic_editor_terms', get_terms($terms_args), $tax, $post_type);

            if($tax_args['multiple'])
            {
                $type = 'checkbox';
                
            }
            else
            {
                $type = 'radio';
            }
            $name = sprintf('tax[%s][]', $tax);


            $data_tax = isset($data['tax'][$tax]) ? $data['tax'][$tax] : array();
            printf('<div class="editor-box metabox-tag">');
            printf('<div class="box-title">%s</div>',$tax_args['label']);
            echo '<ul class="the-taxes">';
            foreach ($terms as $term) 
            {
                printf(
                    '<li class="the-tax"><label><input name="%3$s" type="%4$s" value="%2$s" %5$s/>%1$s</label></li>'
                    ,$term->name
                    ,$term->term_id
                    ,$name
                    ,$type
                    ,checked( (in_array($term->term_id, $data_tax) ? 1 : 0) , 1, false )
                );
            }
            echo '</ul>';
            printf('</div>');
        }
    }
    ?>

    <?php
    if(isset($metas))
    {
        foreach ($metas as $key => $meta_args) 
        {
            $args = wp_parse_args( $meta_args, array(
                'required'  => false
                ,'label'    => ''
                ,'type'     => 'text'
                ,'default'  => ''
            ));
            // special meta is thumbnail and post content
            if($args['type'] != 'hidden')
            {
                printf('<div class="editor-box metabox-%s">', $args['type']);
                printf('<div class="box-title">%s %s</div>',$args['label'], $args['required'] ? '*' : '');
            }

            _print_fields($data, $key, $args );
            
            if($args['type'] != 'hidden')
            {
                printf('<p class="metabox-desc description">%s</p>', isset($args['desc']) ? $args['desc'] : '');
                printf('</div>');
            }
        }
    }
    ?>

    <?php
    if(in_array('thumbnail',$support))
    {
        
        echo '<div class="editor-box">
            <span class="box-title">Feature Image</span>';
                if(!empty($data['_thumbnail_id']))
                {
                    $thumbnail = wp_get_attachment_image_url($data['_thumbnail_id'], 'thumbnail');
                }
                else
                {
                    $thumbnail = '';
                }
        msp_image_picker('post-thumbnail', '_thumbnail', intval($data['_thumbnail_id']), $thumbnail);
        echo '</div>';
    }
    ?>

    <div class="editor-box submit-box">
        <?php
        if(!empty($object_id))
        {
            $publish = 'Update';
        }
        else
        {
            $publish = 'Publish';
        }
        if(isset($second_button))
        {
            printf('<a href="%s" id="object-editor-save-draft" class="button button-secondary">%s</a> ', $second_button['url'], $second_button['text']);
        }
        printf('<button class="button button-primary">%s</button>', $publish);
        ?>
    </div>


</form>