<?php
class mbuilder_microsie_posts_list extends WP_List_Table{

    var $posts;

    var $total;

    var $perpage;

    var $post_converter;

    var $args = array();

    function __construct($posts,$total,$perpage,$post_converter = false, $args = array()){
        parent::__construct();
        $this->posts            = $posts;
        $this->total            = $total;
        $this->perpage          = $perpage;
        $this->post_converter   = $post_converter;
        $this->args             = $args;
    }

    function get_columns(){
        return array(
            'title'         => 'Title'
            ,'date'         => 'Date'
        );
    }

    function column_default($row, $column){
        switch($column)
        {
            case 'title':

                $link_replace = array(
                    '__nonce_id__'  => wp_create_nonce($row['id'])
                    ,'__id__'       => $row['id']
                );
            
                $row_actions = array(
                    'edit'  => sprintf(
                        '<a href="%s">Edit</a>'
                        ,isset($this->args['edit_link']) ? strtr($this->args['edit_link'], $link_replace) : '#'
                    )
                    ,'delete'  => sprintf(
                        '<a href="%s">Delete</a>'
                        ,isset($this->args['delete_link']) ? strtr($this->args['delete_link'], $link_replace)  : '#'
                    )
                );

                if(isset($this->args['permalink']))
                {
                    $permalink = call_user_func_array($this->args['permalink'], array($row));
                    
                    if(!empty($permalink))
                    {
                        $row_actions['permalink'] = sprintf(
                            '<a href="%s">View</a>'
                            ,$permalink
                        );
                    }
                }

                if(empty($this->args['edit_link']))
                {
                    unset($row_actions['edit']);
                }
                if(empty($this->args['delete_link']))
                {
                    unset($row_actions['delete']);
                }

                $post_status = '';
                if(!empty($row['status']) && $row['status'] != 'publish')
                {
                    $post_status = ' - ' . $row['status'];
                }

                return sprintf('<strong>%s %s</strong>  %s', $row['title'], $post_status, $this->row_actions($row_actions));
            case 'date':
                if(empty($row['date']))
                {
                    return '';
                }
                $label = 'Published';
                
                if($row['status'] != 'publish')
                {
                    $label = 'Last Modified';
                }
                return $label . '<br/>' . date('d F Y H:i', $row['date']);
            default:
                return '';
        }
    }

    function prepare_items(){
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        $this->set_pagination_args( array(
            'total_items'   => $this->total
            ,'per_page'     => $this->perpage
        ));

        if($this->post_converter === false)
        {
            $this->items = $this->posts;
        }
        else
        {
            $this->items = array_map($this->post_converter, $this->posts);
        }
    }
}
?>
<div class="mbuilder-posts-list">
    <?php $table = new mbuilder_microsie_posts_list($posts, $total, $perpage, $post_converter, $args) ?>
    
    <?php $table->prepare_items()?>

    <form action="<?php echo home_url( add_query_arg(array('paged' => 1)) ) ?>" method="post">
        <?php $table->search_box('search', 'search_posts'); ?>
    </form>
    
    <?php $table->display()?>

</div>