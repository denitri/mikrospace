<div id="post-ui-list" class="wrap">
    <h2 class="wp-heading-inline">
        All <?php echo $label ?>
        <a href="<?php echo add_query_arg('edit',0) ?>" class="page-title-action aria-button-if-js" role="button" aria-expanded="false">Add New</a>
    </h2>
</div>
<?php

msp_process_api()->respond();

msp_render('/view/posts/list.php', array(
    'posts'             => $posts
    ,'total'            => $total_posts
    ,'perpage'          => 10
    ,'post_converter'   => function($post){
        return array(
            'id'        => $post['ID']
            ,'title'    => $post['post_title']
            ,'date'     => strtotime($post['post_date']) 
            ,'status'       => $post['post_status']
            ,'permalink'    => $post['permalink']
        );
    }
    ,'args'             => array(
        'edit_link'     => remove_query_arg('paged', add_query_arg('edit', '__id__')) 
        ,'delete_link'  => msp_process_api()->mark_link(
            add_query_arg(
                array(
                    $post_type . '_delete_id'   => '__id__'
                    ,'delete_validity'          => '__nonce_id__'
                )
            )
            ,$delete_mark
        )
        ,'permalink'    => function($row){
            if(!empty($row['permalink']))
            {
                return $row['permalink'];
            }
            return '';
        }
    )
));