<?php
class mbuilder_microsie_term_list extends WP_List_Table{

    var $posts;

    var $total;

    var $perpage;

    var $post_type;

    var $args = array();

    function __construct($posts, $total, $perpage, $post_type,  $args = array()){
        parent::__construct();
        $this->posts            = $posts;
        $this->total            = $total;
        $this->perpage          = $perpage;
        $this->args             = $args;
        $this->post_type        = $post_type;
    }

    function get_columns(){
        return array(
            'name'         => 'Name'
        );
    }

    function column_default($row, $column){
        switch($column)
        {
            case 'name':

                $link_replace = array(
                    '__nonce_id__'  => wp_create_nonce($row->term_id)
                    ,'__id__'       => $row->term_id
                );
            
                $row_actions = array(
                    'edit'  => sprintf(
                        '<a href="%s">Edit</a>'
                        ,isset($this->args['edit_link']) ? strtr($this->args['edit_link'], $link_replace) : '#'
                    )
                    ,'delete'  => sprintf(
                        '<a href="%s">Delete</a>'
                        ,isset($this->args['delete_link']) ? strtr($this->args['delete_link'], $link_replace)  : '#'
                    )
                );

                if(empty($this->args['edit_link']))
                {
                    unset($row_actions['edit']);
                }
                if(empty($this->args['delete_link']))
                {
                    unset($row_actions['delete']);
                }


                return sprintf('<strong>%s</strong>  %s', $row->name, $this->row_actions($row_actions));
            default:
                return '';
        }
    }

    function prepare_items(){
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        $this->set_pagination_args( array(
            'total_items'   => $this->total
            ,'per_page'     => $this->perpage
        ));

        $this->items = $this->posts;
    }
}
?>
<div id="post-ui-term-list">
    <div id="col-container">
        <?php msp_process_api()->respond() ?>
        <div id="col-left">
            <div class="col-wrap">
                <div class="form-wrap">
                    <?php  

                    $metas = array_merge(array(
                        'taxonomy'  => array(
                            'type'  => 'hidden'
                        )
                        ,'_parent'   => array(
                            'type'      => 'select'
                            ,'label'    => 'Parent'
                            ,'required' => false
                            ,'options'  => array(
                                ''  => 'Select Parent'
                            )
                        )
                    ), $metas);

                    $view_args = array(
                        'support'       => array()
                        ,'id'           => 'mbuilder-post-ui-term-' . $taxonomy
                        ,'data'         => array(
                            'taxonomy' => $taxonomy
                        )
                        ,'object_id'    => !empty($_GET['edit']) ? $_GET['edit'] : 0 
                        ,'metas'        => $metas
                    );

                    if(!empty($_GET['edit']) && wp_verify_nonce($_GET['edit_validity'], $_GET['edit']))
                    {
                        $parent_terms = get_terms(array(
                            'hide_empty'    => false
                            ,'parent'       => msp()->get_microsite_cat(msp()->get_microsite_id())
                            ,'taxonomy'     => $taxonomy
                            ,'fields'       => 'id=>name'
                            ,'exclude'      => array($_GET['edit'])
                        ));

                        $view_args['metas']['_parent']['options'] = array_merge($view_args['metas']['_parent']['options'], $parent_terms) ;

                        $term = get_term_by('id', $_GET['edit'], $taxonomy);

                        $view_args['object_id']         = $_GET['edit'];

                        $view_args['second_button']     = array(
                            'url'   => remove_query_arg(array('edit','edit_validity'))
                            ,'text' => 'Cancel Update'
                        );

                        $view_args['data']['title']     = $term->name;

                        foreach ($metas as $meta_key => $meta_value) 
                        {
                            if($meta_key == '_class_stamp')
                            {
                                continue;
                            }
                            $view_args['data'][$meta_key] = get_term_meta($term->term_id, $meta_key, true);
                        }
                        $view_args['data']['taxonomy'] = $taxonomy;

                        if(!empty($term))
                        {
                            printf ('<h2>Edit %s : %s</h2>', $label, $term->name);
                        }
                    }
                    else
                    {
                        $parent_terms = get_terms(array(
                            'hide_empty'    => false
                            ,'parent'       => msp()->get_microsite_cat(msp()->get_microsite_id())
                            ,'taxonomy'     => $taxonomy
                            ,'fields'       => 'id=>name'
                        ));

                        $view_args['metas']['_parent']['options'] = array_merge($view_args['metas']['_parent']['options'], $parent_terms) ;

                        printf ('<h2>Add New %s</h2>', $label);
                    }
                    ?>
                    <?php msp_render('/view/posts/editor.php', $view_args); ?>
                </div>
            </div>
        </div>
        <div id="col-right">
            <div class="col-wrap">
                <?php
                $perpage    = 10;
                $page       = !empty($_GET['paged']) ? $_GET['paged'] : 1;
                
                $terms = get_terms(apply_filters('mbuilder_term_ui_list_terms_args', array(
                    'taxonomy'      => $taxonomy
                    ,'number'       => $perpage
                    ,'hide_empty'   => false
                    ,'offset'       => ($page - 1) * $perpage
                    ,'meta_key'     => '_mbuilder_updated'
                    ,'meta_value'   => msp()->get_microsite_id()
                ),$taxonomy, $post_type));

                $total = intval(get_terms(apply_filters('mbuilder_term_ui_list_terms_count_args',array(
                    'count'             => true
                    ,'hide_empty'       => false
                    ,'taxonomy'         => $taxonomy
                ),$taxonomy, $post_type)));
                
                $table = new mbuilder_microsie_term_list($terms, $total, $perpage,$post_type, array(
                    'edit_link'     => add_query_arg(array(
                        'edit'              => '__id__'
                        ,'edit_validity'    => '__nonce_id__'
                    ))
                    ,'delete_link'  => msp_process_api()->mark_link(
                        remove_query_arg(array('edit','edit_validity')
                            ,add_query_arg(
                                array(
                                    'delete_id'                 => '__id__'
                                    ,'delete_validity'          => '__nonce_id__'
                                    ,'tax-to-delete'            => $taxonomy
                                )
                            )
                        )
                        ,$delete_mark
                    ) 
                ));
                $table->prepare_items();
                $table->display();
                ?>
            </div>

        </div>
    </div>
</div>