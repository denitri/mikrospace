<?php $theme = msp()->get_microsite_theme($microsite_id); ?>
<?php
if(empty($theme))
{
    return;
}
?>
<div id="widget-active-themes">

    <span class="theme-name">
        <?php echo $theme['ThemeName'] ?>
    </span>
    <span class="theme-author">
        By : <?php echo $theme['Author'] ?>
    </span>
    <div class="widget-pic-wrapper theme-preview">
        <div class="image-box ">
            <img src="<?php echo $theme['Preview'] ?>" alt="<?php echo $theme['ThemeName'] ?>">
        </div>
    </div>
</div>