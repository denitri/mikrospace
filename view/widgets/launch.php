<div id="widget-launch">
    <div class="launch-date">
        <?php echo date('d F Y',strtotime($microsite->post_date))  ?>
    </div>
    <div class="launch-time">
        <?php echo date('H:i',strtotime($microsite->post_date))  ?>
    </div>
</div>